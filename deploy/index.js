#!/usr/bin/env node
'use strict'

const fs = require('fs')
const path = require('path')
const ajv = new (require('ajv'))()
const { program } = require('commander')
const childProcess = require('child_process')

const SUCCESS_EXIT = 0
const ERROR_EXIT = 1

/**
 * Deploy to a single environment
 *
 * @param {object} deployConf: object containing deploy configs for all environments
 * @param {string} env: the name of the environment to deploy to
 * @param {array}  args: custom deploy command-line arguments
 * @callback cb
 */
const deployForEnv = async (deployConf, env, args) => {
  if (!deployConf[env]) throw new Error(`Eviroment ${env} not defined in deploy section`)

  const pipedData = JSON.stringify(deployConf[env])
  const targetConf = JSON.parse(pipedData) // effectively clones the conf

  if (targetConf.ssh_options) {
    let sshOpt = ''
    if (Array.isArray(targetConf.ssh_options)) {
      sshOpt = '-o ' + targetConf.ssh_options.join(' -o ')
    } else {
      sshOpt = '-o ' + targetConf.ssh_options
    }
    targetConf.ssh_options = sshOpt
  }

  if (!ajv.validate({
    type: 'object',
    properties: {
      user: {
        type: 'string',
        minLength: 1,
      },
      host: {
        type: ['string', 'array'],
      },
      repo: {
        type: 'string',
      },
      path: {
        type: 'string',
      },
      ref: {
        type: 'string',
      },
      fetch: {
        type: 'string',
      },
    },
    required: ['host', 'path', 'ref'],
  }, targetConf)) {
    throw new Error(ajv.errorsText())
  }

  console.info('--> Deploying to %s environment', env)
  if (process.platform !== 'win32' && process.platform !== 'win64') targetConf.path = path.resolve(targetConf.path)
  if (!Array.isArray(targetConf.host)) targetConf.host = [targetConf.host]

  for (const host of targetConf.host) {
    console.info('--> on host %s', host.host ? host.host : host)
    targetConf.host = host
    const confJson = JSON.stringify(targetConf)
    /**
     * Spawn a modified version of visionmedia/deploy
     */
    await new Promise((resolve, reject) => {
      const shellSyntaxCommand = `"${__dirname.replace(/\\/g, '/')}/deploy.sh" --conf '${confJson}' ${args.join(' ')}`
      const proc = childProcess.spawn('sh', ['-c', shellSyntaxCommand], { stdio: 'inherit' })

      proc.once('error', reject)
      proc.once('close', resolve)
    })
  }
}

const deploy = async (file, env = 'production', args) => {
  if (!file) {
    const defaultConfigNames = ['ecosystem.config.js', 'ecosystem.json', 'ecosystem.json5', 'package.json']
    file = defaultConfigNames.find(name => {
      try { fs.statSync(name) } catch (e) { return false }
      return true
    })
    if (!file) throw new Error(`Not any default deployment file exists. Allowed default config file names are: ${defaultConfigNames.join(', ')}`)
  }
  if (file.indexOf('.config.js') === -1 && file.indexOf('.json') === -1) throw new Error('In valid config file extension.')

  const jsonConf = require(path.resolve(file))

  if (!jsonConf.deploy || !jsonConf.deploy[env]) throw new Error(`Environment ${env} is not defined in ${file} file`)

  if (!jsonConf.deploy[env]['post-deploy']) jsonConf.deploy[env]['post-deploy'] = 'echo no post-deploy command'

  await deployForEnv(jsonConf.deploy, env, args)
}

const usage = `[options] [command]

  Commands:
    setup                run remote setup commands
    update               update deploy to the latest release
    revert [n]           revert to [n]th last deployment or 1
    curr[ent]            output current release commit
    prev[ious]           output previous release commit
    exec|run <cmd>       execute the given <cmd>
    list                 list previous deploy commits
    ref [ref]            deploy to [ref], the "ref" setting, or latest tag`

if (require.main === module) {
  program
    .option('-f, --file <file>', 'custom configurstion file, default is ecosytem.config.js')
    .option('-e, --env <enviroment>', 'deployment enviroment, default is production')
    .usage(usage)
    .parse(process.argv)
  deploy(program.file, program.env, program.args)
    .then(() => {
      console.info('--> Success')
      return process.exit(SUCCESS_EXIT)
    })
    .catch((e) => {
      console.error(e.message)
      console.error('Deploy failed')
      return process.exit(ERROR_EXIT)
    })
}
