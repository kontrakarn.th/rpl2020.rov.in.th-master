const PROJECT_NAME = 'ved-nodekit'

const USER = 'tienquang.nguyen'
const PATH = `/var/www/${PROJECT_NAME}`

module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: PROJECT_NAME,
      script: 'dist/server.js',
      instances: 'max',
      exec_mode: 'cluster',
      env: {},
      combine_logs: true,
      out_file: '../logs/pino.log',
      error_file: '../logs/error.log',
    },
  ],
  deploy: {
    production: {
      user: USER,
      host: ['137.59.117.57'],
      path: PATH,
      ref: 'origin/master',
      'pre-setup': `sudo mkdir -p ${PATH} && sudo chown \`whoami\` ${PATH}`,
      'post-setup': 'echo noop',
      'post-deploy': 'yarn && yarn migrate && yarn build-server && yarn pm2 && yarn build-browser'
    },
  }
}
