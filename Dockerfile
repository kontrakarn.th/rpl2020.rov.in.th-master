FROM ubuntu:focal

ENV NODE_VERSION 14.4.0

# Install nginx
RUN ARCH= && dpkgArch="$(dpkg --print-architecture)" \
    && case "${dpkgArch##*-}" in \
      amd64) ARCH='x64';; \
      ppc64el) ARCH='ppc64le';; \
      s390x) ARCH='s390x';; \
      arm64) ARCH='arm64';; \
      armhf) ARCH='armv7l';; \
      i386) ARCH='x86';; \
      *) echo "unsupported architecture"; exit 1 ;; \
    esac \
    && apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests nginx-light curl ca-certificates xz-utils \
    && curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
    && tar -xJf "node-v$NODE_VERSION-linux-$ARCH.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
    && rm "node-v$NODE_VERSION-linux-$ARCH.tar.xz" \
    && apt-get remove --purge --auto-remove -y curl ca-certificates xz-utils \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

RUN groupadd --gid 1000 node \
    && useradd --uid 1000 --gid node --shell /bin/bash --create-home node \
    && sed -i 's/^user.*/user node;/;s/worker_processes.*/worker_processes auto;/;s/worker_connections.*/worker_connections 8192;/' /etc/nginx/nginx.conf

WORKDIR /src

# Build the dist files
ADD . .
RUN npm install -g yarn \
    && yarn && yarn build \
    && yarn --production \
    && yarn cache clean \
    && npm uninstall -g yarn \
    && npm cache clean --force \
    && rm -rf static
