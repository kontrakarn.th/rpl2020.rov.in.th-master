if (require.main === module) {
  const path = require('path')
  const { program } = require('commander')

  program
    .command('build', 'run webpack build [default]', {
      isDefault: true,
      executableFile: path.resolve(__dirname, 'build.js'),
    })
    .command('serve', 'run dev server', {
      executableFile: path.resolve(__dirname, 'serve.js'),
    })
    .command('runp', 'run npm commands parallel', {
      executableFile: path.resolve(__dirname, 'runp.js'),
    })
    .parse(process.argv)
}
