/**
 * Browser webpack settings.
 * This will provide the foundation settings for configuring our source code to work in any modern browser
 */

const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const PATHS = require('./paths')

module.exports = {
  __esModule: true,
  default: {
    // Javascript file extensions that webpack will resolve
    resolve: {
      extensions: ['.mjs', '.js', '.jsx'],
      modules: [PATHS.root, 'node_modules'],
    },

    // Output settings
    output: {
      publicPath: '/',
      path: PATHS.public,
    },

    // Optimization settings
    optimization: {
      splitChunks: { chunks: 'all', name: 'vendor' },
    },

    module: {
      rules: [
        {
          test: /\.jsx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                babelrc: false, // Ignore the .babelrc at the root of our project
                presets: [
                  ['@babel/preset-env', {
                    loose: true,
                    modules: false, // Enable tree-shaking by disabling commonJS transformation
                  }],
                  ['@babel/preset-react'],
                ],
                plugins: [
                  ['@babel/plugin-proposal-decorators', { 'legacy': true }],
                  ['@babel/plugin-proposal-class-properties', { 'loose': true }],
                ],
              },
            },
          ],
        },
        { // Fonts
          test: /\.(woff2?|[eot]tf|eot)$/i,
          loader: 'file-loader',
          options: {
            name: `assets/fonts/[name].[hash:10].[ext]`,
          },
        },
        { // Images
          test: /\.(jpe?g|png|gif|svg)$/i,
          loader: 'file-loader',
          options: {
            name: `assets/img/[name].[hash:10].[ext]`,
          },
        },
        { // GraphQL queries
          test: /\.(graphql|gql)$/,
          exclude: /node_modules/,
          loader: 'graphql-tag/loader',
        },
        { // CSV
          test: /\.csv$/,
          loader: 'csv-loader',
          options: {
            header: true,
            dynamicTyping: true,
            skipEmptyLines: true,
          },
        },
      ],
    },
  },

  ui: {
    name: 'ui',
    entry: { browser: PATHS.ui },
    plugins: [
      new HtmlWebpackPlugin({ template: path.resolve(PATHS.ui, 'template.html') }),
    ],
  },

  admin: {
    name: 'admin',
    entry: { admin: PATHS.admin },
    output: {
      path: path.resolve(PATHS.public, 'admin'),
      publicPath: '/admin/',
    },
    plugins: [
      new HtmlWebpackPlugin({ template: path.resolve(PATHS.admin, 'template.html') }),
    ],
  },
}
