/**
 * This settings generates a production-grade browser bundle.
 * It minifies and optimises all Javascript source code, and extracts and processes CSS before dumping it in
 * a finished `styles.css` file in the `dist` folder
 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const PATHS = require('./paths')
const { ui, admin } = require('./browser')
const { extend, css } = require('./common')

// Extend the `browser.js` settings
const browserProd = extend('browser', {
  mode: 'production',
  output: { filename: '[name].[chunkhash:10].js' },
  module: { rules: [...css.getCssLoaders(false)] },
  plugins: [new MiniCssExtractPlugin({ filename: 'assets/css/[name].[contenthash:10].css' })],
})

module.exports = [
  extend(browserProd, ui, {
    resolve: {
      alias: {
        'react': 'preact/compat',
        'react-dom/test-utils': 'preact/test-utils',
        'react-dom': 'preact/compat',
      },
    },
    output: { publicPath: PATHS.publicPath },
    plugins: [
      // Copy files from `PATHS.static` to `dist/public`. No transformations will be performed on the files-- they'll be copied as-is
      {
        from: PATHS.static,
        apply (compiler) {
          compiler.hooks.done.tap('copy-plugin', stats => {
            const os = require('os')
            const path = require('path')
            const shell = require('child_process').execSync
            const outputPath = compiler.options.output.path
            console.log(`copy static files to %s`, outputPath)
            switch (os.platform()) {
              case 'win32': {
                return shell(`xcopy /s/y ${this.from} ${outputPath}`)
              }
              case 'darwin': {
                return shell(`cp -Rf ${path.join(this.from, '/')}. ${outputPath}`)
              }
              default: {
                return shell(`cp -urf ${path.join(this.from, '/')}. ${outputPath}`)
              }
            }
          })
        }
      },
    ],
  }),
  extend(browserProd, admin),
]
