const runp = (commands) => {
  const { spawn } = require('child_process')
  for (const command of commands) {
    spawn('npm', ['run', command], { stdio: 'inherit', shell: true })
  }
}

if (require.main === module) {
  const { program } = require('commander')

  program
    .arguments('<commands...>')
    .action(runp)
    .parse(process.argv)
}
