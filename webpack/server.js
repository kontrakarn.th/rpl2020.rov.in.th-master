module.exports = {
  entry: './server',
  stats: 'minimal',
  target: 'node',
  node: { __dirname: true },
  optimization: { minimize: false },
  module: { rules: [{ test: /\.js$/, exclude: /node_modules/, loader: 'babel-loader' }] },
  externals: require('webpack-node-externals')(),
}
