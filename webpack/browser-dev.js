/**
 * Browser dev server.
 * This settings will be used with `webpack-dev-server` to enable hot-reloading. Sourcemaps and full debugging is enabled.
 */

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin')

const PATHS = require('./paths')
const { ui, admin } = require('./browser')
const { extend, css } = require('./common')

const browserDev = extend('browser', {
  mode: 'development',
  devtool: 'source-map',
  module: { rules: [...css.getCssLoaders(true)] },
  plugins: [
    new MiniCssExtractPlugin(),
    new ReactRefreshWebpackPlugin({
      overlay: {
        sockIntegration: 'whm',
      },
    }),
  ],
})

const jsxOptions = browserDev.module.rules.find(l => l.test.toString() === /\.jsx?$/.toString()).use[0].options
jsxOptions.plugins.unshift('react-refresh/babel')
jsxOptions.presets[1].push({ development: true })

// Dev server config
const devServer = {
  host: process.env.BROWSER_HOST || 'localhost',
  port: process.env.BROWSER_PORT || 8080,
  hot: true,
  stats: 'minimal',
  contentBase: [PATHS.static],
  historyApiFallback: true,
  proxy: {
    context: ['/graphql', '/admin/graphql', '/connect', '/fb'],
    target: `http://${process.env.HOST || 'localhost'}:${process.env.PORT || 8081}`,
    ws: true,
  },
}

module.exports = [
  extend(browserDev, ui, { devServer }),
  extend(browserDev, admin),
]
