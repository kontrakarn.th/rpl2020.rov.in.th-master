const path = require('path')
const memfs = require('memfs')
const webpack = require('webpack')

// patch memfs
memfs.join = require('path').join

class Webpack {
  constructor ({ config, fs, hmr } = {}) {

    this.configs = Webpack.configLoader(config, hmr)
    this.compiler = webpack(this.configs.length > 1 ? this.configs : this.configs[0])

    if (fs) this.compiler.outputFileSystem = fs
  }

  run () {
    this.compiler.run(Webpack.statsCallback)
  }

  watch () {
    const [{ watchOptions }] = this.configs

    this.compiler.watch(watchOptions, Webpack.statsCallback)
    console.log('Watching enabled')
  }

  static configLoader (config = 'webpack.config.js', hmr) {

    let configs = require(path.resolve(process.cwd(), config))
    if (configs instanceof Function) configs = config()

    const arr = Array.isArray(configs) ? configs : [configs || { entry: ['./src'] }]

    if (hmr) {
      const { isPlainObject } = require('lodash')
      const TimeFixPlugin = require('time-fix-plugin')
      const { HotModuleReplacementPlugin } = require('webpack')

      for (const result of arr) {
        if (!result.name) result.name = require('uuid/v4').slice(0, 8)

        // push hmr entries
        const hotEntry = `webpack-hot-middleware/client?name=${result.name}&reload=true`

        if (typeof result.entry === 'string') result.entry = [hotEntry, result.entry]
        else if (typeof result.entry === 'undefined') result.entry = [hotEntry, './src']
        else if (isPlainObject(result.entry)) {
          for (const key of Object.keys(result.entry)) {
            const entry = result.entry[key]
            if (!Array.isArray(entry)) result.entry[key] = [hotEntry, entry]
            else result.entry[key].unshift(hotEntry)
          }
        } else if (!Array.isArray(result.entry)) result.entry.unshift(hotEntry)

        // push hmr plugins
        if (Array.isArray(result.plugins)) {
          if (!result.plugins.find(plugin => plugin instanceof TimeFixPlugin)) {
            result.plugins.unshift(new TimeFixPlugin())
          }
          if (!result.plugins.find(plugin => plugin instanceof HotModuleReplacementPlugin)) {
            result.plugins.push(new HotModuleReplacementPlugin())
          }
        } else {
          result.plugins = [new TimeFixPlugin(), new webpack.HotModuleReplacementPlugin()]
        }
      }

    }

    return arr
  }

  static statsCallback (err, stats) {
    if (err) {
      console.error(err)
    } else {
      console.log(stats.toString({
        colors: true,
        chunks: false,
        modules: false,
      }))
    }
  }
}

class StaticServe {
  constructor ({ base, fs = require('fs') } = {}) {
    this.fs = fs
    this.base = base

    this.join = require('path').join
    this.getType = path => require('mime-types').lookup(path) || 'application/octet-stream'
  }

  middleware = (req, res, next) => {
    if (req.method !== 'GET' && req.method !== 'HEAD') return next()

    const file = this.join(this.base, '.' + req.path)

    this.fs.stat(file, (err, stat) => {
      if (err) return next()

      const sendFile = (file, stat) => {
        const type = this.getType(file)
        const stream = this.fs.createReadStream(file)

        res.setHeader('Content-Type', type)
        res.setHeader('Content-Length', stat.size)

        stream.pipe(res)
        stream.once('error', next)
      }

      if (stat.isDirectory()) {
        const index = this.join(file, 'index.html')
        this.fs.stat(index, (err, stat) => err ? next() : sendFile(index, stat))
      } else {
        sendFile(file, stat)
      }
    })
  }
}

class Proxy {
  constructor (context, options) {
    this.options = options

    const { parse, match } = require('matchit')
    const proxyContext = context.map(parse)
    for (const pattern of context) {
      switch (pattern.slice(-1)) {
        case '*':
          break
        case '/':
          proxyContext.push(parse(pattern + '*'))
          break
        default:
          proxyContext.push(parse(pattern + '/*'))
      }
    }

    this.match = url => match(url, proxyContext).length

    this.log = console

    this.agent = new (require('http').Agent)({ keepAlive: true })

    this.log.info('Proxy is running', options.target)
  }

  web (req, res, cb) {
    const http = require('http')
    const { URL } = require('url')

    const { hostname: host, port, pathname, search, protocol } = new URL(req.url, this.options.target)
    const request = http.request({
      method: req.method,
      headers: req.headers,
      agent: this.agent,
      host, port, path: pathname + search, protocol,
    }, response => {
      res.writeHead(response.statusCode, response.statusMessage, response.headers)
      response.pipe(res)
      response.once('error', cb)
    })

    req.pipe(request)
    request.once('error', cb)
  }

  ws (req, cltSocket, head, cb) {
    const net = require('net')
    const { URL } = require('url')
    const { finished } = require('stream')

    const target = new URL(req.url, this.options.target)

    const srvSocket = net.connect(target.port, target.hostname, () => {
      srvSocket.write(`GET ${target.pathname}${target.search} HTTP/1.1\r\n`)

      const { rawHeaders } = req
      for (let i = 0; i < rawHeaders.length; i += 2) {
        const key = rawHeaders[i]
        const value = rawHeaders[i + 1]
        srvSocket.write(`${key}: ${value}\r\n`)
      }

      srvSocket.write('\r\n')
      srvSocket.write(head)

      srvSocket.pipe(cltSocket).pipe(srvSocket)
    })

    cltSocket.setTimeout(0)

    finished(cltSocket, err => {
      if (err) cb(err)
      if (!srvSocket.destroyed) srvSocket.end()
    })
    finished(srvSocket, err => {
      if (err) cb(err)
      if (!cltSocket.destroyed) cltSocket.end()
      this.log.info('[ws] Client disconnected')
    })
  }

  apply (app) {
    app.use((req, res, next) => {
      if (!this.match(req.url)) return next()
      this.web(req, res, error => {
        this.log.error('[http]', error.message)
        res.statusCode = 500
        res.end(error.message)
      })
    })

    if (this.options.ws) {
      app.server.on('upgrade', (req, socket, head) => {
        if (!this.match(req.url)) { return }
        this.ws(req, socket, head, error => {
          this.log.error('[ws]', error.message)
        })
        this.log.info('[ws] Upgrading to WebSocket')
      })
    }
  }
}

module.exports = {
  Proxy,
  Webpack,
  StaticServe,
}
