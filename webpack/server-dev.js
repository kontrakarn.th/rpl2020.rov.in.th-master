const { extend } = require('./common')

module.exports = extend('server', {
  mode: 'development',
  output: { filename: 'server-dev.js' },
  watch: true,
  plugins: [{
    apply (compiler) {
      compiler.hooks.done.tap('done', () => {
        if (this.server) this.server.kill()
        this.server = require('child_process').fork('server', {
          execArgv: ['-r', '@babel/register', '--inspect'],
        })
      })
    }
  }]
})
