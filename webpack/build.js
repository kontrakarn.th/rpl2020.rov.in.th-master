const { Webpack } = require('./webpack')

if (require.main === module) {
  const { program } = require('commander')

  const { config, memory } = program
    .option('-m, --memory', 'use memfs')
    .option('-c, --config <file>', 'load config file')
    .parse(process.argv)

  const webpack = new Webpack({ config, fs: memory && require('memfs') })
  const { configs: [{ watch }] } = webpack

  if (watch) webpack.watch()
  else webpack.run()
}