const mime = require('mime-types')
const { MultiCompiler } = require('webpack')
const { Webpack, StaticServe, Proxy } = require('./webpack')

class DevServe {
  constructor (webpack) {
    webpack.watch()

    const { compiler } = webpack
    const { outputPath, outputFileSystem } = compiler instanceof MultiCompiler ? compiler.compilers[0] : compiler

    this.static = new StaticServe({ base: outputPath, fs: outputFileSystem })

    this.ready = new Promise((resolve, reject) => {
      webpack.compiler.hooks.done.tap('dev-serve', () => {
        resolve(true)
        this.ready = true
      })
    })
  }

  middleware = async (req, res, next) => {
    try {
      await this.ready
      this.static.middleware(req, res, next)
    } catch (err) {
      next(err)
    }
  }
}

class FallbackServe {
  constructor ({ dev, index = '/index.html' }) {
    this.dev = dev
    this.index = index
  }

  middleware = (req, res, next) => {
    if (mime.lookup(req.path)) {
      next()
    } else {
      req.path = this.index
      this.dev(req, res, next)
    }
  }
}

const serve = ({ config, open }) => {
  const http = require('http')
  const chalk = require('chalk')
  const polka = require('polka')
  const lodash = require('lodash')
  const hotMiddeware = require('webpack-hot-middleware')

  const defaults = {
    open,
    hot: true,
    port: 8080,
    contentBase: [],
    protocol: 'http',
    host: 'localhost',
  }

  const webpack = new Webpack({ config, fs: require('memfs'), hmr: true })
  const options = lodash.merge({}, defaults, webpack.configs[0].devServer)

  const app = polka()
  const log = console
  const server = http.createServer(app.handler)
  app.server = server

  const dev = new DevServe(webpack).middleware

  if (options.hot) app.use(hotMiddeware(webpack.compiler))

  app.use(dev)

  if (options.proxy) {
    const proxy = new Proxy(options.proxy.context, options.proxy)
    proxy.apply(app)
  }

  for (const base of options.contentBase) {
    const static = new StaticServe({ base })
    app.use(static.middleware)
  }

  if (options.historyApiFallback) {
    app.use(new FallbackServe({ dev }).middleware)
  }

  server.listen(options.port, options.host, () => {
    const uri = `${options.protocol}://${options.host}:${options.port}`
    log.info(chalk`Project is running at {blue ${uri}}`)
    if (options.open) {
      const open = require('open')
      open(new URL(options.open.path || '', uri).href, options.open.app || {})
    }
  })
}

if (require.main === module) {
  const { program } = require('commander')

  const { config, open } = program
    .option('-c, --config <file>', 'load config file')
    .option('-o, --open', 'open in the default browser')
    .parse(process.argv)

  serve({ config, open })
}
