const path = require('path')

// Parent folder = project root
const root = path.join(__dirname, '..')

module.exports = {
  // Root project folder.  This is the current dir.
  root,
  // Webpack configuration files
  webpack: path.join(root, 'webpack'),
  // Static files.  HTML, images, etc that can be processed by Webpack before being moved into the final `dist` folder
  static: path.join(root, 'static'),
  // Dist path; where bundled assets will wind up
  dist: path.join(root, 'dist'),
  // Public.  This is where our web server will start looking to serve static files from
  public: path.join(root, 'dist', 'public'),
  // Server source
  server: path.join(root, 'server'),
  // Admin source
  admin: path.join(root, 'admin'),
  // Front-end source
  ui: path.join(root, 'ui'),
  // Public production web. Exp: https://cdn.vn.garenanow.com/web/lol/
  publicPath: process.env.PUBLIC_PATH || '/',
}
