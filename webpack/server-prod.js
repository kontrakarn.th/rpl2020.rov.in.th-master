/**
 * Server-side production web server.
 * Generates a `dist/server.js` file that can be run directly
 */

const { extend } = require('./common')

module.exports = extend('server', {
  mode: 'production',
  output: { filename: 'server.js' },
})
