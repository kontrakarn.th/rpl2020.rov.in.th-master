const path = require('path')
const lodash = require('lodash')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const PATHS = require('./paths')

const css = {
  // CSS loader configuration -- plain CSS, SASS and LESS
  rules: [
    { ext: 'css', use: [] },
    { ext: 's[ac]ss', use: [{ loader: 'sass-loader', options: { implementation: require('sass') } }] },
  ],

  // Defaults to use with `css-loader` in all environments
  loaderDefaults: {
    esModule: true,
    importLoaders: 1, // Retain the loader pipeline
  },

  // Return an array containing the module RegExp and css-loader config, based on the original file extension
  getModuleRegExp (ext) {
    return [
      [`\\.${ext}$`, {
        modules: {
          auto: new RegExp(`\\.local\\.${ext}$`),
          localIdentName: '[local]_[hash:base64:5]', // Format for 'localised' CSS modules
        },
      }],
    ]
  },

  * getCssLoaders (isDev = false) {
    for (const loader of css.rules) {
      // Iterate over CSS/SASS/LESS and yield local and global mod configs
      for (const [regex, options] of css.getModuleRegExp(loader.ext)) {
        yield {
          test: new RegExp(regex),
          use: [
            { loader: MiniCssExtractPlugin.loader, options: { hmr: isDev } },
            { loader: 'css-loader', options: Object.assign({ sourceMap: isDev }, css.loaderDefaults, options) },
            ...loader.use,
          ],
        }
      }
    }
  },
}

const customMergeFunc = (objValue, srcValue) => lodash.isArray(objValue) ? objValue.concat(srcValue) : undefined

function requireES (moduleName) {
  const module = require(path.resolve(PATHS.webpack, moduleName))
  return module.__esModule ? module.default : module
}

function extend (...args) {
  const source = args.map(value => typeof value === 'string' ? requireES(value) : value)
  return lodash.mergeWith({}, ...source, customMergeFunc)
}

module.exports = { css, extend }
