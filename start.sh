#!/usr/bin/env sh

DIR=$(dirname "$0")
export DOMAIN=$(basename "$DIR")
export DOMAIN="rpl2020.rov.in.th"

echo $DOMAIN

# Setup nginx log dir and virtual host
export LOG_DIR=/var/log/nginx/$DOMAIN 
mkdir -p $LOG_DIR
chown node -R $LOG_DIR
sed s/example.com/$DOMAIN /src/nginx.conf > /etc/nginx/sites-enabled/default

# Start nginx with daemon on
nginx
chown -R node:node /var/tmp

cd /src
# .env loading in the shell
set -a
  [ -f .env ] && . ./.env
set +a

# Init enviroment variables
if test $DEPLOY = "test"; then
  export NODE_ENV=staging
  export DB_HOST=$STAGING_DB_HOST
  export DB_PASSWORD=$STAGING_DB_PASSWORD
else 
  export NODE_ENV=production
  export DB_HOST=$LIVE_DB_HOST
  export DB_PASSWORD=$LIVE_DB_PASSWORD
fi

echo $REGION

case $REGION in
  vn)
    export LANG_ID=3
    export LOCALE=vi-VN
    export GARENA_APP_ID=100054
    export DOMAIN=event.apl.lienquan.garena.vn
    export GAMEWS_HOST=vn.game.proxy.garenanow.com
    ;;
  th)
    export LANG_ID=4
    export LOCALE=th-TH
    export GARENA_APP_ID=100055
    export DOMAIN=rpl2020.rov.in.th
    export GAMEWS_HOST=th.game.proxy.garenanow.com
    ;;
  tw)
    export LANG_ID=2
    export LOCALE=zh-TW
    export GARENA_APP_ID=100050
    export DOMAIN=event.apl.moba.garena.tw
    export GAMEWS_HOST=tw.game.proxy.garenanow.com
    ;;
  id)
    export LANG_ID=6
    export LOCALE=id-ID
    export GARENA_APP_ID=100057
    export DOMAIN=event.apl.aov.garena.co.id
    export GAMEWS_HOST=id.game.proxy.garenanow.com
    ;;
  *)
    echo "No region specified!!!"
    exit 1
esac

set -x

sed -i "s/example.com/$DOMAIN" /src/dist/public/index.html

# Run migrate and seed database
npx knex migrate:latest

echo $LOG_DIR 

# Build and start backend
export UDS=/tmp/node.sock
[ -S $UDS ] && unlink $UDS
su node -c "node dist/server.js cluster -i 6 0<&- 1>>$LOG_DIR/node.log 2>>$LOG_DIR/node.error.log"
