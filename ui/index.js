// Browser entry point, for Webpack.  We'll grab the browser-flavoured
// versions of React mounting, routing etc to hook into the DOM

// Patch global.`fetch`
import 'unfetch/polyfill'
// Enable async/await and generators, cross-browser
import 'regenerator-runtime/runtime'
// Corejs
import 'core-js/features/promise/finally'

// React parts
import React from 'react'
import ReactDOM from 'react-dom'

// Apollo Provider
import client from 'ui/kit/graphql'
import { ApolloProvider } from '@apollo/client'

// Redux
// import store from 'ui/kit/redux'
// import { Provider } from 'react-redux'

// Browser routing
// import Router from './app/component/HashRouter'

// Root component.
import App from 'ui/app'

ReactDOM.render(
  <ApolloProvider client={client}>
    {/* <Provider store={store}> */}
    {/* <Router> */}
    <App />
    {/* </Router> */}
    {/* </Provider> */}
  </ApolloProvider>,
  document.getElementById('main'),
)
