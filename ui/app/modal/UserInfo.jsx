import React, { useState, useCallback, useEffect, useRef } from 'react'

import cl from 'clsx'
import { Form, useField } from 'react-final-form'
import { useQuery, useMutation } from '@apollo/client'

import showRule from './Rule'
import BaseModal from './BaseModal'
import { modal, showModal } from './index'
import Scrollbar from '../component/Scrollbar'
import { useCheckAuth, useAuth } from '../component/Auth'
import { errorHandler } from '../component/Common'

import STATE from '../../gql/queries/state.gql'
import USERINFO from '../../gql/queries/userinfo.gql'
import UPDATE_USERINFO from '../../gql/mutations/updateUserInfo.gql'

export const ID = 'user-info'

const PHONE_REGEX = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/

const TextInput = ({ name, validate, className, ...props }) => {
  const { input, meta: { touched, error } } = useField(name, { validate })

  return (
    <>
      <input className={cl(className, { error: touched && error })} type='text' {...input} {...props} />
      {touched && error && <div className="txt-error">{error}</div>}
    </>
  )
}
const UserForm = ({ handleSubmit, form: { submit, reset } }) => {
  const elm = useRef()
  const { lang } = useAuth()
  const [check, setCheck] = useState(false)

  const required = useCallback(value => value ? undefined : lang.get('FormRequiredField'), [lang])
  const isPhoneNumber = useCallback(value => PHONE_REGEX.test(value) ? undefined : lang.get('FormInvalidPhone'), [lang])

  useEffect(() => {
    if (elm.current) {
      for (const el of elm.current.getElementsByTagName('a')) {
        el.setAttribute('href', '#')
        el.onclick = (evt) => {
          showRule()
          evt.preventDefault()
        }
      }
    }
  }, [lang, elm.current])


  return (
    <form className="flex-grow-1 d-flex flex-column justify-content-between" onSubmit={handleSubmit}>
      <div className="title">{lang.get('UpdateUserInfoTitle')}</div>

      <div className="row justify-content-center">
        <div className="col-10 mb-3">
          <TextInput name="fullname" placeholder={lang.get('FormFullname')} validate={required} />
        </div>
        <div className="col-10 mb-3">
          <TextInput name="phone" placeholder={lang.get('FormPhone')} validate={isPhoneNumber} />
        </div>
        <div className="col-10 mb-3">
          <TextInput name="address" placeholder={lang.get('FormAddress')} validate={required} />
        </div>
        <div className="col-12 text-center">
          <input type="checkbox" name="rule" checked={check} onChange={evt => setCheck(evt.target.checked)} />
          <span ref={elm} className="checkrule" dangerouslySetInnerHTML={{ __html: lang.get('FormRuleCheck') }} />
        </div>
      </div>

      <div className="my-3 text-center">
        <div className={cl('btn btn-seco', { grayscale: !check })} onClick={check ? submit : null}>{lang.get('FormConfirm')}</div>
        <div className="btn btn-seco grayscale" onClick={reset}>{lang.get('FormReset')}</div>
      </div>
    </form>
  )
}

const SimplePopup = ({ close }) => {
  const { lang } = useAuth()

  const [updateUserInfo] = useMutation(UPDATE_USERINFO)
  const { data: { userInfo } = {} } = useQuery(USERINFO)

  return (
    <BaseModal close={close()}>
      <Form
        component={UserForm}
        initialValues={userInfo || {}}
        onSubmit={async value => {
          const { fullname, phone, address, facebook, email } = value
          try {
            await updateUserInfo({
              variables: { data: { fullname, phone, address, facebook, email } },
              refetchQueries: [
                { query: STATE },
              ],
            })
            close()()
          } catch (err) {
            errorHandler(err)
          }
        }} />
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default () => showModal(ID)
