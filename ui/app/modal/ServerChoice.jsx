import React, { useState, useCallback } from 'react'

import cl from 'clsx'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { useAuth } from '../component/Auth'

export const ID = 'schoose'

const SimplePopup = ({ close, data: { partitions } }) => {
  const { lang } = useAuth()
  const [value, setValue] = useState(0)

  const confirmClick = useCallback(evt => {
    if (value == 0) return false

    return close(value)(evt)
  }, [value])

  return (
    <BaseModal close={close()}>
      <div className="title">{lang.get('ServerChooseTitle')}</div>
      <div className="flex-grow-1 d-flex flex-column justify-content-center text-center">
        <p>{lang.get('ServerChooseNote')}</p>
        <div className="mx-5">
          <select className="custom-select" onChange={evt => setValue(evt.target.value)} value={value}>
            <option value={0}>{lang.get('ServerChoosePlaceholder')}</option>
            {partitions?.map((row, idx) => (
              <option key={idx} value={row.id}>
                {lang.get(`Server${row.id}`) || row.id} ({row.name})
              </option>
            ))}
          </select>
        </div>
      </div>
      <div className="my-3 text-center">
        <div className={cl('btn', 'btn-seco', { grayscale: value == 0 })} onClick={confirmClick}>{lang.get('ModalConfirm')}</div>
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (state) => {
  const partitions = state?.partitions

  if (!partitions?.length) throw Error('DontHaveIngameCharactor')
  if (partitions.length === 1) return partitions[0].id

  return showModal(ID, state)
}
