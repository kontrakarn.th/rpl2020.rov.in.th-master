import React from 'react'

import { gql, useQuery } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import Scrollbar from '../component/Scrollbar'
import { useAuth } from '../component/Auth'

export const ID = 'rule'

const RULE = gql`{rule}`

const SimplePopup = ({ close, data }) => {
  const { lang } = useAuth()
  const { data: { rule } = {} } = useQuery(RULE)
  return (
    <BaseModal close={close()}>
      <div className="title">{lang.get('RuleTitle')}</div>
      <Scrollbar>
        <div className="rule" dangerouslySetInnerHTML={{ __html: rule }} />
      </Scrollbar>
      <div className="btn__submit text-center mt-3" onClick={close()}>
        <span>ยืนยัน</span>
        {/* <a className="btn btn-seco" onClick={close()} href={lang.get('OpenGameUrl')} target={'_blank'}>{lang.get('OpenGame')}</a> */}
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default () => showModal(ID)
