import React, { useState, useRef, useEffect, useCallback } from 'react'

import cl from 'clsx'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { shareFB } from '../component/Common'

export const ID = 'livestream'

const Video = ({ className, youtubeId }) => {
  const elm = useRef()
  const plr = useRef()
  const [ready, setReady] = useState(false)

  useEffect(() => {
    if (!window.YT) {
      const ytScript = document.createElement('script')
      ytScript.src = 'https://www.youtube.com/iframe_api'
      document.head.appendChild(ytScript)

      window.onYouTubeIframeAPIReady = () => {
        setReady(true)
      }
    } else {
      setReady(true)
    }
  }, [])

  useEffect(() => {
    if (ready && window.YT) {
      const onPlayerReady = (event) => {
        event.target.playVideo()
      }

      plr.current = new YT.Player(elm.current, {
        videoId: youtubeId,
        events: {
          'onReady': onPlayerReady,
        }
      })
    }

    return () => {
      if (plr.current) {
        plr.current.destroy()
      }
    }
  }, [ready, youtubeId])


  return (
    <div className={cl('embed-responsive embed-responsive-16by9', className)}>
      <div className="embed-responsive-item" ref={elm} />
    </div>
  )
}

const SimplePopup = ({ close, data: { title, shareBtn, youtubeId, quote, hashtag } }) => {
  const shareClick = useCallback(evt => {
    return shareFB(quote, `https://www.youtube.com/watch?v=${youtubeId}`, hashtag).catch(console.error)
  }, [])

  return (
    <BaseModal close={close()}>
      <div className="title">{title}</div>
      <div className="livestream">
        <Video className="video" youtubeId={youtubeId} />
        <div className="image">
          <img src={'/images/livestream-gift.png'} />
        </div>
      </div>
      <div className="mt-3 text-center">
        <div className="btn btn-seco" style={{ fontSize: '120%' }} onClick={shareClick}>{shareBtn}</div>
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (data) => showModal(ID, data)
