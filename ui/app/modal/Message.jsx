import React from 'react'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { useAuth } from '../component/Auth'

export const ID = 'message'

const SimplePopup = ({ close, data: { title = 'Notice', messsage } }) => {
  const { lang } = useAuth()

  return (
    <BaseModal close={close()}>
      <div className="title">{title}</div>
      <div className="txt__secondfont flex-grow-1 d-flex flex-column justify-content-center text-center">
        {messsage}
      </div>
      <div className="btn__submit text-center mt-3" onClick={close()}>
        <span>ยืนยัน</span>
      </div>
      {/* <div className="my-3 text-center">
        <div className="btn btn-seco" onClick={close()}>{lang.get('ModalConfirm')}</div>
      </div> */}
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (title, messsage) => showModal(ID, { title, messsage })
