import React, { useMemo, useState, useCallback } from 'react'

import cl from 'clsx'
import { useQuery, useMutation } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'
import { shareFB, errorHandler, congrat } from '../component/Common'

import VODS from '../../gql/queries/vods.gql'
import CLAIM_VOD from '../../gql/mutations/claimVod.gql'

export const ID = 'Vod'

const Vod = ({ vod: { id, title, url, thumb, meta } }) => {
  const { state, lang } = useAuth()
  const [claimVod] = useMutation(CLAIM_VOD)

  const onClick = useCallback(async evt => {
    try {
      if (!state?.id) throw Error('Unauthorized')

      await shareFB(meta?.quote, url, meta?.hashtag)

      const { data: { claimVod: { point } } } = await claimVod({ variables: { vodId: id } })

      return congrat(<div dangerouslySetInnerHTML={{ __html: lang.get('CongratReceivePoints')?.replace('{}', point) }} />)

    } catch (err) {
      errorHandler(err)
    }
  }, [])

  const thumbClick = useCallback(evt => {
    window.open(url, '_blank')
  }, [])

  return (
    <div className="vod">
      <div class="embed-responsive embed-responsive-16by9" onClick={thumbClick}>
        <img class="embed-responsive-item vod-thumb" src={thumb} alt="thumb" />
      </div>
      <div className="vod-title">
        {title}
      </div>
      <div className="text-center">
        <svg viewBox={`${-150} ${-30} ${300} ${60}`} width={'12em'}>
          <g className={cl('btn-claim', { active: true })} onClick={onClick}>
            <path transform={`scale(1.25,1)`} d="M100,0 L80,24 L-80,24 L-100,0 L-80,-24 L80,-24 Z" />
            <text x={0} y={12} style={{ fontSize: '32px' }}>{lang.get('VodShare')}</text>
          </g>
        </svg>
      </div>
    </div>
  )
}

const SimplePopup = ({ close, data }) => {
  const { lang } = useAuth()
  const [tab, setTab] = useState(0)

  const { data: { vods } = {} } = useQuery(VODS)

  const arr = useMemo(() => {
    if (tab === 1) {
      return [...vods].sort((a, b) => b.priority - a.priority)
    }

    return vods
  }, [tab, vods])

  return (
    <BaseModal close={close()}>
      <div className="title">{lang.get('VodTitle')}</div>
      <div className="text-center mb-3">
        <div className={cl('btn', { nonactive: tab !== 0 })} onClick={evt => setTab(0)}>{lang.get('VodNewest')}</div>
        <div className={cl('btn', { nonactive: tab !== 1 })} onClick={evt => setTab(1)}>{lang.get('VodHotest')}</div>
      </div>
      <Scrollbar className="">
        <div className="vods">
          {arr?.map((row, idx) => <Vod key={idx} vod={row} />)}
        </div>
      </Scrollbar>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default () => showModal(ID)
