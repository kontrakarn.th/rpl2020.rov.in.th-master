import React, { useState, useMemo, useCallback } from 'react'

import cl from 'clsx'
import { useQuery } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { PlayerShop as Player } from '../component/Player'
import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'

import ROLES from '../../gql/queries/roles.gql'
import REGIONS from '../../gql/queries/regions.gql'
import PLAYERS from '../../gql/queries/players.gql'

export const ID = 'shop'

const SimplePopup = ({ close, data }) => {
  const { lang } = useAuth()
  const [hot, setHot] = useState(false)
  const [search, setSearch] = useState()
  const [regionId, setRegionId] = useState(0)
  const [select, setSelect] = useState(data?.choices)
  const [roleId, setRoleId] = useState(data?.roleId || 0)

  const onSearchChange = useCallback(evt => {
    setSearch(evt.target.value)
  }, [])

  const onRegionCompleted = useCallback(({ regions }) => {
    for (const region of regions) {
      if (region.isMyRegion) {
        setRegionId(region.id)
        break
      }
    }
  }, [setRegionId])

  const { data: { roles } = {} } = useQuery(ROLES)
  const { data: { players } = {} } = useQuery(PLAYERS)
  const { data: { regions } = {} } = useQuery(REGIONS, { onCompleted: onRegionCompleted })

  const role = useMemo(() => {
    return roles?.find(row => row.id === roleId)
  }, [roleId, roles])

  const arr = useMemo(() => {
    let arr = players?.filter(row => row.isActive) || []

    if (regionId) {
      arr = players.filter(row => row.regionId === regionId)
    }

    if (search) {
      arr = arr.filter(row => row?.name?.toLowerCase()?.search(search.toLowerCase()) >= 0)
    }

    arr = arr
      .map(row => {
        const role = row.roles?.find(row => row.roleId === roleId)
        return { ...row, role }
      })
      .filter(row => row.role)

    return arr?.sort((a, b) => {
      const x = a.role
      const y = b.role

      return hot ? y?.hot - x?.hot : y?.mvp - x?.mvp
    })
  }, [search, players, roleId, hot, regionId])

  const onConfirm = useCallback(evt => {
    close()()
    data?.setChoices(select)
  }, [select])

  return (
    <BaseModal close={close()}>
      <div className="flex-grow-1 d-flex flex-column">
        {/* <div className="player-search">
          <input
            type="text"
            placeholder={lang.get('ShopSearchPlaceholder')}
            value={search}
            onChange={onSearchChange}
          />
          <div className="btn-search"><i className="icomoon">search2</i></div>
        </div> */}
        {/* <div className="text-center">
          {regions?.map((region, idx) => (
            <div
              key={idx}
              className={cl('btn-tab', { active: region.id === regionId })}
              onClick={evt => setRegionId(v => v === region.id ? 0 : region.id)}
            >
              {region.name}
            </div>
          ))}
        </div> */}
        {/* <div className="text-center">
          <div
            className={cl('btn-filter', { active: hot })}
            onClick={evt => setHot(v => !v)}
          >
            {lang.get('ShopHotPick')}
          </div>
          {roles?.map((role, idx) => (
            <div
              key={idx}
              className={cl('btn-tab', { active: idx === roleId })}
              onClick={evt => setRoleId(idx)}
            >
              <i className="icoaov txt-secondary">{String.fromCharCode(role.code)}</i> {role.name}
            </div>
          ))}
        </div> */}
        <Scrollbar className="player_scroll">
          <div className="players">
            {arr?.length ? arr?.map((player, idx) => {
              return (
                <div
                  key={idx}
                  className={cl('player', { active: player.id === select[roleId] })}
                  onClick={evt => setSelect(arr => {
                    const newArr = [...arr]
                    newArr[roleId] = player.id
                    return newArr
                  })}
                >
                  <Player player={player} role={role} />
                </div>
              )
            }) : <div className="col-12 text-center">{lang.get('ShopEmptyPlayer')}</div>}
          </div>
        </Scrollbar>
        <div className="text-center mt-3">
          <div className="mb-1">
            {lang.get('ShopScrollNote')}
          </div>
          <i className="icomoon" style={{ fontSize: '1em' }}>down10</i>
        </div>
      </div>
      <div className="btn__submit text-center mt-3" onClick={onConfirm}>
        <span>{lang.get('ShopConfirm')}</span>
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (data) => showModal(ID, data)
