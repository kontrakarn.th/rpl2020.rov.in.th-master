import React, { useState, useMemo, useCallback } from 'react'

import format from 'date-fns/format'
import { useQuery } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import showUserInfo from './UserInfo'
import Loading from '../component/Loading'
import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'

import ITEM from '../../gql/queries/item.gql'
import MISSIONS from '../../gql/queries/missions.gql'
import HISTORIES from '../../gql/queries/histories.gql'
import PERSON_MILESTONES from '../../gql/queries/personMilestones.gql'
import REGION_MILESTONES from '../../gql/queries/regionMilestones.gql'

export const ID = 'history'

const LIMIT = 50

const History = ({ history: { id, type, content, point, createdAt } }) => {
  const { lang } = useAuth()

  const { data: { missions } = {} } = useQuery(MISSIONS, { skip: type !== 0 })
  const { data: { milestones: rMilestones } = {} } = useQuery(REGION_MILESTONES, { skip: type !== 1 })
  const { data: { milestones: pMilestones } = {} } = useQuery(PERSON_MILESTONES, { skip: type !== 2 })
  const { data: { item: cItem } = {} } = useQuery(ITEM, { skip: type !== 6, variables: { itemId: content } })

  const category = useMemo(() => {
    switch (type) {
      case 0:
        return missions?.find(row => row.id === content)?.name || '...'
      case 1:
        return lang.get('HistoryRegionClaim') || '...'
      case 2:
        return lang.get('HistoryPersonClaim') || '...'
      case 3:
        return lang.get('HistoryShareVod') || '...'
      case 4:
        return lang.get('HistoryConfirmMvpVote') || '...'
      case 5:
        return lang.get('HistoryCorrectMvpVote') || '...'
      case 6:
        return lang.get('HistoryReceiveLuckyGift') || '...'
      default:
        return type
    }
  }, [lang, missions,])

  const body = useMemo(() => {
    switch (type) {
      case 0:
      case 3:
      case 5:
        return `${point} ${lang.get('QuestPoints')}`
      case 1:
        return rMilestones?.find(row => row.id === content)?.item?.name || '...'
      case 2:
        return pMilestones?.find(row => row.id === content)?.item?.name || '...'
      case 4:
        return ''
      case 6:
        return <>{cItem?.name} <div onClick={showUserInfo} style={{ color: 'white' }}>{lang.get('HistoryUpdateUserInfo')} <i className="icomoon">pencil5</i></div></>
      default:
        return content
    }
  }, [lang, rMilestones, pMilestones, cItem])

  return (
    <tr>
      <td>{format(createdAt,  'DD.MM.YYYY HH:mm:ss')}</td>
      <td>{category}</td>
      <td><b>{body}</b></td>
    </tr>
  )
}

const SimplePopup = ({ close, data }) => {
  const { lang } = useAuth()
  const [more, setMore] = useState(true)

  const { data: { histories = [] } = {}, loading, fetchMore } = useQuery(HISTORIES, {
    variables: { offset: 0, limit: LIMIT },
    fetchPolicy: 'cache-and-network',
  })

  const onYReachEnd = useCallback(evt => {
    if (loading) return false
    if (!more) return false

    fetchMore({
      variables: {
        offset: histories.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev

        if (!fetchMoreResult.histories?.length) {
          setMore(false)
          return prev
        }

        return Object.assign({}, prev, {
          histories: [...prev.histories, ...fetchMoreResult.histories]
        })
      }
    })

  }, [more, fetchMore, loading, histories?.length])

  return (
    <BaseModal close={close()}>
      <div className="title">{lang.get('HistoryTitle')}</div>
      <Scrollbar onYReachEnd={onYReachEnd}>
        <table>
          <thead>
            <tr>
              <th>{lang.get('HeadTime')}</th>
              <th>{lang.get('HeadCat')}</th>
              <th>{lang.get('HeadContent')}</th>
            </tr>
          </thead>
          <tbody>
            {histories?.map((row, idx) => <History key={idx} history={row} />)}
          </tbody>
        </table>
        {loading && <Loading />}
      </Scrollbar>
      <div className="btn__submit text-center mt-3" onClick={close()}>
        <span>ยืนยัน</span>
        {/* <a className="btn btn-seco" onClick={close()} href={lang.get('OpenGameUrl')} target={'_blank'}>{lang.get('OpenGame')}</a> */}
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default () => showModal(ID)
