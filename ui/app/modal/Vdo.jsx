import React from 'react'
import BaseModal from './BaseModal'
import { modal, showModal } from './index'
export const ID = 'vdo'

const SimplePopup = ({ close }) => {
  return (
    <BaseModal close={close()}>
      <div className="txt__secondfont flex-grow-1 d-flex flex-column justify-content-center">
        <div className="videoWrapper">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLCm7VHDJdtZLfm9O0jvRUH-_vkfdUDICw&autoplay=1&mute=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>      
        </div>
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (meta) => showModal(ID, meta)
