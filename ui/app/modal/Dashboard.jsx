import React, { useState, useCallback } from 'react'

import format from 'date-fns/format'
import { useQuery } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { mlines } from '../component/Svg'
import Loading from '../component/Loading'
import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'
import { svg2img } from '../component/SvgUtils'
import { shareFB, upload, randName, sleep, errorHandler } from '../component/Common'

import DASHBOARDS from '../../gql/queries/dashboards.gql'

export const ID = 'dashboard'

const LIMIT = 50

const Dashboard = ({ idx, dashboard: { charName, point, lastTime } }) => {
  const { lang } = useAuth()

  return (
    <tr>
      <td>{idx + 1}</td>
      <td>{charName || lang.get('Noname')}</td>
      <td>{point} {lang.get('QuestPoints')}</td>
      <td>{format(lastTime, lang.get('DateTimeFormat') || 'DD.MM HH:mm:ss')}</td>
    </tr>
  )
}

const RankLayout = ({ state }) => {
  const { lang } = useAuth()

  return (
    <svg viewBox="-600 -315 1200 630" width={1200} height={630}>
      <image x={-600} y={-315} width={1200} height={630} xlinkHref="/images/bg-thumb.jpg" />
      <g
        fontFamily={`"ALP GT America Compressed", "PSL Empire Pro", "PingFang SC", sans-serif`}
        fontWeight={700} fill="white" textAnchor="middle"
      >
        <text y={-285} fontSize={45}  >
          <tspan x={0} dy={45} dangerouslySetInnerHTML={{
            __html: lang.get('ThumbCongrat')
              ?.toUpperCase()
              ?.replace('{NAME}', `<tspan fill="#cd2424">${
                (state?.partitions?.[0].name || lang.get('Noname'))?.toUpperCase()
                }</tspan>`),
          }} />
          <tspan x={0} dy={50}>{lang.get('ThumbRank')?.toUpperCase()}</tspan>
        </text>
        <text x={0} y={120} fontSize={360} fill="#24cdb3" >
          {state?.rankNum || '???'}
        </text>
        <g fontSize={60} dangerouslySetInnerHTML={{ __html: mlines(lang.get('ThumbEventName') || '', 65, 0, 220) }} />
      </g>
    </svg>
  )
}

const SimplePopup = ({ close, data }) => {
  const { state, lang } = useAuth()
  const [more, setMore] = useState(true)

  const { data: { dashboards = [] } = {}, loading, fetchMore } = useQuery(DASHBOARDS, {
    variables: { offset: 0, limit: LIMIT },
    fetchPolicy: 'cache-and-network',
  })

  const shareClick = useCallback(async evt => {
    if (!state?.id) return errorHandler(Error('Unauthorized'))

    const filename = randName()

    const uploadFile = async () => {
      const thumb = await svg2img(<RankLayout state={state} />)
      return upload(filename, thumb)
    }

    const shareImage = async () => {
      await sleep(960)
      return shareFB(lang.get('RankShareQuote'), `${location.protocol}//${location.hostname}/fb/${filename}`)
    }

    return Promise.all([uploadFile(), shareImage()]).catch(console.error)
  }, [state, state?.id, lang])

  const onYReachEnd = useCallback(evt => {
    if (loading) return false
    if (!more) return false

    fetchMore({
      variables: {
        offset: dashboards.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev

        if (!fetchMoreResult.dashboards?.length) {
          setMore(false)
          return prev
        }

        return Object.assign({}, prev, {
          dashboards: [...prev.dashboards, ...fetchMoreResult.dashboards]
        })
      }
    })

  }, [more, fetchMore, loading, dashboards?.length])

  return (
    <BaseModal close={close()}>
      <div className="title">{lang.get('DashboardTitle')}</div>
      <div className="btn mb-3 mx-3">
        {lang.get('DashboardYourRank')}:&nbsp;
        {state?.rankNum || '???'}&nbsp;-&nbsp;
        {state?.point || 0} {lang.get('QuestPoints')}
      </div>
      <Scrollbar onYReachEnd={onYReachEnd}>
        <table>
          <thead>
            <tr>
              <th>{lang.get('HeadOrder')}</th>
              <th>{lang.get('HeadCharName')}</th>
              <th>{lang.get('HeadPoint')}</th>
              <th>{lang.get('HeadTime')}</th>
            </tr>
          </thead>
          <tbody>
            {dashboards?.map((row, idx) => <Dashboard key={idx} idx={idx} dashboard={row} />)}
          </tbody>
        </table>
        {loading && <Loading />}
      </Scrollbar>
      <div className="text-center mt-3">
        <div className="btn btn-seco" onClick={shareClick}>{lang.get('RankShare')}</div>
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default () => showModal(ID)
