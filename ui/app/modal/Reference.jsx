import React, { useRef, useMemo, useState, useCallback } from 'react'

import qs from 'querystring'
import format from 'date-fns/format'
import { useQuery } from '@apollo/client'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import Loading from '../component/Loading'
import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'
import { congrat, errorHandler, shareFB } from '../component/Common'

import REFERENCES from '../../gql/queries/references.gql'

export const ID = 'reference'

const LIMIT = 50

const Reference = ({ idx, reference: { id, type, charName, createdAt } }) => {
  const { lang } = useAuth()

  return (
    <tr>
      <td>{idx + 1}</td>
      <td>{charName || lang.get('Noname')}</td>
      <td>{format(createdAt, lang.get('DateTimeFormat') || 'DD.MM.YYYY HH:mm:ss')}</td>
    </tr>
  )
}

const SimplePopup = ({ close, data }) => {
  const elm = useRef()
  const { state, lang } = useAuth()
  const [more, setMore] = useState(true)

  const link = useMemo(() => `${location.protocol}//${location.host}?refcode=${state?.referCode}`, [state])

  const { data: { references = [] } = {}, loading, fetchMore } = useQuery(REFERENCES, {
    variables: { offset: 0, limit: LIMIT },
    fetchPolicy: 'cache-and-network',
  })

  const onYReachEnd = useCallback(evt => {
    if (loading) return false
    if (!more) return false

    fetchMore({
      variables: {
        offset: references.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        if (!fetchMoreResult) return prev

        if (!fetchMoreResult.references?.length) {
          setMore(false)
          return prev
        }

        return Object.assign({}, prev, {
          references: [...prev.references, ...fetchMoreResult.references]
        })
      }
    })

  }, [more, fetchMore, loading, references?.length])

  const copyClick = useCallback(evt => {
    try {
      const node = elm.current

      node.select()
      document.execCommand('copy')
      node.blur()

      return congrat(
        lang.get('CongratCopySuccess')
      )
    } catch (err) {
      return errorHandler(err)
    }
  }, [lang])

  const fbClick = useCallback(async () => {
    return shareFB(data?.quote, link, data?.quote).catch(console.error)
  }, [link])

  const meClick = useCallback(() => {
    if (/iPad|iPhone|iPod/.test(navigator.userAgent)) {
      window.open(`fb-messenger://share?${qs.stringify({
        link,
      })}`, '_blank')
    } else {
      FB.ui({
        method: 'send',
        link: link,
      })
    }

  }, [link])

  return (
    <BaseModal close={close()}>
      {/* <div className="text-center mb-3">
        <svg viewBox="0 0 300 190" style={{ width: '9em' }}>
          <rect x={0} y={0} width="100%" height="100%" fill="#cd2424" mask="url(#logo)" />
        </svg>
      </div> */}
      <div className="txt__secondfont flex-grow-1 d-flex flex-column justify-content-center">
        <div className="text-uppercase mb-1 text-center">
          {lang.get('ReferYourLink')}
        </div>
        <div className="section-copy">
          <input ref={elm} type="text" value={link} onChange={evt => { }} />
          <div className="btn-copy" onClick={copyClick}>copy</div>
        </div>
        {/* <div>
          <span className="text-uppercase">
            {lang.get('ReferShare')}:&nbsp;&nbsp;
          </span>
          <div className="btn-share" onClick={fbClick}><img src="/images/fb-ico.png" /></div>
          <div className="btn-share" onClick={meClick}><img src="/images/me-ico.png" /></div>
        </div>
        <div className="text-center">
          <svg viewBox="-600 -50 1200 110" style={{ width: '90%' }}>
            <defs>
              <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
                <stop offset="0%" stopColor="#00000000" />
                <stop offset="40%" stopColor="#ffffff" />
                <stop offset="60%" stopColor="#ffffff" />
                <stop offset="100%" stopColor="#00000000" />
              </linearGradient>
              <mask id="mask1">
                <rect x={-600} y={-5} width={'100%'} height={10} fill="white" />
                <rect x={-40} y={-40} width={80} height={100} fill="black" />
              </mask>
            </defs>
            <rect x={-600} y={0} width={'100%'} height={2} fill="url(#grad1)" mask="url(#mask1)" />
            <rect x={-40} y={-40} width={80} height={100} fill="#cd2424" mask="url(#apl)" />
          </svg>
        </div>
        <div className="text-uppercase font-weight-bold">{lang.get('ReferWhoHelpedYou')}</div>
        <div className="text-center mb-2">
          <svg viewBox="-600 0 1200 20" style={{ width: '90%' }}>
            <rect x={-600} y={18} width={'100%'} height={2} fill="url(#grad1)" />
          </svg>
        </div> */}
      </div>
      {/* <Scrollbar onYReachEnd={onYReachEnd}>
        <table>
          <thead>
            <tr>
              <th>{lang.get('HeadOrder')}</th>
              <th>{lang.get('HeadCharName')}</th>
              <th>{lang.get('HeadTime')}</th>
            </tr>
          </thead>
          <tbody>
            {references?.map((row, idx) => <Reference key={idx} idx={idx} reference={row} />)}
          </tbody>
        </table>
        {loading && <Loading />}
      </Scrollbar> */}
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (meta) => showModal(ID, meta)
