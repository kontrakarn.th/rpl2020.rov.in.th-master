
import cl from 'clsx'
import React from 'react'

import css from './modal.local.scss'

const BaseModal = ({ className, close, children }) => {
  const ccn = cl('modal', css.container, css.fade, css.shown)
  return (
    <div className={ccn} onClick={evt => {
      if (evt.target.className === ccn) {
        if (close) close(evt)
        evt.stopPropagation()
        evt.preventDefault()
      }
    }}>
      <div className={cl('modal-content', css.popup, css.show, className)}>
        <div className="btn-close" onClick={evt => close()}>
          X
          {/* <svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0" y="0" viewBox="0 0 224.512 224.512">
            <polygon className="shape" points="224.507,6.997 217.521,0 112.256,105.258 6.998,0 0.005,6.997 105.263,112.254 
              0.005,217.512 6.998,224.512 112.256,119.24 217.521,224.512 224.507,217.512 119.249,112.254 	"/>
          </svg> */}
        </div>
        {children}
      </div>
    </div >
  )
}

export default BaseModal
