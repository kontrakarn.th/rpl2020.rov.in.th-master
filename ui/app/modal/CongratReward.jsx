import React, { useCallback } from 'react'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

import { mlines } from '../component/Svg'
import { useAuth } from '../component/Auth'
import { svg2img } from '../component/SvgUtils'
import { randName, upload, shareFB, sleep } from '../component/Common'

import showUserInfo from './UserInfo'

export const ID = 'congrat'

const Layout = ({ state, item }) => {
  const { lang } = useAuth()

  return (
    <svg viewBox="-600 -315 1200 630" width={1200} height={630}>
      <image x={-600} y={-315} width={1200} height={630} xlinkHref="/images/bg-thumb.jpg" />
      <g
        fontFamily={`"ALP GT America Compressed", "PSL Empire Pro", "PingFang SC", sans-serif`}
        fontWeight={700} fill="white" textAnchor="middle"
      >
        <text y={-285} fontSize={45}  >
          <tspan x={0} dy={45} dangerouslySetInnerHTML={{
            __html: lang.get('ThumbCongrat')
              ?.toUpperCase()
              ?.replace('{NAME}', `<tspan fill="#cd2424">${
                (state?.partitions?.[0].name || lang.get('Noname'))?.toUpperCase()
                }</tspan>`),
          }} />
          <tspan x={0} dy={50}>{lang.get('ThumbRank')?.toUpperCase()}</tspan>
        </text>
        <image x={-150} y={-155} width={300} height={300} xlinkHref={item.icon} />
        <g fontSize={60} dangerouslySetInnerHTML={{ __html: mlines(lang.get('ThumbEventName') || '', 65, 0, 220) }} />
      </g>
    </svg>
  )
}

const SimplePopup = ({ close, data: { id, name, icon, gameType } }) => {
  const { state, lang } = useAuth()

  const onClose = useCallback(evt => {
    close()(evt)
    if (gameType === 128) showUserInfo()
  }, [close, gameType])

  const shareClick = useCallback(async evt => {
    const filename = randName()

    const uploadFile = async () => {
      const thumb = await svg2img(<Layout state={state} item={{ name, icon }} />)
      return upload(filename, thumb)
    }

    const shareImage = async () => {
      await sleep(960)
      return shareFB(lang.get('MerchandiseShareQuote'), `${location.protocol}//${location.hostname}/fb/${filename}`)
    }

    return Promise.all([uploadFile(), shareImage()]).catch(console.error)
  }, [state, lang])

  return (
    <BaseModal close={onClose}>
      <div className="title">{lang.get('ModalCongrat')}</div>
      <div className="flex-grow-1 d-flex flex-column justify-content-center text-center">
        <div>
          <img className="mb-4" style={{ width: '10em' }} src={icon} /><br />
          <div dangerouslySetInnerHTML={{ __html: lang.get('CongratReward')?.replace('{item}', name) }} />
          <div className="mt-3">{lang.get(gameType === 128 ? 'CongratMerchandiseNote' : 'CongratIngameNote')}</div>
        </div>
      </div>
      <div className="my-3 text-center">
        <div className="btn btn-seco" onClick={onClose}>{lang.get('ModalConfirm')}</div>
        {gameType === 128 && <div className="btn btn-seco" onClick={shareClick}>{lang.get('MerchandiseShare')}</div>}
        {/*  bnbngh<a className="btn btn-seco grayscale" href={lang.get('OpenGameUrl')} target={'_blank'}>{lang.get('OpenGame')}</a> */}
      </div>
    </BaseModal>
  )
}

modal(ID)(SimplePopup)

export default (item) => showModal(ID, item)
