import React from 'react'

import BaseModal from './BaseModal'
import { modal, showModal } from './index'

export const ID = 'simple-popup'

const SimplePopup = ({ close, data }) => (
  <BaseModal close={close()}>
    <div className="title">title</div>
    <p>{data}</p>
    <div>
      <div className="btn btn-seco" onClick={close()}>xác nhận</div>
    </div>
  </BaseModal>
)

modal(ID)(SimplePopup)

export default (data) => showModal(ID, data)
