import React, { useState, useEffect } from 'react'

const isIOS = /iphone|ipod|ipad/i.test(navigator.userAgent || navigator.vendor)

const replaceRvhWithPx = (propertyStringValue, windowHeight) => {
  const rvhRegex = /(\d+(\.\d*)?)rvh(?!\w)/g
  return propertyStringValue.replace(
    rvhRegex,
    (_, rvh) => `${(windowHeight * parseFloat(rvh)) / 100}px`
  )
}

const convertStyle = (usedStyle, windowHeight) => {
  if (typeof usedStyle !== 'object' || Array.isArray(usedStyle))
    throw Error(`style (the first argument) must be an object or undefined`)
  if (typeof windowHeight !== 'number' || windowHeight < 0)
    throw Error('Second argument (windowHeight) must be a non-negative number')

  const convertedStyle = {}
  for (const key of Object.keys(usedStyle)) {
    convertedStyle[key] = typeof usedStyle[key] === 'string' ? replaceRvhWithPx(usedStyle[key], windowHeight) : usedStyle[key]
  }
  return convertedStyle
}

const Div100vh = ({ component, ...props }) => {
  const [style, setStyle] = useState({})

  useEffect(() => {
    const handler = () => {
      const innerHeight = isIOS ? window.innerHeight : (document?.documentElement?.clientHeight || window.innerHeight)
      setStyle(convertStyle(props.style, innerHeight))
      document.documentElement.style.setProperty('--vh', `${innerHeight / 100}px`)
    }
    const oriHandler = () => setTimeout(handler, 200)

    handler()
    window.addEventListener('resize', handler)
    window.addEventListener('orientationchange', oriHandler)

    return () => {
      window.removeEventListener('resize', handler)
      window.removeEventListener('orientationchange', oriHandler)
    }
  }, [props.style])

  return React.createElement(component, { ...props, style })
}

Div100vh.defaultProps = {
  component: 'div',
  style: { height: '100rvh' },
}

export default Div100vh
