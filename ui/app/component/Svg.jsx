import React from 'react'

export const Image = ({ x, y, size, bottom, ...props }) => {
  return <image x={x - 0.5 * size} y={y - bottom * size} width={size} height={size} {...props} />
}

Image.defaultProps = {
  x: 0,
  y: 0,
  size: 100,
  bottom: 0.5,
}

export const Apl = ({ x, y, size, ...props }) => {
  return <rect x={x - 0.35 * size} y={y - 0.4 * size} width={0.7 * size} height={size} {...props} />
}
      {/* <rect x="-28" y="-21" width="56" height="400" class="" mask="url(#apl)"></rect> */}


Apl.defaultProps = {
  x: 0,
  y: 0,
  size: 100,
  mask: 'url(#apl)',
}

export const mlines = (lines, lineHeight, x = 0, y = 0) => lines
  .toUpperCase()
  .split('<BR>')
  .map((line, idx) => `<text x="${x}" y="${y + idx * lineHeight}">${line.replace(/<r>(.+)<\/r>/i, `<tspan fill="#cd2424">$1</tspan>`)}</text>`)
  .join('')

export const SvgDefs = () => {
  return (
    <defs>
      <mask id="apl"
        maskUnits="objectBoundingBox"
        maskContentUnits="objectBoundingBox"
        x={0} y={0} width={1} height={1}
      >
        <image x={0.1} y={0.15} width={0.8} height={0.5} xlinkHref="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_dot_white.png" preserveAspectRatio="none" />
      </mask>
      <mask id="logo"
        maskUnits="objectBoundingBox"
        maskContentUnits="objectBoundingBox"
        x={0} y={0} width={1} height={1}
      >
        <image x={0.1} y={0} width={1} height={1} xlinkHref="/images/logo.png" preserveAspectRatio="none" />
      </mask>
      <filter id="shadow" x="0" y="0" width="110%" height="150%">
        <feOffset result="offOut" in="SourceAlpha" dx="0" dy="0" />
        <feGaussianBlur result="blurOut" in="offOut" stdDeviation="5" />
        <feBlend in="SourceGraphic" in2="blurOut" mode="normal" />
      </filter>
    </defs>
  )
}
