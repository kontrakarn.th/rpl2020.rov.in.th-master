import Canvg from 'canvg'
import Dataloader from 'dataloader'

import React, { useEffect, useRef, useState } from 'react'

const hsvg = {}
const sleep = ms => new Promise(r => setTimeout(r, ms))

const DEFAULT_FONTS = [
  { family: 'PSL Empire Pro', format: 'woff', url: '/fonts/PSLEmpireProBold.woff' },
  { family: 'ALP Bebas Neue Pro', format: 'woff', url: '/fonts/ALPBebasNeueProBold.woff' },
  { family: 'ALP GT America Compressed', format: 'woff', url: '/fonts/ALPGTAmericaCompressedBold.woff' },
]

const uriToBlob = uri => {
  const byteString = window.atob(uri.split(',')[1])
  const mimeString = uri.split(',')[0].split(':')[1].split(';')[0]
  const buffer = new ArrayBuffer(byteString.length)
  const intArray = new Uint8Array(buffer)
  for (let i = 0; i < byteString.length; i++) {
    intArray[i] = byteString.charCodeAt(i)
  }
  return new Blob([buffer], { type: mimeString })
}

const UrlLoader = new Dataloader(async ([src]) => {
  try {
    let resp
    try {
      resp = await fetch(src, { credentials: 'omit' })
    } catch {
      if (src.slice(0, 4) === 'http') {
        resp = await fetch(`/fb/px?${require('querystring').stringify({ p: src })}`, { credentials: 'omit' })
      } else {
        throw Error('')
      }
    }
    const blob = await resp.blob()

    return await new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onloadend = () => resolve([reader.result])
      reader.onerror = reject
      reader.readAsDataURL(blob)
    })
  } catch (err) {
    console.error(err)
    return ['']
  }
}, { batch: false })

const CheckUrlLoader = new Dataloader(async ([src]) => {
  try {
    if (src.slice(0, 4) === 'http') {
      await fetch(src, { credentials: 'omit' })
    }

    return [src]
  } catch (err) {
    console.error(err)
    return [`/fb/px?${require('querystring').stringify({ p: src })}`]
  }
}, { batch: false })

const xmlSerializer = new XMLSerializer()

export const svg2img = async (data, {
  css = '',
  blob = true,
  quantity = '0.33',
  type = 'image/jpeg',
  fonts = DEFAULT_FONTS,
} = {}) => {
  let el
  let svgString

  if (React.isValidElement(data)) {
    hsvg.setChild(data)
    await sleep(30)
    el = hsvg.node.firstChild.cloneNode(true)

    await Promise.all(Array.from(el.querySelectorAll('image')).map(async image => {
      const href = image.getAttributeNS('http://www.w3.org/1999/xlink', 'href')

      if (!href) return null

      const url = await CheckUrlLoader.load(href)
      image.setAttributeNS('http://www.w3.org/1999/xlink', 'href', url)
      return true
    }))

    svgString = xmlSerializer.serializeToString(el)
  } else if (typeof data === 'string') {
    svgString = data
  } else if (el instanceof SVGElement) {
    svgString = xmlSerializer.serializeToString(data)
  } else {
    throw Error('InvalidData')
  }

  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  canvas.width = 1200
  canvas.height = 630

  const canvg = await Canvg.from(ctx, svgString, {
    ignoreMouse: true,
    enableRedraw: false,
    ignoreAnimation: true,
    anonymousCrossOrigin: true,
  })

  await canvg.render()

  const uri = canvas.toDataURL(type, quantity)

  // const elm = document.createElement('img')
  // elm.src = uri
  // document.body.appendChild(elm)

  return blob ? uriToBlob(uri) : uri
}

export const svg2img2 = async (data, {
  css = '',
  blob = true,
  quantity = '0.33',
  type = 'image/jpeg',
  fonts = DEFAULT_FONTS,
} = {}) => {
  let el

  if (React.isValidElement(data)) {
    hsvg.setChild(data)
    await sleep(30)
    el = hsvg.node.firstChild.cloneNode(true)
  } else if (typeof data === 'string') {
    hsvg.node.innerHTML = data
    el = hsvg.node.firstChild.cloneNode(true)
  } else if (el instanceof SVGElement) {
    el = data.cloneNode(true)
  } else {
    throw Error('InvalidData')
  }

  await Promise.all(Array.from(el.querySelectorAll('image')).map(async image => {
    const href = image.getAttributeNS('http://www.w3.org/1999/xlink', 'href')

    if (!href) return null

    const url = await UrlLoader.load(href)
    image.setAttributeNS('http://www.w3.org/1999/xlink', 'href', url)
    return true
  }))

  const fontCss = await Promise.all(fonts
    .map(async ({ family, format, url }) =>
      `@font-face{font-family:'${family}';src:url('${await UrlLoader.load(url)}') format('${format}');}`
    ))
    .then(arr => arr.join(''))

  const defs = document.createElement('defs')
  const style = document.createElement('style')

  style.setAttribute('type', 'text/css')
  style.innerHTML = fontCss + css

  defs.appendChild(style)

  el.insertBefore(defs, el.firstChild)

  const svgString = xmlSerializer.serializeToString(el)
  const svgBlob = new Blob([svgString], { type: 'image/svg+xml' })
  const svgBlobUrl = URL.createObjectURL(svgBlob)

  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')

  return new Promise((resolve, reject) => {
    const im = new Image()
    im.onload = () => {
      try {
        canvas.width = im.width
        canvas.height = im.height
        ctx.drawImage(im, 0, 0)


        const uri = canvas.toDataURL(type, quantity)
        resolve(blob ? uriToBlob(uri) : uri)

      } catch (err) {
        reject(err)
      }
    }
    im.onerror = reject
    im.src = svgBlobUrl
  })
}

export default () => {
  const node = useRef()
  const [child, setChild] = useState()

  useEffect(() => {
    Object.assign(hsvg, {
      setChild,
      node: node.current,
    })
  }, [node.current])

  return (<div ref={node} style={{ display: 'none', height: 'auto' }} children={child} />)
}
