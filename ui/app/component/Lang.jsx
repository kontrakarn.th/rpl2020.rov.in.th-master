import React, { useMemo } from 'react'

import { gql, useQuery } from '@apollo/client'

export const LANG_QUERY = gql`
query lang($languageId: UInt16!) {
  entities(languageId: $languageId)
}
`

export const useLang = (languageId, options) => {
  const { data } = useQuery(LANG_QUERY, { variables: { languageId }, ...options })

  return useMemo(() => {
    return new Map(Object.entries(data?.entities))
  }, [data?.entities])
}

const Lang = ({ children }) => {
  const { data } = useQuery(LANG_QUERY)
  return children
}

export default Lang
