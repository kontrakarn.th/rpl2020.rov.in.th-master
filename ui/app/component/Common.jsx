import React from 'react'
import uuid from 'uuid/v4'

import { META, LANG } from './Auth'
import showMessage from '../modal/Message'
export { default as congratReward } from '../modal/CongratReward'

export const randName = () => {
  const buff = Buffer.alloc(16)
  uuid({}, buff)
  return buff.toString('hex')
}

export const upload = (filename, file) => {
  const form = new FormData()
  form.append('file', file)

  return fetch(`/fb/up/${filename}`, {
    method: 'POST',
    body: form,
  })
}

export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
export const format = new Intl.NumberFormat('vi-VN', { style: 'decimal' }).format

export const countBits = (n = 0) => {
  let count = 0
  while (n) {
    n &= (n - 1)
    count++
  }
  return count
}

export const shareFB = (
  quote = '',
  href = '',
  hashtag = '',
) => new Promise((resolve, reject) => {
  FB.ui({
    method: 'share',
    quote: quote || META.fbQuote,
    hashtag: hashtag || META.fbHashtag,
    href: href || `${location.protocol}//${location.hostname}`,
  }, (response) => {
    if (response && !response.error_message) {
      resolve(true)
    } else {
      reject(Error('FacebookShareError'))
    }
  })

  const ua = navigator.userAgent || navigator.vendor || window.opera
  if (ua.indexOf('FBAN') > -1 || ua.indexOf('FBAV') > -1) {
    resolve(true)
  }
})

export const congrat = (message) => showMessage(
  LANG.get('ModalCongrat'), (
  <div className="my-5">
    {message}
  </div>
))

export const errorHandler = (error) => {
  console.error(error)

  let message

  const errMessage = error?.graphQLErrors?.[0]?.message ?? error?.message
  switch (errMessage) {
    case 'Unauthorized':
      message = (
        <div>
          <p>
            {LANG.get('ErrorUnauthorized')}
          </p>
          <p>
            <a href="/connect/garena/facebook"><img style={{ width: '2em' }} src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_facebook.png" /></a>
            &nbsp;
            <a href="/connect/garena"><img style={{ width: '2em' }} src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_garena.png" /></a>
          </p>
        </div>
      )
      break

    default: {
      const str = LANG.get('Error' + errMessage)
      message = str || (
        <>
          {LANG.get('ErrorUndefined')}<br />
          [{errMessage}]
        </>
      )
    }
  }

  return showMessage(
    LANG.get('ModalNotice'),
    message,
  )
}
