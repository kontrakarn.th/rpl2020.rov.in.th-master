import React from 'react'
import { useAuth } from './Auth'

const Login = () => {
  const { lang, loading, user, state } = useAuth()
  if (loading) return null

  if (!user) return (
    <div className="login">
      {lang.get('LogIn')}:
      <a href="/connect/garena/facebook"><img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_facebook.png" /></a>
      <a href="/connect/garena"><img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_garena.png" /></a>
    </div>
  )

  return (
    <div className="login">
      <a href="/connect/garena/logout">
        <span className="username">
          {lang.get('Hello')} {state?.partitions?.[0]?.name || user?.name || lang.get('Noname')}
        </span>
        <img className="rounded-circle" src={user?.avatar} />
        &nbsp;|&nbsp;
        <span className="txt-secondary">{lang.get('LogOut')}</span>
      </a>
    </div>
  )
}

const Header = () => {
  const { lang } = useAuth()

  return (
    <header>
      {/* <div>
        <a className="btn btn-opengame" href={lang.get('OpenGameUrl')} target={'_blank'}>{lang.get('OpenGame')}</a>
      </div> */}
      <Login />
      <div className="logo">
        <img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/logo.png" alt="logo" />
      </div>
    </header>
  )
}

export default Header
