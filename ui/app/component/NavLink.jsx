import React, { useMemo } from 'react'

import cl from 'clsx'
import { useLocation, useRouter } from 'wouter'

import Link from './Link'

const NavLink = ({ exact, className, activeClassName, ...props }) => {
  const router = useRouter()
  const [path] = useLocation()

  const isActive = useMemo(() => {
    let pattern = props?.to
    if (pattern?.charAt(pattern?.length - 1) === '/') {
      pattern = pattern.slice(0, -1)
    }

    if (!exact) pattern += '/:rest?'

    return router?.matcher(pattern, path)?.[0]
  }, [router, path, props?.to, exact])

  return <Link className={cl(className, isActive && activeClassName)} {...props} />
}

NavLink.defaultProps = {
  exact: false,
  activeClassName: 'active',
}

export default NavLink
