import React, { useRef, useEffect } from 'react'

import { gql, useQuery } from '@apollo/client'


const COMMENTS_QUERY = gql`
  query comments{
    comments
  }`

const ADD_COMMENT_MUTATION = gql`
  mutation addComment($comment: String){
    addComment(comment: $comment)
  }`

const COMMENTS_SUBSCRIPTION = gql`
  subscription commentAdded{
    commentAdded
  }`

const Comment = () => {
  const textRef = useRef()
  const { data: { comments = [] } = {}, subscribeToMore, client } = useQuery(COMMENTS_QUERY)

  useEffect(() => subscribeToMore({
    document: COMMENTS_SUBSCRIPTION,
    updateQuery: ({ comments }, { subscriptionData: { data: { commentAdded } } }) => ({ comments: [commentAdded, ...comments] })
  }), [])

  return (
    <>
      <ul>{comments.map((comment, id) => <li key={id}>{comment}</li>)}</ul>
      <input type="text" ref={textRef} />
      <button onClick={() => client.mutate({
        mutation: ADD_COMMENT_MUTATION,
        variables: { comment: textRef.current.value }
      })}> Add</button>
    </>
  )
}

export default React.memo(Comment)
