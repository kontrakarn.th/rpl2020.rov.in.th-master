import React, { useMemo } from 'react'

import cl from 'clsx'

import { mlines } from './Svg'
import { useAuth } from './Auth'

const PlayerAvatar = ({ player, role, ...props }) => {
  return (
    <g {...props}>
      {/* <rect x={0} y={0} width={300} height={250} fill="#383838" /> */}
      <text className="playername" x={150} y={10} fontSize={30} fill="red" textAnchor="middle">{player?.name ?? '???'}</text>
      <image x={0} y={0} width={297} height={287} xlinkHref={"//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/player_bg.png"} />
      <image x={40} y={24} width={218} height={198} xlinkHref={player?.icon ? player.icon : '//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/player_annonymus.png'} />
      <path d="M0,285 L0,220 295,220 295,285" fill="#0d46bb" />     
      <text className="playername" x={150} y={266} fontSize={30} fill="white" textAnchor="middle">{role?.name}</text>
    </g>
  )
}

const PlayerLink = ({ player, role, weekMvp, useWeekMvp, ...props }) => {
  const { lang } = useAuth()
  const playerRole = useMemo(() => {
    return player?.roles?.find(row => row.roleId === role?.id)
  }, [player, role])

  return (
    <g transform={`translate(${0},${280})`} {...props}>
      <text fontSize={32} fill="black">
        <tspan x={0} dy={36} textAnchor="start">
          {lang.get('MvpMvp')}:&nbsp;
          <tspan fontWeight={700}>
            {(useWeekMvp ? weekMvp?.toFixed(1) : playerRole?.mvp?.toFixed(1)) ?? '???'}
          </tspan>
        </tspan>
        <tspan x={300} dy={0} textAnchor="end">
          ทีม:&nbsp;
          <tspan fontWeight={700}>
            <tspan fontWeight={700}>{player?.team ?? '???'}</tspan>
          </tspan>
        </tspan>
      </text>
    </g>
  )
}

PlayerLink.defaultProps = {
  useWeekMvp: false,
}

const PlayerInfo = ({ player, role, ...props }) => {
  const { lang } = useAuth()
  const playerRole = useMemo(() => {
    return player?.roles?.find(row => row.roleId === role?.id)
  }, [player, role])

  return (
    <g transform={`translate(${0},${250})`} {...props}>
      <rect x={0} y={0} width={300} height={76} fill="white" />
      <text fontSize={28} fill="black">
        <tspan x={8} dy={32} textAnchor="start">
          {lang.get('MvpName')}:&nbsp;
          <tspan fontWeight={600}>
            {player?.name ?? '???'}
          </tspan>
        </tspan>
        <tspan x={292} dy={0} textAnchor="end">
          {lang.get('MvpMvp')}:&nbsp;
          <tspan fontWeight={600}>
            {playerRole?.mvp?.toFixed(1) ?? '???'}
          </tspan>
        </tspan>
        <tspan x={8} dy={30} textAnchor="start">
          {lang.get('MvpTeam')}:&nbsp;
          <tspan fontWeight={600}>
            {player?.team ?? '???'}
          </tspan>
        </tspan>
      </text>
    </g>
  )
}

const Player = ({ player, role, weekMvp, useWeekMvp, ...props }) => {
  return (
    <svg viewBox="0 -15 300 350" {...props}>
      <PlayerAvatar player={player} role={role} />
      <PlayerLink player={player} role={role} weekMvp={weekMvp} useWeekMvp={useWeekMvp} />
    </svg>
  )
}

export const PlayerShop = ({ player, role, ...props }) => {
  return (
    <svg viewBox="0 -15 300 350" {...props}>
      {/* <PlayerInfo player={player} role={role} /> */}
      <PlayerAvatar player={player} role={role} filter="url(#shadow)" />
    </svg>
  )
}

export const ShareLayout = ({ roles, players, width, height }) => {
  const { lang } = useAuth()
  return (
    <svg viewBox="0 0 1200 630" width={width} height={height}>
      <image x={0} y={0} width={width} height={height} xlinkHref="/images/bg.jpg" />
      <g
        fontFamily={`"ALP GT America Compressed", "PSL Empire Pro", "PingFang SC", sans-serif`}
        fill="#cd2424" fontSize={65} fontWeight={650} textAnchor="middle"
        dangerouslySetInnerHTML={{
          __html: mlines('<BR>เลือก DREAM TEAM ของคุณ', 105, 630, 155)
        }}
      />
      <g fontFamily={`"ALP Bebas Neue Pro", "PSL Empire Pro", "PingFang SC", sans-serif`}>
        {roles?.map((role, idx) => (
          <g key={idx} transform={`translate(${60 + idx * 220},${115})`}>
            <Player key={idx} role={role} player={players[idx]} width={200} />
          </g>
        ))}
      </g>
    </svg>
  )
}

ShareLayout.defaultProps = {
  width: 1200,
  height: 630,
}

export default Player
