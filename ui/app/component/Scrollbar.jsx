import 'react-perfect-scrollbar/dist/css/styles.css'

import React from 'react'

import cl from 'clsx'
import PerfectScrollbar from 'react-perfect-scrollbar'

const Scrollbar = ({ className, ...props }) => (
  <div className={cl('scrollbar', className)} >
    <PerfectScrollbar {...props} />
  </div>
)

Scrollbar.defaultProps = {
  children: '',
  options: {
    suppressScrollX: true,
    handlers: ['touch', 'wheel'],
  },
}

export default Scrollbar
