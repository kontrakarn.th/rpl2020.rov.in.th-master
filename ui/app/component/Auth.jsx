import React, { useState, useContext, useCallback, useEffect, useMemo } from 'react'

import qs from 'querystring'
import { useQuery, useLazyQuery } from '@apollo/client'

import showLivestream from '../modal/Livestream'

import USER from '../../gql/queries/user.gql'
import STATE from '../../gql/queries/state.gql'
import REFER from '../../gql/mutations/refer.gql'
import LANG_QUERY from '../../gql/queries/lang.gql'
import META_QUERY from '../../gql/queries/meta.gql'
import LIVESTREAM from '../../gql/queries/livestream.gql'

const ctx = React.createContext()
const AuthProvider = ctx.Provider
const params = qs.parse(location.search?.slice(1))

if (params.access_token) {
  window.location = '/connect/garena/callback' + location.search
}

export const LANG = {
  get: () => '',
}

export const META = {}

export const useAuth = () => {
  return useContext(ctx)
}

export const useCheckAuth = () => {
  const { loading, user } = useAuth()

  return useCallback(evt => {
    if (!user?.id) {
      require('./Common').errorHandler(Error('Unauthorized'))
      return false
    }
    return true
  }, [loading, user])
}

export default ({ children }) => {
  const { data: userData, loading, error, client } = useQuery(USER)
  const [loadState, { called, data: stateData }] = useLazyQuery(STATE)
  const { data: { entities } = {}, loading: langLoading } = useQuery(LANG_QUERY)

  useEffect(() => {
    client.query({ query: META_QUERY })
      .then(({ data: { meta } }) => {
        Object.assign(META, meta)

        window.fbAsyncInit = function () {
          FB.init({
            version: 'v7.0',
            appId: meta.fbAppId,
            xfbml: false,
            status: false,
          })
        }

        const fbScript = document.createElement('script')
        fbScript.async = true
        fbScript.defer = true
        fbScript.src = meta.fbSdk

        window.dataLayer = window.dataLayer || []
        window.dataLayer.push({
          event: 'gtm.js',
          'gtm.start': new Date().getTime(),
        })

        const gtScript = document.createElement('script')
        gtScript.async = true
        gtScript.src = `https://www.googletagmanager.com/gtm.js?id=${meta.gtagId}`;

        const title = document.createElement('title')
        title.innerText = meta.title

        const desc = document.createElement('meta')
        desc.name = 'description'
        desc.content = meta.description

        const keyw = document.createElement('meta')
        keyw.name = 'keywords'
        keyw.content = meta.keywords

        document.head.appendChild(title)
        document.head.appendChild(desc)
        document.head.appendChild(keyw)
        document.head.appendChild(fbScript)
        document.head.appendChild(gtScript)
      })
      .catch(console.error)

    client.query({ query: LIVESTREAM })
      .then(({ data: { livestream } }) => {
        // console.log(livestream)
        if (livestream && livestream.youtubeId) {
          showLivestream(livestream)
        }
      })
      .catch(console.error)
  }, [])

  const lang = useMemo(() => {
    const map = entities ? new Map(Object.entries(entities)) : new Map()

    LANG.get = map.get.bind(map)
    return map
  }, [entities])

  useEffect(() => {
    if (!loading && !langLoading) {
      const { refcode } = params

      if (/[0-9a-f]{16}/.test(refcode)) {
        if (userData?.user?.id) {
          const run = async () => {
            try {
              const { data: { refer: { point, charName } } } = await client.mutate({
                mutation: REFER,
                variables: { referCode: refcode },
              })

              require('./Common').congrat(<div dangerouslySetInnerHTML={{
                __html: lang.get('CongratRefer')?.replace('{name}', charName || lang.get('Noname'))?.replace('{point}', point),
              }} />)

            } catch (err) {
              require('./Common').errorHandler(err)
            } finally {
              history.replaceState({}, '', location.pathname);
            }
          }

          run()
        } else {
          require('./Common').errorHandler(Error('Unauthorized'))
        }
      }
    }
  }, [loading, langLoading])

  useEffect(() => {
    if (!loading && !error && userData?.user && !called) {
      loadState()
    }
  }, [loading, userData, called])

  const value = useMemo(() => ({
    loading,
    lang,
    ...userData,
    ...stateData,
  }), [
    lang,
    loading,
    userData,
    stateData,
  ])

  return (
    <AuthProvider value={value}>
      {children}
    </AuthProvider>
  )
}
