import React, { useCallback, useMemo } from 'react'

import { useLocation } from 'wouter'
import { gql, useApolloClient, useMutation, useQuery } from '@apollo/client'

import showVod from '../modal/Vod'
import showReference from '../modal/Reference'
import showVdo from '../modal/Vdo'

import { useAuth } from '../component/Auth'
import { svg2img } from '../component/SvgUtils'
import { ShareLayout } from '../component/Player'
import { sleep, errorHandler, congratReward, shareFB, congrat, randName, upload } from '../component/Common'

import ROLES from '../../gql/queries/roles.gql'
import PREDICT from '../../gql/queries/predict.gql'
import PLAYERS from '../../gql/queries/players.gql'
import MISSIONS from '../../gql/queries/missions.gql'
import BEGIN_TIME from '../../gql/queries/beginTime.gql'
import CLAIM_MISSION from '../../gql/mutations/claimMission.gql'

export const useMission = (mission = {}) => {
  const { id, action, meta } = mission
  const [, setLocation] = useLocation()
  const { data: { beginTime } = {} } = useQuery(BEGIN_TIME, { skip: (id >> 4) !== 1 })

  const client = useApolloClient()
  const { state, lang } = useAuth()
  const [claimMission] = useMutation(CLAIM_MISSION)
  const isDone = useMemo(() => state?.[id >> 4 ? 'daily' : 'weekly'] >> (id & 0x0f) & 1, [state, id])

  const onClick = useCallback(async evt => {
    try {
      if (!state?.id) throw Error('Unauthorized')

      if ((id >> 4) === 1) {
        if (beginTime && Date.now() < Date.parse(beginTime)) {
          throw Error('DailyMissionNotStart')
        }
      }

      const actionFn = async () => {
        switch (action) {
          case 0:
            return shareFB(meta?.quote, meta?.url, meta?.hashtag)

          case 1:
            const { data: { predict: { playerIds } = {} } = {} } = await client.query({
              query: PREDICT,
              fetchPolicy: 'cache-only',
              variables: { weekId: state.checkedWeek },
            })

            if (playerIds?.length !== 5) throw Error('NotConfirmMvpYet')

            const filename = randName()

            const uploadFile = async () => {
              try {
                const { data: { roles } } = await client.query({ query: ROLES })
                const { data: { players } } = await client.query({ query: PLAYERS })

                const playerMap = new Map(players.map(row => [row.id, row]))
                const playerArr = playerIds.map(id => playerMap.get(id))

                const thumb = await svg2img(<ShareLayout roles={roles} players={playerArr} />)

                return upload(filename, thumb)

              } catch (err) {
                console.error(err)
              }
            }

            const shareImage = async () => {
              await sleep(960)
              return shareFB(meta?.quote, `${location.protocol}//${location.hostname}/fb/${filename}`, meta?.hashtag)
            }

            return Promise.all([uploadFile(), shareImage()])

 
          case 129:
          case 127:
            if (!isDone) {
              return showVdo()
            } else {
              throw Error('MissionClaimed')
            }

          case 132:
            return showVod()

          case 133:
            setLocation('/mvp')
            return false

          default:
            if (meta?.url) {
              window.open(meta.url, '_blank')
            }
        }
      }

      await actionFn()

      if (action < 128) {
        if (isDone) throw Error('MissionClaimed')

        const { data: { claimMission: { point, item } } } = await claimMission({ variables: { missionId: id } })

        await congrat(<div dangerouslySetInnerHTML={{ __html: lang.get('CongratReceivePoints')?.replace('{}', '&nbsp;'+point) }} />)

        if (item) {
          await congratReward(item)
        }
      }

    } catch (err) {
      errorHandler(err)
    }

  }, [state?.id, state, lang, mission, isDone, beginTime])

  return [onClick, isDone]
}

export const findMission = (action) => {
  const { data } = useQuery(MISSIONS)
  return useMemo(() => data?.missions?.find(row => row.action === action), [action, data])
} 
