import React from 'react'
import Slider from 'react-slick'

import 'slick-carousel/slick/slick.scss';
import 'slick-carousel/slick/slick-theme.scss';

const MySlider = ({ children, ...props }) => {
  return (
    <Slider {...props}>
      {children}
    </Slider>
  )
}

MySlider.defaultProps = {
  dots: true,
  speed: 500,
  arrows: false,
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
}

export default MySlider
