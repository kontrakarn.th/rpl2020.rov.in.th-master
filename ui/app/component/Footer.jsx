import React, { useState, useCallback } from 'react'

import cl from 'clsx'

import { useAuth } from '../component/Auth'
import NavLink from '../component/NavLink'

import showRule from '../modal/Rule'
import showHistory from '../modal/History'
import showDashboard from '../modal/Dashboard'

const Footer = () => {
  const { lang } = useAuth()
  const [active, setActive] = useState(false)

  const deactive = useCallback(evt => setActive(false), [])
  const toggle = useCallback(evt => setActive(v => !v), [])

  const ruleClick = useCallback(evt => {
    showRule()
    deactive()
  }, [])

  const historyClick = useCallback(evt => {
    showHistory()
    deactive()
  }, [])

  const dashbardClick = useCallback(evt => {
    showDashboard()
    deactive()
  }, [])

  return (
    <footer>
      <div className="nav">
        <NavLink className="item m-home" to="/" exact>
          <img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_home.png" alt="ic_home" />
        </NavLink>
        <NavLink className="item m-quest" to="/mission">
          <img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_mission.png" alt="ic_mission" />
        </NavLink>
        <NavLink className="item m-mvp" to="/dreamteam">
          <img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_dreamteam.png" alt="ic_dreamteam" />
        </NavLink>
        <a className={cl('item', { active })} onClick={toggle}>
          <img src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ic_more.png" alt="ic_more" />
        </a>
      </div>
      <div className={cl('overlay', { active })} onClick={deactive} />
      <div className={cl('menu', { active })}>
        {/* <a href={lang.get('MenuMainPageUrl')} className="item" onClick={deactive} target={'_blank'}>{lang.get('MenuMainPage')}</a> */}
        {/* <div className="item m-top" onClick={dashbardClick}>{lang.get('MenuDashboard')}</div> */}
        <div className="item m-rule" onClick={ruleClick}>{lang.get('MenuRule')}</div>
        <hr />
        <div className="item m-history" onClick={historyClick}>{lang.get('MenuHistory')}</div>
      </div>
    </footer>
  )
}

export default Footer
