import React, { useCallback } from 'react'

import { useLocation, useRouter } from 'wouter'

const Link = ({ component, href, to, onClick, ...props }) => {
  const { base } = useRouter()
  const [, navigate] = useLocation()

  href = href || to

  const handleClick = useCallback(
    event => {
      // ignores the navigation when clicked using right mouse button or
      // by holding a special modifier key: ctrl, command, win, alt, shift
      if (
        event.ctrlKey ||
        event.metaKey ||
        event.altKey ||
        event.shiftKey ||
        event.button !== 0
      )
        return

      event.preventDefault()
      navigate(href)
      onClick && onClick(event)
    },
    [href, onClick, navigate]
  )

  return React.createElement(component, {
    href: base + href,
    onClick: handleClick,
    ...props,
  })
}

Link.defaultProps = {
  component: 'a',
}

export default Link
