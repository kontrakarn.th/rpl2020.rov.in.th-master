import 'bootstrap/dist/css/bootstrap.min.css';
import './styles/main.scss'

import React, { Suspense } from 'react'
import { Switch, Route, Redirect } from 'wouter'

import Modal from './modal'
import Auth from './component/Auth'
import Header from './component/Header'
import Footer from './component/Footer'
import Loading from './component/Loading'
import Div100vh from './component/Div100vh'

import { SvgDefs } from './component/Svg'
import SvgUtils from './component/SvgUtils'

const Mvp = React.lazy(() => import('./page/Mvp'))
const Home = React.lazy(() => import('./page/Home'))
const Quest = React.lazy(() => import('./page/Quest'))

const App = () => (
  <Div100vh className="div100vh">
    <div style={{ height: 0 }}><svg height={0}><SvgDefs /></svg></div>
    <Auth>
      <SvgUtils />
      <Header />
      <Suspense fallback={<Loading />}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/mission" component={Quest} />
          <Route path="/dreamteam" component={Mvp} />
          <Route path="/fb/:name?" children={() => <Redirect to="/" />} />
        </Switch>
      </Suspense>
      <Footer />
      <Modal />
    </Auth>
  </Div100vh>
)

export default App
