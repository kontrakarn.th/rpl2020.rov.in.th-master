import React, { useState, useMemo, useCallback, useEffect } from 'react'

import cl from 'clsx'
import format from 'date-fns/format'
import { useQuery, useMutation } from '@apollo/client'

import showChoose from '../modal/ServerChoice'

import { useAuth } from '../component/Auth'
import { Image, Apl } from '../component/Svg'
// import Scrollbar from '../component/Scrollbar'
import { useMission } from '../component/Mission'
import { errorHandler, congratReward } from '../component/Common'
import NavLink from '../component/NavLink'

import LUCKY from '../../gql/queries/lucky.gql'
import PREDICT from '../../gql/queries/predict.gql'
import MISSIONS from '../../gql/queries/missions.gql'
import BEGIN_TIME from '../../gql/queries/beginTime.gql'
import CLAIM_MILESTONE from '../../gql/mutations/claimMilestone.gql'
import PERSON_MILESTONES from '../../gql/queries/personMilestones.gql'

const MissionButton = ({ isDone, onClick, children, ...props }) => {
  return (
    <svg viewBox={`${-100} ${-30} ${200} ${60}`} width={'4em'} {...props}>
      <g className={cl('btn-claim', { active: !isDone })} onClick={onClick}>
        {/* <path transform={`scale(1.25,1)`} d="M80,0 L60,24 L-60,24 L-80,0 L-60,-24 L60,-24 Z" /> */}
        <Image x={0} y={63} size={140} bottom={1} xlinkHref="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/btn_blue.png" />
        <text x={-25} y={4} style={{ fontSize: '32px' }}>{children}</text>
      </g>
    </svg>
  )
}

const Mission = ({ idx, mission }) => {
  const { state, lang } = useAuth()
  const { id, name, action, meta } = mission
  const [onClick, isDone] = useMission(mission)
  return (
    <div key={idx} className={cl('mission')}>
      <div className={cl('mission-name', { 'done': isDone })}>
        {idx + 1}. {name}
      </div>
      <div className="mission-score">
        {action === 128 && `[5 คะแนน]`}
        {action === 127 && `[5 คะแนน]`}
        {action === 1 && `[15 คะแนน]`}
      </div>
      <div className="mission-btn">
        {action === 128 && <MissionButton isDone={isDone} onClick={onClick}>{isDone ? 'สำเร็จ' : 'ทำเลย'}</MissionButton>}
        {action === 127 && <MissionButton isDone={isDone} onClick={onClick}>{isDone ? 'สำเร็จ' : 'ทำเลย'}</MissionButton>}
        {action === 1 && <NavLink to="/dreamteam"><MissionButton isDone={isDone}>{isDone ? 'สำเร็จ' : 'ทำเลย'}</MissionButton></NavLink>}
      </div>
    </div>
  )
}

const Missions = () => {
  const { state, lang } = useAuth()
  const [tab, setTab] = useState(0)
  const { data: { missions } = {} } = useQuery(MISSIONS)
  const { data: { predict } = {} } = useQuery(PREDICT, { variables: { weekId: state?.checkedWeek }, skip: !state?.checkedWeek })

  const { data: { beginTime } = {}, loading } = useQuery(BEGIN_TIME)

  useEffect(() => {
    if (!loading && beginTime) {
      if (Date.now() > Date.parse(beginTime)) setTab(1)
    }
  }, [loading])

  const array = useMemo(() => {
    return missions
  }, [missions, tab])

  return (
    <div className="missionwrapper">
      {/* <div className="tabs">
        <div className={cl('tab', { active: tab === 1 })} onClick={evt => setTab(1)}>{lang.get('QuestDailyMission')}</div>
        <div className={cl('tab', { active: tab === 0 })} onClick={evt => setTab(0)}>{lang.get('QuestWeeklyMission')}</div>
      </div> */}
      {/* <Scrollbar> */}
        {array?.map((mission, idx) => <Mission key={idx} idx={idx} mission={mission} />)}
      {/* </Scrollbar> */}
    </div>
  )
}

const Milestone = ({ milestone: { id, requiredPoints, item, startAt } }) => {
  const idx = id - 1
  const { state, lang } = useAuth()
  const [claimMilestone] = useMutation(CLAIM_MILESTONE, {
    variables: {
      milestoneId: id,
      type: 'PERSON',
    }
  })

  const done = state?.person >> id & 1
  const reach = state?.point >= requiredPoints
  const intime = useMemo(() => Date.now() >= Date.parse(startAt), [startAt])

  const onClick = useCallback(async evt => {
    try {
      if (!state) throw Error('Unauthorized')
      if (!intime) throw Error('PersonMilestoneNotStart')
      if (!reach) throw Error('PersonMilestoneDontEnoughPoint')
      if (done) throw Error('PersonMilestoneClaimed')

      const partitionId = await showChoose(state)

      if (!partitionId) return

      const { data: { claimMilestone: { item } } } = await claimMilestone({
        variables: {
          partitionId,
        },
      })

      return congratReward(item)
    } catch (err) {
      errorHandler(err)
    }
  }, [state, done, reach, intime])

  return (
    <g className="milestone" key={idx} transform={`translate(${200 * idx},0)`}>
      <Apl x={0} y={0} size={80} className={cl({ active: reach })} />
      <g transform={`translate(0,${-10})`}>
        <Image x={0} y={0} size={120} bottom={1} xlinkHref={item?.icon} />
        {/* <text y={0}>
          <tspan x={0} dy={24} className="chest-name">{item?.name}</tspan>
        </text> */}
      </g>
      {/* <text y={75} className={cl({ active: intime })}>
        {lang.get('QuestClaimTime')?.replace('{}', format(startAt, lang.get('DateFormat') || 'DD.MM'))}
      </text> */}
      <g className={cl('btn-claim', { active: intime && reach, done })} transform={`translate(0,${55})`} onClick={onClick}>
        {/* <path d="M80,0 L60,24 L-60,24 L-80,0 L-60,-24 L60,-24 Z" /> */}
        <Image x={0} y={70} size={140} bottom={1} xlinkHref="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/btn_red.png" />

        <text x={0} y={5}>
          {
            reach && intime
              ? done ? lang.get('QuestClaimed') : lang.get('QuestClaim')
              : `${requiredPoints} ${lang.get('QuestPoints')}`
          }

        </text>
        {/* {idx !== 0 && <line x1={-80} y1={0} x2={-120} y2={0} className="sec-line" />} */}
      </g>
    </g>
  )
}

const Milestones = ({ props }) => {
  const { state, lang } = useAuth()
  const { data: { milestones } = {} } = useQuery(PERSON_MILESTONES)

  const rWidth = useMemo(() => {
    if (milestones?.length && state?.point) {
      let idx = -1
      for (let i = milestones.length - 1; i >= 0; i--) {
        if (state.point >= milestones[i].requiredPoints) {
          idx = i
          break
        }
      }

      if (idx >= 0) {
        return 200 * (idx + (idx >= milestones.length - 1 ? 0
          : (state.point - milestones[idx].requiredPoints) / (milestones[idx + 1].requiredPoints - milestones[idx].requiredPoints)
        ))
      }
    }

    return 0
  }, [milestones, state?.point])

  return (
    <svg viewBox={`-100 -150 ${200 * (milestones?.length || 2)} 230`}>
      <defs>
        <mask id="line" x={0} y={-5} width={'100%'} height={10}>
          <rect x={0} y={-5} width={'100%'} height={10} fill="white" />
          {milestones?.map((_, idx) => <Apl key={idx} x={200 * idx} y={0} size={80} fill="black" mask={'none'} />)}
        </mask>
      </defs>
      <rect x={0} y={-1} width={200 * ((milestones?.length || 1) - 1)} height={1} className="prim-line" mask="url(#line)" />
      <rect x={0} y={-1} width={rWidth} height={1} className="thir-line" mask="url(#line)" />
      {milestones?.map((value, idx) => <Milestone key={idx} milestone={value} />)}
    </svg>
  )
}

const Quest = () => {
  const { state, lang } = useAuth()
  const { data: { lucky } = {} } = useQuery(LUCKY)

  const topItem = lang.get('QuestTopReward')
  const showTop = topItem && topItem != 'none'

  return (
    <div className="app-body quest">
      {/* <div className="title">{lang.get('QuestTitle')}</div> */}
      <div className="text-center">
        <div className="my-point">
          {lang.get('QuestCurrentPoint')} :<span className="txt-third"> {state?.point || 0} </span> {lang.get('QuestPoints')}
        </div>
      </div>
      <Missions />
      <div className="lucky-reward">
        {/* <img src={lucky.icon} /> */}
        <div className="lucky-name"></div>
      </div>
      <div className="milestoneWrapper">
        <Milestones />
      </div>
    </div>
  )
}

export default Quest
