import React, { useRef, useState, useEffect, useCallback, useMemo } from 'react'

import cl from 'clsx'
import format from 'date-fns/format'
import { useQuery, useMutation } from '@apollo/client'

import showChoose from '../modal/ServerChoice'

import { useAuth } from '../component/Auth'
import { Image, Apl } from '../component/Svg'
import { findMission, useMission } from '../component/Mission'
import { errorHandler, congratReward } from '../component/Common'

import REGIONS from '../../gql/queries/regions.gql'
import MYREGIONPOINT from '../../gql/queries/myRegionPoint.gql'
import CLAIM_MILESTONE from '../../gql/mutations/claimMilestone.gql'
import REGION_MILESTONES from '../../gql/queries/regionMilestones.gql'

const Milestone = ({ milestone: { id, name, item, startAt, minPoints }, myRegionPoint, transform }) => {
  const idx = id - 1
  const { state, lang } = useAuth()
  const [claimMilestone] = useMutation(CLAIM_MILESTONE, {
    variables: {
      milestoneId: id,
      type: 'REGION',
    }
  })

  const intime = useMemo(() => Date.now() >= Date.parse(startAt), [startAt])
  const reach = useMemo(() => state?.point >= minPoints && (myRegionPoint / 100 | 0) >= id, [state?.point, id, myRegionPoint])
  const done = state?.region >> id & 1

  const onClick = useCallback(async evt => {
    try {
      if (!state) throw Error('Unauthorized')
      if (!intime) throw Error('RegionMilestoneNotStart')
      if (!reach) throw Error('RegionMilestoneDontEnoughPoint')
      if (done) throw Error('RegionMilestoneClaimed')

      const partitionId = await showChoose(state)

      if (!partitionId) return

      const { data: { claimMilestone: { item } } } = await claimMilestone({
        variables: {
          partitionId,
        },
      })

      return congratReward(item)

    } catch (err) {
      errorHandler(err)
    }
  }, [state, done, reach, intime])

  return (
    <g className="milestone" key={idx} transform={cl(`translate(${250 * id},0)`, transform)}>
      <Apl x={0} y={0} size={50 * (1.25 ** idx)} className={cl('apl', { active: reach })} />
      <Image x={0} y={-105} size={68 * (1.2 ** idx)} bottom={1} xlinkHref={item?.icon} />

      <text y={-105}>
        <tspan x={0} dy={22} className="chest-name">{item?.name}</tspan>
      </text>
      {
        intime && reach
          ? (
            <g className={cl('btn-claim', { active: true, done })} transform={`translate(0,${-50})`} onClick={onClick}>
              <path transform={`scale(${0.75})`} d="M80,0 L60,24 L-60,24 L-80,0 L-60,-24 L60,-24 Z" />
              <text x={0} y={7} style={{ fontSize: '20px' }}>
                {done ? lang.get('HomeClaimed') : lang.get('HomeClaim')}
              </text>
            </g>
          ) : (
            <text y={-80}>
              <tspan x={0} dy={22} className={cl({ active: reach })}>{name}</tspan>
              <tspan x={0} dy={22} className={cl({ active: intime })}>
                {lang.get('HomeClaimTime')?.replace('{}', format(startAt, lang.get('DateFormat') || 'DD.MM'))}
              </tspan>
            </text>
          )}
    </g>
  )
}

const Region = ({ region: { id, icon, isMyRegion, regionPoint }, transform, ...props }) => {
  const x = useMemo(() => {
    const a = regionPoint / 100 | 0
    switch (a) {
      case 0:
        return 50 + regionPoint * 2
      default:
        return Math.min(900, regionPoint * 2.5)
    }
  }, [regionPoint])

  return (
    <g transform={cl(`translate(${x},0)`, transform)} className={cl('anchor', { active: isMyRegion })}>
      <line x1={0} y1={0} x2={0} y2={68} strokeWidth={1} />
      <rect x={-5} y={-5} width={10} height={10} transform="rotate(45)" />
      <Image x={0} y={68} size={60} xlinkHref={icon} />
    </g>
  )
}

const Milestones = () => {
  const elm = useRef()
  const [config, setConfig] = useState({ zoom: 1, width: 100, height: 100 })

  const { lang } = useAuth()
  const { data: { regions } = {} } = useQuery(REGIONS)
  const { data: { myRegionPoint } = {} } = useQuery(MYREGIONPOINT)
  const { data: { milestones } = {} } = useQuery(REGION_MILESTONES)

  const regionOrdered = useMemo(() => {
    if (regions?.length) {
      return [...regions].sort((a, b) => a.regionPoint - b.regionPoint)
    }
  }, [regions])

  useEffect(() => {
    const updateConfigs = () => {
      setTimeout(() => {
        setConfig(configs => ({
          ...configs,
          zoom: window.innerHeight >= window.innerWidth ? 1.75 : 1,
          width: elm.current.clientWidth,
          height: Math.min(elm.current.clientHeight, 440),
        }))
      }, 200)
    }

    updateConfigs()
    window.addEventListener('resize', updateConfigs)

    return () => window.removeEventListener('resize', updateConfigs)
  }, [])

  const { zoom, width, height } = config

  return (
    <div className="section-milestone" ref={elm}>
      <svg viewBox={`0 -${200 * zoom} 1000 ${335 * zoom}`} width={width} height={height}>
        <defs>
          <mask id="line"
            x={0} y={-5} width={'100%'} height={10}
          >
            <rect x={0} y={-5} width={'100%'} height={10} fill="white" />
            {milestones?.map((_, idx) => (
              <g key={idx} transform={`translate(${250 * (idx + 1)},0) scale(${zoom})`}>
                <Apl x={0} y={0} size={62 * (1.2 ** idx)} fill="black" mask={'none'} />
              </g>
            ))}
          </mask>
        </defs>

        <rect x={50} y={-1} width={900} height={2} className="prim-line" mask="url(#line)" />

        {milestones?.map((value, idx) => <Milestone key={idx} milestone={value} myRegionPoint={myRegionPoint} transform={`scale(${zoom})`} />)}

        {regionOrdered?.map((value, idx) => <Region key={idx} region={value} transform={`scale(${zoom})`} />)}

        <g transform={`translate(500,0) scale(${zoom})`}>
          <text x={0} y={125} textAnchor="middle" fontSize={20}>{lang.get('HomeNoted')}</text>
        </g>
      </svg>
    </div>
  )
}

const Home = () => {
  const { lang } = useAuth()
  const shareMission = findMission(0)
  const [shareClick] = useMission(shareMission)

  return (
    <div className="app-body home">
      <img className="banner" src="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/banner.png" alt="banner" />
      {/* <div className="text-center mt-2">
        <div className="btn btn-seco home-share" onClick={shareClick}>{lang.get('HomeFacebookShare')}</div>
      </div> */}
    </div>
  )
}

export default Home
