import React, { useState, useEffect, useCallback, useMemo, useRef } from 'react'

import cl from 'clsx'
import { useQuery, useMutation } from '@apollo/client'

import showShop from '../modal/Shop'

import { Image } from '../component/Svg'
import Slider from '../component/Slider'
import Player from '../component/Player'
import { useAuth } from '../component/Auth'
import Scrollbar from '../component/Scrollbar'
import { errorHandler, congrat } from '../component/Common'
import { findMission, useMission } from '../component/Mission'

import WEEK from '../../gql/queries/week.gql'
import ROLES from '../../gql/queries/roles.gql'
import PREDICT from '../../gql/queries/predict.gql'
import PLAYERS from '../../gql/queries/players.gql'
// import LIVETIMEVOTE from '../../gql/queries/livetimeVote.gql'
import LASTWEEK_VOTE from '../../gql/queries/lastweekVote.gql'
import PREDICT_MUTATION from '../../gql/mutations/predict.gql'

const PlayerButton = ({ show, role, choices, onClick, ...props }) => {
  const { lang } = useAuth()
  const isChoosed = !!choices[role.id]

  if (!show) return null

  return (
    <svg viewBox={`${-150} ${-25} ${300} ${100}`} {...props}>
      <g className={cl('btn-claim', 'active')} onClick={onClick}>
        {/* <path transform={`scale(1.1,1)`} d="M80,0 L60,24 L-60,24 L-80,0 L-60,-24 L60,-24 Z" /> */}
        <Image x={0} y={90} size={170} bottom={1} xlinkHref="//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/btn_red.png" />
        <text x={0} y={13} fill="white" style={{ fontSize: '28px' }} textAnchor="middle">{lang.get(isChoosed ? 'MvpChooseAgain' : 'MvpChoose')}</text>
      </g>
    </svg>
  )
}

const ShareButton = ({ className, ...props }) => {
  const { lang } = useAuth()
  const shareMission = findMission(1)
  const [shareClick] = useMission(shareMission)

  return (
    <div className={cl('btn', 'btn-seco', className)} onClick={shareClick} {...props}>
      {lang.get('MvpShareYourTeam')}
    </div>
  )
}

const PlayerSection = ({ weekMvp, useWeekMvp, role, player, choices, setChoices, week, showShopBtn }) => {
  const { state } = useAuth()

  const onClick = useCallback(evt => {
    if (!showShopBtn) return false
    if (!state?.id) return errorHandler(Error('Unauthorized'))
    if (Date.now() >= Date.parse(week?.endTime)) return errorHandler(Error('WeekEnded'))

    return showShop({
      choices,
      setChoices,
      roleId: role.id,
    })
  }, [showShopBtn, choices, setChoices, state?.id, week])

  return (
    <div className="role">
      <Player role={role} player={player} onClick={onClick} weekMvp={weekMvp} useWeekMvp={useWeekMvp} />
      <PlayerButton role={role} show={showShopBtn} choices={choices} onClick={onClick} />
    </div>
  )
}

const YourVote = (props) => {
  const [predictMutation] = useMutation(PREDICT_MUTATION)
  const { state, lang, week, roles, playerMap, choices, setChoices, isPredicted } = props

  const playerArray = useMemo(() => {
    return choices?.map(row => playerMap.get(row))
  }, [playerMap, choices])

  const confirmClick = useCallback(async evt => {
    try {
      if (!state?.id) throw Error('Unauthorized')
      if (isPredicted) throw Error('MvpVoted')
      if (Date.now() >= Date.parse(week?.endTime)) throw Error('WeekEnded')

      if (choices.length !== 5) throw Error('InvalidPlayerIds')
      for (const value of choices) {
        if (!value) throw Error('InvalidPlayerIds')
      }

      await predictMutation({
        variables: {
          weekId: week?.id,
          playerIds: choices,
        },
      })

      return congrat(lang.get('CongratVoteSuccess'))
    } catch (err) {
      errorHandler(err)
    }
  }, [state, week, choices, isPredicted, lang])

  return (
    <>
      <div className="section-note">
        <span style={{opacity: 0}}>{lang.get('MvpYourVoteNote')}</span>
      </div>
      <Scrollbar className="section-player">
        <div className="players">
          {roles?.map((role, idx) => (
            <PlayerSection
              key={idx}
              role={role} player={playerArray?.[idx]}
              choices={choices}
              setChoices={setChoices}
              week={week} showShopBtn={!isPredicted}
            />
          ))}
        </div>
      </Scrollbar>
      <div className="section-btn2">
        <div className={cl('btn', 'btn-seco', { grayscale: isPredicted })} onClick={confirmClick}>
          {lang.get('MvpConfirm')}
        </div>
        <ShareButton className={cl('mvp-share', { grayscale: !isPredicted })} />
      </div>
    </>
  )
}

// const LivetimeVote = (props) => {
//   const { data: { livetimeVote } = {} } = useQuery(LIVETIMEVOTE)
//   const { state, lang, week, roles, playerMap, choices, setChoices, isPredicted, setTab } = props

//   const playerArray = useMemo(() => {
//     return livetimeVote?.map(row => playerMap.get(row))
//   }, [playerMap, livetimeVote])

//   const pickClick = useCallback(async evt => {
//     try {
//       if (!state?.id) throw Error('Unauthorized')
//       if (isPredicted) throw Error('MvpVoted')

//       if (playerArray?.length === 5) {
//         setChoices(playerArray.map(row => row.id))
//         setTab(0)
//       }
//     } catch (err) {
//       errorHandler(err)
//     }
//   }, [state, isPredicted, playerArray])

//   return (
//     <>
//       <div className="section-note">
//         {/* <span>{lang.get('MvpLivetimeNote')}</span> */}
//       </div>
//       <div className="section-player">
//         <div className="players">
//           {roles?.map((role, idx) => (
//             <PlayerSection
//               key={idx}
//               role={role} player={playerArray?.[idx]}
//               choices={choices}
//               setChoices={setChoices}
//               week={week} showShopBtn={false}
//             />
//           ))}
//         </div>
//       </div>
//       <div className="section-btn2">
//         <div className={cl('btn', 'btn-seco', { grayscale: playerArray?.length !== 5 })} onClick={pickClick}>
//           {lang.get('MvpPickThere')}
//         </div>
//       </div>
//     </>
//   )
// }
const LastweekVote = (props) => {
  const [index, setIndex] = useState(0)
  const { data: { lastweekVote } = {} } = useQuery(LASTWEEK_VOTE)
  const { state, lang, week, roles, playerMap, choices, setChoices, isPredicted, setTab } = props

  const playerArrays = useMemo(() => {
    if (lastweekVote?.length) {
      return lastweekVote.map(row => ({
        name: row.name,
        weekMvps: row.mvps,
        playerArray: row.playerIds?.map(row => playerMap.get(row)),
      }))
    }
    return [{ name: '' }]
  }, [playerMap, lastweekVote])

  const iWeek = useMemo(() => {
    return playerArrays?.[index]
  }, [playerArrays, index])

  const pickClick = useCallback(async evt => {
    try {
      if (!state?.id) throw Error('Unauthorized')
      if (isPredicted) throw Error('MvpVoted')

      const iArray = iWeek?.playerArray

      if (iArray?.length === 5) {
        setChoices(iArray.map(row => row.id))
        setTab(0)
      }
    } catch (err) {
      errorHandler(err)
    }
  }, [state, isPredicted, iWeek])

  return (
    <>
      <div className="section-note">
        <span>{lang.get('MvpLastweekNote')?.replace('{week}', iWeek.name || '')}</span>
      </div>
      <div className="section-player">
        <Slider afterChange={idx => setIndex(idx)}>
          {playerArrays.map(({ playerArray, weekMvps }, idx) => (
            <div key={idx}>
              <div className="players">
                {roles?.map((role, idx) => (
                  <PlayerSection
                    key={idx}
                    role={role} player={playerArray?.[idx]}
                    choices={choices} setChoices={setChoices}
                    week={week} showShopBtn={false}
                    weekMvp={weekMvps?.[idx]} useWeekMvp={true}
                  />
                ))}
              </div>
            </div>
          ))}
        </Slider>
      </div>
      <div className="section-btn2">
        <div key={index} className={cl('btn', 'btn-seco', { grayscale: iWeek?.playerArray?.length !== 5 })} onClick={pickClick}>
          {lang.get('MvpPickThere')}
        </div>
      </div>
    </>
  )
}

const Mvp = () => {
  const [tab, setTab] = useState(0)
  const [choices, setChoices] = useState([])

  const { state, lang } = useAuth()
  const { data: { roles } = {} } = useQuery(ROLES)
  const { data: { players } = {} } = useQuery(PLAYERS)

  const { data: { week } = {} } = useQuery(WEEK, { variables: { weekId: state?.checkedWeek }, skip: !state?.checkedWeek })
  const { data: { predict } = {} } = useQuery(PREDICT, { variables: { weekId: state?.checkedWeek }, skip: !state?.checkedWeek })

  const isPredicted = useMemo(() => predict?.playerIds?.length === 5, [predict, predict?.playerIds])

  useEffect(() => {
    if (isPredicted) {
      setChoices(predict?.playerIds)
    }
  }, [isPredicted])

  const playerMap = useMemo(() => {
    if (players?.length) {
      return new Map(players?.map(row => [row.id, row]))
    }
    return new Map()
  }, [players])

  const childProps = {
    setTab,
    isPredicted,
    lang, state,
    choices, setChoices,
    week, roles, playerMap,
  }

  return (
    <div className="app-body mvp">
      <div className="title">{lang.get('MvpTitle')}</div>
      <div className="section-btn">
        <div className={cl('btn', { active: tab === 0 })} onClick={evt => setTab(0)}>ทีมของคุณ</div>
        <div className={cl('btn', { active: tab === 1 })} onClick={evt => setTab(1)}>ทีมยอดนิยม</div>
        {/* <div className={cl('btn', { active: tab === 2 })} onClick={evt => setTab(2)}>{lang.get('MvpLivetimeVote')}</div> */}
      </div>
      {tab === 1 ? <LastweekVote {...childProps} /> : <YourVote {...childProps} />}
    </div>
  )
}

export default Mvp
