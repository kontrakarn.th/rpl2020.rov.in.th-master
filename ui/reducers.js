import { combineReducers } from 'redux'

export default combineReducers({
  noop: (state = null) => state,
})
