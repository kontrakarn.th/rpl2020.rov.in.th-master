import { createUploadLink } from 'apollo-upload-client'

import settings from '../../settings'

export default createUploadLink({
  uri: settings?.graphql?.endpoint,
  credentials: 'include',
})
