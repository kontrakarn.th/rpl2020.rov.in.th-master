import { split } from '@apollo/client'
import { getMainDefinition } from '@apollo/client/utilities'

import ws from './ws'
import http from './upload'

export default split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  ws,
  http,
)
