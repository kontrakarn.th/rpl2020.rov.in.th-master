import { WebSocketLink } from '@apollo/link-ws'

import settings from '../../settings'

export default new WebSocketLink({
  uri: `${location.protocol.replace('http', 'ws')}//${window.location.host}${settings?.graphql?.endpoint}`,
  credentials: 'include',
  options: {
    lazy: true,
    reconnect: true,
    reconnectionAttempts: 12,
  },
})
