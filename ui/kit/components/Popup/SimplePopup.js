import React from 'react'
import Popup, { showPopup, hidePopup } from './Popup'

const POPUP_ID = 'simple-popup'

const styles = {
  content: {
    width: '300px',
    height: '150px',
    border: '1px solid rgb(204, 204, 204)',
    background: 'rgb(255, 255, 255)',
    overflow: 'auto',
    borderRadius: '4px',
    outline: 'none',
    margin: '150px auto',
    textAlign: 'center',
  }
}

export const showMessage = (message) => showPopup(POPUP_ID, { message })

export const hideMesasge = () => hidePopup(POPUP_ID)

const PopupBody = ({ actions: { hidePopup }, data: { message } }) =>
  <div style={styles.content}>
    <p><b>Message</b></p>
    <p>{message}</p>
    <button onClick={hidePopup}>OK</button>
  </div>

const SimplePopup = () => <Popup popupId={POPUP_ID}><PopupBody/></Popup>

export default SimplePopup
