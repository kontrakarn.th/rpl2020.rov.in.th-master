import React from 'react'
import { connect } from 'react-redux'

import styles from './styles.scss'

const BasePopup = ({ popupId, hidePopup, current, data, children, classes = {} }) => (
  <div className={styles.overlay} style={{ display: (popupId === current) ? 'block' : 'none' }}>
    <div className={styles.curtain} onClick={hidePopup}/>
    <div className={`${styles.dialog} ${classes.modal || ''}`}>
      {React.Children.map(children, child =>
        React.cloneElement(child, { data, actions: { showPopup, hidePopup } }))}
    </div>
  </div>
)

export const hidePopup = () => ({ type: 'hidePopup' })

export const showPopup = (target, data) => {
  const payload = {
    data,
    current: target,
  }
  return { type: 'showPopup', payload }
}

export const popupReducer = (state = { current: null }, { type, payload }) => {
  switch (type) {
    case 'showPopup':
      return { ...state, ...payload }
    case 'hidePopup':
      return { ...state, current: null }
    default:
      return state
  }
}

export default connect(
  // state to props
  ({ popup: { current, data = {} } }) => ({ current, data }),
  // dispatch to props
  { showPopup, hidePopup },
)(BasePopup)
