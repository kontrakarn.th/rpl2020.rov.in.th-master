import React from 'react'

import style from './style.scss'

const Reloadable = ({ loading, children }) =>
  <div className={style['reloadable-area']}>
    { children }
    { loading &&
      <div className={style['loading-area']}>
        <div className={style.loader}/>
      </div>
    }
    <div className="clearfix"></div>
  </div>

export default Reloadable
