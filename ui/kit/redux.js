/* eslint-disable no-underscore-dangle */

/*
Custom Redux store creation.
*/

import { createStore } from 'redux'

import uistore from 'ui/reducers'

function createNewStore () {
  const store = createStore(
    uistore,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  )

  // Hot reload
  if (module.hot) {
    module.hot.accept(['ui/reducers', 'ui/app'], () => {
      store.replaceReducer(require('ui/reducers').default)
    })
  }

  return store
}

export default createNewStore()
