import settings from 'ui/settings'
const { language } = settings
const i18n = path => require(`ui/i18n/${language}/${path}`).default

export default i18n
