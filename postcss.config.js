module.exports = ({ file, options, env }) => {
  return ({
    plugins: [
      require('postcss-advanced-variables')({ disable: '@import' }),
      require('postcss-nested'),
    ],
  })
}
