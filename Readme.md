<!-- <img src="https://reactql.org/reactql/logo.svg" alt="ReactQL" width="278" height="77" /> -->

# New in 2.x: Full-stack GraphQL + React v16

Full-stack React + GraphQL, done properly.

Want to install quickly? Use the [CLI](#usage) - it does the heavy lifting for you.

## Features

### Stack

- New in 2.x: [React v16](https://facebook.github.io/react/) for UI
- New in 2.x: [Apollo Server](http://dev.apollodata.com/tools/) for enabling the built-in GraphQL server
- [Apollo Client (React)](http://dev.apollodata.com/react/) for connecting to GraphQL
- [React Router 4](https://github.com/ReactTraining/react-router/tree/v4) for declarative browser + server routes
- [Redux](http://redux.js.org/) for flux/store state management

### GraphQL

- New in 2.x: Built-in GraphQL server via - just pass in your schema, and enable `/graphql` with a single line of code
- [GraphiQL](https://github.com/graphql/graphiql) query browser enabled by default
- Write `.gql` GraphQL query files, use fragments, or type queries inline.

### Server-side

- Built-in [Koa 2](http://koajs.com/) web server, with async/await routing

### Real-time

- [Hot code reloading](http://gaearon.github.io/react-hot-loader/); zero refresh, real-time updates in development
- React + Redux state preservation on hot reloading, to avoid interrupting your dev flow
- [Development web server](#developer-support) that automatically rebuilds and restarts on code changes, for on-the-fly SSR testing with full source maps
- Hot code reload works inside Docker too! Just `docker-compose -f docker-compose.yml up`

### Code optimisation

- [Webpack v3](https://webpack.js.org/), with [tree shaking](https://webpack.js.org/guides/tree-shaking/) -- dead code paths are automatically eliminated
- Separate local + vendor bundles, for better browser caching/faster builds
- Dynamic polyfills, courtesy of [babel-preset-env](https://github.com/babel/babel-preset-env)
- Aggressive code minification with [Uglify](https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin)
- [GIF/JPEG/PNG/SVG crunching](https://github.com/tcoopman/image-webpack-loader) for images in production
- CSS code is combined, minified and optimised automatically - even if you use SASS, LESS and CSS together!

### Styles

- [PostCSS v6](http://postcss.org/) with [next-gen CSS](http://cssnext.io/) and inline [@imports](https://github.com/postcss/postcss-import)
- [SASS](http://sass-lang.com) and [LESS](http://lesscss.org/) support (also parsed through PostCSS)
- Automatic vendor prefixing - write modern CSS, and let the compiler take care of browser compatibility
- Mix and match SASS, LESS and regular CSS - without conflicts!
- CSS modules - your classes are hashed automatically, to avoid namespace conflicts
- Compatible with Foundation, Bootstrap, Material and more. Simply configure via a `.global.*` import to preserve class names

### Highly configurable

- New in 2.x: Userland configuration.  No need to edit kit code; simply use the built-in `Config` singleton
- Add a GraphQL server with one line of code
- Add GET|POST|PUT|PATCH|DELETE routes - auto-injected with Koa context and the per-request Redux store
- Add a custom 404 handler
- Enable/disable POST body parsing, along with custom options
- Enable/disable HTTP hardening

### Production-ready

- Easily extendable [webpack-config](https://fitbit.github.io/webpack-config/) files, for modular Webpack tweaks
- [Docker](https://www.docker.com/) support, with an optimised `Dockerfile` out-the-box
- [PM2](http://pm2.keymetrics.io/)-enabled Dockerfile, which auto-clusters over multi-core processes and auto-restarts on failure

### Developer support

- Run `npm start` or `npm run server-dev && npm run browser-dev`

## Usage

Run `npm start` or `yarn` in the project root, and away you go!

## Docker

An Alpine-based [Dockerfile](https://git.ved.com.vn/COD_GOP/VED.node-graphql-kit/blob/master/Dockerfile) is included, that will build, optimise and bundle a production-mode ReactQL web server, your static assets and client-side code -- making it trivial to deploy to production.

Build as normal with:

`docker build . -t <project>`

Then run with:

`docker run -p 4000:4000 <project>`

Navigating to http://<docker_host>:4000 will yield the ReactQL project code.

You can also run with Docker Compose:

`docker-compose -f docker-compose.yml up`

This will build and spawn a development environment on ports 8080 and 8081. Just like your local dev environment, both browser hot code reloading and SSR auto-restarting will work on local code changes -- even inside the Docker stack!

(You can also spawn a production-grade environment with `docker-compose up`, using the default [docker-compose.yml](https://git.ved.com.vn/COD_GOP/VED.node-graphql-kit/blob/master/docker-compose.yml))

# Follow @node-graphql-kit for updates

Get the latest updates by following us on Git: https://git.ved.com.vn/COD_GOP/VED.node-graphql-kit

