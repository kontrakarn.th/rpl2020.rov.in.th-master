/**
 * Document https://github.com/tj/commander.js/
 * @type {name: string, alias: string, description: string, builder: function, action: function}
 */

// if (require.main === module) {
//   const program = require('commander')
//   program
//     .option('--no-sauce', 'Remove sauce')
//     .option('--cheese <flavour>', 'cheese flavour', 'mozzarella')
//     .option('--no-cheese', 'plain with no cheese')
//     .parse(process.argv);

//   const sauceStr = program.sauce ? 'sauce' : 'no sauce'
//   const cheeseStr = (program.cheese === false) ? 'no cheese' : `${program.cheese} cheese`
//   console.log(`You ordered a pizza with ${sauceStr} and ${cheeseStr}`)
// }

module.exports = {
  name: 'example',
  builder: command => {
    command
      .alias('exp')
      .description('Example command')
      .option('--no-sauce', 'Remove sauce')
      .option('--cheese <flavour>', 'cheese flavour', 'mozzarella')
      .option('--no-cheese', 'plain with no cheese')
  },
  action: (command) => {
    const sauceStr = command.sauce ? 'sauce' : 'no sauce'
    const cheeseStr = (command.cheese === false) ? 'no cheese' : `${command.cheese} cheese`
    console.log(`You ordered a pizza with ${sauceStr} and ${cheeseStr}`)
  }
}