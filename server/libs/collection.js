/* Return k random items of n items with weight

  ar: array of items
  k: the number of random items, default is 1
  f: the function to get weight from item, default is return 1 for any item
*/
export const sample = (ar, k = 1, f = x => 1) => {
  const l = ar.length
  if (k >= l) { return [...ar] }

  const s = ar.reduce((sum, x) => sum + f(x), 0)
  if (s <= 0) { return [] }

  const r = Math.random() * s
  let c = 0
  let i = 0
  for (i = 0; i < l; ++i) {
    c += f(ar[i])
    if (r <= c) { break }
  }
  i = (i >= l ? l - 1 : i)
  if (k <= 1) {
    return [ar[i]]
  } else {
    return [ar[i]].concat(sample([...ar.slice(0, i), ...ar.slice(i + 1, l)], k - 1, f))
  }
}

/* Return k random items of an array of item with priority and weight
  The items with higher priority will always be selected before the lower priority items are considered

  ar: array of items
  k: number of random items, default is 1
  pf: the function to get priority of item, default returns 1
  wf: the function to get weight of item, default returns 1
*/
export const biasedSample = (ar, k = 1, pf = x => 1, wf = x => 1) => {
  const l = ar.length
  if (k > l) { return [...ar] }

  const pd = {}
  for (const x of ar) {
    const pv = pf(x)
    pd[pv] = pd[pv] || []
    pd[pv].push(x)
  }

  let res = []
  for (const pv of Object.keys(pd).sort((x, y) => (parseFloat(y) - parseFloat(x)))) {
    const xar = pd[pv]
    if (k === xar.length) { return res.concat(xar) } else if (k < xar.length) { return res.concat(sample(xar, k, wf)) } else {
      res = res.concat(xar)
      k -= xar.length
    }
  }
}
