export const factorial = (number) => {
  let value = number
  for (let i = number; i > 1; i--) { value *= i - 1 }
  return value
}

export const combination = (n, r) => {
  // TODO: do not use factorial (big number problem)

  if (n === r) {
    return 1
  }
  return factorial(n) / (factorial(r) * factorial(n - r))
}
