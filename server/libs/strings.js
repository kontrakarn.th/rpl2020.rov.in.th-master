export const titleize = (str) => {
  if (typeof str !== 'string') {
    throw new TypeError('String Required')
  }
  return str.toLowerCase().replace(/(?:^|\s|-)\S/g, function (m) {
    return m.toUpperCase()
  })
}

export const random = (length, set) => {
  set = set || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  let res = ''
  for (let i = 0; i < length; i++) { res += set.charAt(Math.floor(Math.random() * set.length)) }
  return res
}

export const fromStream = (stream) => (
  new Promise((resolve, reject) => {
    const chunks = []
    stream.on('data', chunk => chunks.push(chunk.toString()))
    stream.on('end', () => resolve(chunks.join('')))
  })
)
