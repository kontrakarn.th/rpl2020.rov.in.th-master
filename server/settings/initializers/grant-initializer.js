export default (config) => {
  const homepage = config.homepage

  config.grant = {
    defaults: {
      origin: `${homepage.protocol}//${homepage.host}`,
    },
    google: {
      key: process.env.GOOGLE_AUTH_KEY || 'google.auth.key',
      secret: process.env.GOOGLE_AUTH_SECRET || 'google.auth.secret',
      scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email'],
      nonce: true,
      callbackHook: {
        model: 'Admin',
        key: 'email',
        mode: 'update',
        infoUrl: 'https://www.googleapis.com/oauth2/v2/userinfo',
        info2Model: ({ email, name, picture }) => ({ email, name, avatar: picture }),
        redirect: '/admin',
      },
      logoutHook: (req, res) => {
        req.session.admin = undefined
        res.end('OK')
      },
    },
    garena: {
      // Default garena oauth settings
      authorize_url: `https://auth.garena.com/oauth/login`,
      access_url: `https://auth.garena.com/oauth/token`,
      user_info_url: `https://auth.garena.com/oauth/user/info/get`,
      inspect_token_url: `https://auth.garena.com/oauth/token/inspect`,
      logout_url: `https://auth.garena.com/oauth/logout`,
      oauth: 2,
      // Custom settings
      key: process.env.GARENA_APP_ID || 0, // Garena app id
      secret: 'N/A',
      response_type: 'token',
      custom_params: {
        all_platforms: 1,
        locale: process.env.LOCALE,
      },
      overrides: {
        facebook: {
          custom_params: { platform: 3 },
        }, // overide facebook => /connect/garena/facebook
      },
      callbackHook: {
        model: 'User',
        key: 'openId',
        mode: 'upsert',
        infoUrl: 'https://auth.garena.com/oauth/user/info/get',
        info2Model: ({ uid, open_id: openId, nickname, platform, icon }, req) => ({ uid, openId, name: nickname, platform, avatar: icon, ip: req.ip }),
        redirect: (req, res) => {
          try {
            const referer = new URL(req.session.grant.referer)

            if (referer.host === homepage.host && referer.protocol === homepage.protocol) {
              return referer
            }

            return homepage
          } catch {
            return homepage
          }
        },
      },
      logoutHook: async (req, res) => {
        const { user: { accessToken } = {} } = req.session
        req.session.user = undefined

        try {
          const fetch = require('../../helpers/fetch')
          const resp = await fetch(`https://auth.garena.com/oauth/token/inspect?token=${accessToken}`)

          const { error } = await resp.json()
          if (error) throw Error('InvalidAccesstoken')

          res.redirect(`https://auth.garena.com/oauth/logout?access_token=${accessToken}&format=redirect&redirect_uri=${homepage}`)
        } catch {
          res.redirect(homepage)
        }
      },
    }
  }

  return config
}
