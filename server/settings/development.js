import { URL } from 'url'

export default {
  homepage: new URL('http://rpl2020.rov.in.th'),
  graphql: {
    upload: true,
    subscriptions: true,
  }
}
