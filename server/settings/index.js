import fs from 'fs'
import path from 'path'
import { URL } from 'url'
import { merge } from 'lodash'

let config = {
  homepage: new URL(`https://${process.env.DOMAIN || 'event.game.garena.vn'}`),
  helmet: {},
  timezone: 'Asia/Ho_Chi_Minh',
  session: {
    cookie: { maxAge: 342000000 }, /** expire time default 95 hours */
    name: 'session', /** (string) cookie key */
    secret: process.env.APP_KEY || 'development-secret', /** (string) secret key */
    resave: false,
    saveUninitialized: false,
    unset: 'destroy',
  },
  qjob: {
    /** Bulljs QueueOptions or false to disable queue jobs */
    defaultJobOptions: {
      stackTraceLimit: 16,
    },
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT,
    db: process.env.REDIS_DB,
    keyPrefix: process.env.REDIS_PREFIX,
    password: process.env.REDIS_PASSWORD,

  },
  graphql: {
    defaultEndpoint: '/graphql',
    adminEndpoint: '/admin/graphql',
    subscriptions: false, /** (string|boolean) String defining the path for subscriptions (default: defaultEndpoint). Set to false to disable subscriptions **/
    upload: false, /** (object|boolean) Enable apollo-upload or not **/
  },
}

const { env: { NODE_ENV: env = 'development' } } = process
const extendConfigs = ['database.js']
extendConfigs.forEach((fileName) => {
  const attr = fileName.substr(0, fileName.lastIndexOf('.'))
  config[attr] = require(`./${fileName}`)[env]
})

// import setting by enviroment
try { config = merge(config, require(`./${env}.js`).default) } catch (e) { }

// run initializer
for (const file of fs.readdirSync(path.join(__dirname, 'initializers'))) {
  if (file.charAt(0) !== '.' && file.slice(-3) === '.js') {
    const initializer = require(`./initializers/${file}`)
    config = (initializer.default || initializer)(config)
  }
}

export default config
