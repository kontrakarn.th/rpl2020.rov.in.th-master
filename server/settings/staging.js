import { URL } from 'url'

const homepage = new URL('https://staging.event.garena.vn')

export default {
  homepage: homepage,
  grant: {
    server: {
      protocol: homepage.protocol.slice(0, -1),
      host: homepage.host,
    },
  },
}
