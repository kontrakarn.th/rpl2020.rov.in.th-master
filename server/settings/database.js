const moment = require('moment')

const TIMEZONE = moment().format('Z')
console.log('[TZ] %s %s', Intl.DateTimeFormat().resolvedOptions().timeZone, TIMEZONE)

const defaultConfig = {
  client: 'mysql',
  connection: {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 3306,
    user: process.env.DB_USER || 'root',
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME || 'ved_node_starter_kit_dev',
    timezone: TIMEZONE,
    charset: 'utf8mb4',
    dateStrings: ['DATE'],
  },
  pool: {
    min: 1,
    max: 10,
    afterCreate: (conn, done) => conn.query(`SET @@session.time_zone = ?`, [TIMEZONE], (err) => done(err, conn)),
  },
  migrations: {
    directory: './server/db/migrations',
  },
  seeds: {
    directory: './server/db/seeds',
  },
}

module.exports = {
  test: defaultConfig,
  staging: defaultConfig,
  production: defaultConfig,
  development: defaultConfig,
}
