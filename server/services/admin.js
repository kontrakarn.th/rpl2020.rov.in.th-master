import { State } from '../db'
import { cache } from '../kit/redis-client'

export const typedefs = `
extend type Query {
  regionPoints: Int
}
`

export const resolvers = {
  Query: {
    regionPoints: () => cache('admin:rps', () => State.query().sum('point as p').first().then(row => row.p), 30),
  },
}
