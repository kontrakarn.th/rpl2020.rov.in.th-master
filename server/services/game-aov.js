import moment from 'moment'
import qs from 'querystring'
import fetch from '../helpers/fetch'

import logger from '../kit/logger'

const LANGUAGE = 'vn'
const BASE_PATH = `/game/aov/tidy/v1/`
const DOMAIN = process.env.GAMEWS_HOST
const BASE_URL = `https://${DOMAIN}${BASE_PATH}`
const CLIENT_ID = process.env.GAMEWS_CLIENT_ID

const request = async (endpoint, query, method = 'GET', body = null, logLevel = 'trace') => {
  let url, json = null
  try {
    url = BASE_URL + endpoint
    if (query) url += '?' + qs.stringify(query)

    const resp = await fetch(url, {
      method,
      headers: { 'client-id': CLIENT_ID },
      body: body && JSON.stringify(body),
    })

    json = await resp.json()
    if (resp.statusCode !== 200) throw Error(json.msg || resp.statusMessage)

    logger[logLevel](`[AOV] SUCCESS %s %s body: %o, status: %d, resp: %o`, method, url, body, resp.statusCode, json)
    return json
  } catch (err) {
    logger.error(`[AOV] ERROR %s %s body: %o, error: %s, resp: %o`, method, url, body, err.message, json)
    throw err
  }
}

const get = async (endpoint, partition, query, logLevel) => {
  if (partition) {
    if (query) query.partition = partition
    else query = { partition }
  }
  return request(endpoint, query, 'GET', undefined, logLevel)
}

const post = async (endpoint, partition, body, logLevel = 'info') => {
  return request(endpoint, partition && { partition }, 'POST', body, logLevel)
}

// MATCH
export const dailyGames = (tencentId, partition) => get(`dailygames/${tencentId}`, partition)
export const matchHistory = (tencentId, partition, mode) => get(`matchhistory/${tencentId}`, partition, { mode })

// BALANCE
export const playerBalance = (tencentId, partition) => get(`playerinfo/balance/${tencentId}`, partition)
export const deductCoupon = (tencentId, value, source, serial, partition) => post(`deduct/coupon/${tencentId}`, partition, {
  Value: value, Source: source, Serial: serial,
})
export const deductDiamond = (tencentId, value, partition) => post(`deduct/diamond/${tencentId}`, partition, {
  Value: value,
})
export const deductGold = (tencentId, value, partition) => post(`deduct/gold/${tencentId}`, partition, {
  Value: value,
})

// INVENTORY
export const checkBackpack = (tencentId, queryList, partition) => post(`inventory/check/backpack/${tencentId}`, partition, {
  QueryList: queryList,
}, 'trace')
export const checkHero = (tencentId, heroId, partition) => get(`inventory/check/hero/${tencentId}/${heroId}`, partition)
export const getHeroes = (tencentId, partition) => get(`inventory/heroes/${tencentId}`, partition)
export const getSkins = (tencentId, partition) => get(`inventory/skins/${tencentId}`, partition)
export const getHeroesWithSkins = (tencentId, partition) => get(`inventory/heroeswithskins/${tencentId}`, partition)
export const getOthers = (tencentId, partition) => get(`inventory/others/${tencentId}`, partition)
export const sendItem = (tencentId, packageId, activityId, moduleId, eventName, serial, partition) => post(`senditem/${tencentId}`, partition, {
  AmsInfo: `IEGAMS-${activityId}-${moduleId}`,
  AmsSerial: serial || amsSerial(eventName, activityId, moduleId),
  EventName: eventName,
  PackageGroupID: packageId,
  Language: LANGUAGE,
})

// KICK
export const kickPlayer = (tencentId, partition) => post(`kick/${tencentId}`, partition)

// CHARACTER
export const getCharacters = (openId) => get(`player/${openId}`)
export const getCharacterNames = (openId) => get(`player/${openId}/characters`)

// TENCENT2OPENID
export const getTencentId = (openId) => get(`player/${openId}/tencentopenid`).then(resp => resp.TencentOpenID)

// PLAYER
export const playerInfo = (tencentId, partition) => request(`playerinfo/${tencentId}`, partition)

// AMSSERIAL
const chance = require('chance')()
export const amsSerial = (eventName, activityId, moduleId) =>
  `AMS-${eventName}-${moment().format('MMDDHHmmss')}-${chance.string({
    length: 6,
    pool: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789',
  })}-${activityId}-${moduleId}`
