import { raw, ref } from 'objection'

import { State, RegionMilestone, Week, Mission, Configuration } from '../db'

import { cache } from '../kit/redis-client'

export const chance = require('chance')()

export const cacheMissionById = (id) => cache(
  `ms:${id}`,
  () => Mission.query()
    .findById(id)
    .where('action', '<', 128)
    .select('id', 'point'),
  10,
)

export const cacheMission = (type, action) => cache(
  `m:${type}:${action}`,
  () => Mission.query()
    .where(raw('(??>>4)', 'id'), type)
    .where('action', action)
    .select('id', 'point')
    .first(),
  30,
)

export const currentWeekId = () => cache('cWeek', async () => {
  const week = await Week.query()
    .where('startTime', '<=', new Date())
    .select('id')
    .orderBy('startTime', 'desc')
    .orderBy('id', 'desc')
    .first()

  return week?.id ?? 0
}, 30)

const MUL = 1e2
export const myRegionPoint = () => cache('myrepo', async () => {
  const { totalPoint } = await State.query().sum('point as totalPoint').first()

  const miles = await RegionMilestone.query()
    .select('id', 'requiredPoints')
    .orderBy('id', 'asc')

  let idx = -1

  for (let i = miles.length - 1; i >= 0; i--) {
    if (totalPoint >= miles[i].requiredPoints) {
      idx = i
      break
    }
  }

  if (idx >= 0) {
    const curr = miles[idx]
    const next = miles[idx + 1]

    if (next) {
      return MUL * (1 + idx + (totalPoint - curr.requiredPoints) / (next.requiredPoints - curr.requiredPoints))
    } else {
      return MUL * (idx + totalPoint / curr.requiredPoints)
    }
  } else {
    return MUL * (totalPoint / miles[0].requiredPoints)
  }
}, 30)

export const checkMissionStartTime = async (isThrow = true) => {
  if (Date.now() >= await Configuration.getBeginTime()) {
    return true
  }

  if (isThrow) {
    throw Error('DailyMissionNotStart')
  }
  return false
}

export const checkMissionEndTime = async (isThrow = true) => {
  if (Date.now() <= await Configuration.getEndTime()) {
    return true
  }

  if (isThrow) {
    throw Error('EventEnded')
  }
  return false
}

export const checkClaimEndTime = async (isThrow = true) => {
  if (Date.now() <= await Configuration.getClaimEndTime()) {
    return true
  }

  if (isThrow) {
    throw Error('ClaimTimeEnded')
  }
  return false
}
