import moment from 'moment'
import { raw, ref } from 'objection'

import {
  State,
  Predict,
  Refer,
  History,
  Item,
  Mission,
  RegionMilestone,
  PersonMilestone,
  Vod,
  Week,
  Transaction,
  PlayerRole,
  UserInfo,
  Lucky,
  Configuration,
} from '../db'

import redis from '../kit/redis-client'
import { currentWeekId, cacheMission, checkMissionStartTime, checkMissionEndTime, checkClaimEndTime, chance, myRegionPoint } from './common'

export const typeDefs = `
enum MilestoneType {
  REGION
  PERSON
}

type ReferResponse {
  point: Int
  charName: String
  state: State
}

type MissionResponse {
  point: Int
  item: Item
  state: State
}

type MilestoneResponse {
  item: Item
  state: State
}

input UserInfoInput {
  fullname: String!
  phone: String!
  address: String!
}

type Mutation {
  updateUserInfo(data: UserInfoInput!): UserInfo @auth

  refer(referCode: String!): ReferResponse @ulock @auth
  claimVod(vodId: UInt8!): MissionResponse! @ulock @auth
  claimMission(missionId: UInt8!): MissionResponse! @ulock @auth
  predict(weekId: UInt32!, playerIds: [UInt32!]!): Predict @ulock @auth
  claimMilestone(milestoneId: UInt8!, type: MilestoneType!, partitionId: UInt16!): MilestoneResponse! @ulock @auth
}
`

const predict = async (parent, { weekId, playerIds }, { session: { user } }, info) => {
  if (playerIds.length !== 5) throw Error('InvalidPlayerIds')

  const week = await Week.query().findById(weekId)

  if (!week) throw Error('NotExistWeek')

  if (Date.now() < week.startTime) throw Error('WeekNotStarted')
  if (Date.now() > week.endTime) throw Error('WeekEnded')

  if (await Predict.query().findById([weekId, user.id]).select(1)) throw Error('MvpVoted')

  const playerRoles = playerIds.map((v, idx) => [v, idx])

  const { c } = await PlayerRole.query().findByIds(playerRoles).count('* as c').first()

  if (c !== 5) throw Error('InvalidPlayerIds')

  return State.transaction(async trx => {
    await Predict.query(trx).insert({
      weekId,
      userId: user.id,
      playerIds,
    })

    await History.query(trx).insert({
      userId: user.id,
      type: 4,
      content: weekId,
    })

    await PlayerRole.query(trx)
      .findByIds(playerRoles)
      .increment('hot', 1)

    return {
      id: weekId,
      playerIds,
    }
  })
}

const REFLIMIT = 5

const refer = async (parent, { referCode }, { session: { user } }, info) => {
  await checkMissionStartTime(true)
  await checkMissionEndTime(true)

  const today = moment().format('YYYY-MM-DD')
  const [dstId, nonce] = State.checkCode(referCode)

  if (dstId === user.id) throw Error('ReferYourself')
  if (await Refer.query().where({ srcId: user.id, dstId: dstId, date: today }).first().select(1)) throw Error('HadRefered')

  const dst = await State.query().findById(dstId).where('nonce', nonce)

  if (!dst) throw Error('NotExistCode')

  const src = await State.query().findById(user.id).throwIfNotFound()

  await Promise.all([dst.checkDay(today), src.checkDay(today)])

  if (src.ref >= REFLIMIT) throw Error('ReachRefLimit')
  if (dst.rev >= REFLIMIT) throw Error('ReachRevLimit')

  const refMission = await cacheMission(1, 129)
  const revMission = await cacheMission(1, 130)

  return State.transaction(async trx => {
    const a1 = await src.$query(trx).where('ref', '<', REFLIMIT)
      .update({
        ref: raw('??+?', 'ref', 1),
        point: raw('??+?', 'point', refMission.point),
      })
    if (!a1) throw Error('ReachRefLimit')

    const a2 = await dst.$query(trx).where('rev', '<', REFLIMIT)
      .update({
        rev: raw('??+?', 'rev', 1),
        point: raw('??+?', 'point', revMission.point),
      })
    if (!a2) throw Error('ReachRevLimit')

    await Refer.query(trx).insert({
      srcId: src.id,
      dstId: dst.id,
      date: today,
    })

    await History.query(trx).insert({
      userId: src.id,
      type: 0,
      content: refMission.id,
      point: refMission.point,
    })

    await History.query(trx).insert({
      userId: dst.id,
      type: 0,
      content: revMission.id,
      point: revMission.point,
    })

    src.ref += 1
    src.point += refMission.point

    return {
      point: refMission.point,
      charName: dst?.partitions?.[0]?.name,
      state: src,
    }

  })
}

const claimMission = async (parent, { missionId }, { session: { user } }, info) => {
  await checkMissionEndTime(true)

  const type = missionId >> 4
  if (type !== 0) {
    await checkMissionStartTime(true)
  }

  const mission = await Mission.query().findById(missionId).where('action', '<', 128)
  //  const mission = await Mission.query().findById(missionId)

  if (!mission) throw Error('InvalidMissionId')

  const mask = 1 << (mission.id & 0x0f)
  const state = await State.query().findById(user.id).throwIfNotFound()

  const field = type ? 'daily' : 'weekly'

  if (state[field] & mask) throw Error('MissionClaimed')

  if (type === 0) {
    await state.checkWeek(await currentWeekId())

    if (!state.checkedWeek) throw Error('WeekNotInit')
  }

  if (mission.action === 1) {
    if (! await Predict.query().findById([state.checkedWeek, user.id]).select(1)) {
      throw Error('NotConfirmMvpYet')
    }
  }

  return State.transaction(async trx => {
    const af = await state.$query(trx)
      .whereNot(field, '&', mask)
      .update({
        [field]: raw('??|?', field, mask),
        point: raw('??+?', 'point', mission.point),
      })

    if (!af) throw Error('MissionClaimed')

    await History.query(trx).insert({
      userId: user.id,
      type: 0,
      content: mission.id,
      point: mission.point,
    })

    let item

    if ((~state.status & 1) && state.tencentId && state.partitions?.length) {
      const useLock = 'OK' === await redis.set('luckylock', '1', 'NX', 'EX', 600)
      try {
        if (useLock) {
          const lucLoots = await Lucky.query()
            .where('active', true)
            .where('current', '<', ref('limit'))
            .where('chance', '>', 0)
            .select('id', 'chance', 'itemId')

          if (lucLoots.length) {
            const luc = chance.pickone(lucLoots)
            const drop = chance.bool({ likelihood: luc.chance / 1e4 })

            if (drop) {
              item = await Item.query().findById(luc.itemId).throwIfNotFound()

              const laf = await luc.$query(trx).increment('current', 1).where('current', '<', ref('limit'))
              if (!laf) throw Error('InvalidAction')

              await state.$query(trx).update({ status: raw('??|?', 'status', 1) })
              state.status |= 1

              await History.query(trx).insert({
                userId: user.id,
                type: 6,
                content: luc.itemId,
              })

              await Transaction.query(trx).insert({
                userId: user.id,
                item,
              })
            }
          }
        }

      } catch (err) {
        throw err
      } finally {
        if (useLock) redis.del('luckylock')
      }
    }

    state[field] |= mask
    state.point += mission.point

    return {
      item,
      state,
      point: mission.point,
    }
  })
}

const claimVod = async (parent, { vodId }, { session: { user } }, info) => {
  await checkMissionStartTime(true)
  await checkMissionEndTime(true)

  if (vodId > 31) throw Error('InvalidVodId')

  const vod = await Vod.query().findById(vodId)
  if (!vod) throw Error('NotExistVod')

  const vodMission = await cacheMission(1, 132)
  if (!vodMission) throw Error('NotExistVodMission')

  const mask = 1 << vodId
  const state = await State.query().findById(user.id).throwIfNotFound()

  if (state.once & mask) throw Error('VodClaimed')

  const point = vodMission.point

  return State.transaction(async trx => {
    const af = await state.$query(trx)
      .whereNot('once', '&', mask)
      .update({
        once: raw('??|?', 'once', mask),
        point: raw('??+?', 'point', point),
      })

    if (!af) throw Error('VodClaimed')

    await History.query(trx).insert({
      userId: user.id,
      type: 3,
      content: vodId,
      point,
    })

    state.once |= mask
    state.point += point

    return {
      point,
      state,
    }
  })
}

const claimMilestone = async (parent, { milestoneId, type, partitionId }, { session: { user } }, info) => {
  await checkClaimEndTime(true)

  type = type.toLowerCase()

  const milestone = await (type === 'region' ? RegionMilestone : PersonMilestone)
    .query()
    .findById(milestoneId)

  if (!milestone) throw Error('InvalidMilestoneId')

  if (!(Date.now() >= milestone.startAt)) throw Error(type === 'region' ? 'RegionMilestoneNotStart' : 'PersonMilestoneNotStart')

  const mask = 1 << milestoneId
  const state = await State.query().findById(user.id).throwIfNotFound()

  if (!(state.tencentId && state.partitions?.length)) throw Error('DontHaveIngameCharactor')

  if (state.partitions.findIndex(row => row.id === partitionId) < 0) throw Error('InvalidPartitionId')

  if (type === 'region') {
    if (state[type] & mask) throw Error('RegionMilestoneClaimed')

    if (!(state.point >= milestone.minPoints)) throw Error('PersonMilestoneDontEnoughPoint')

    const rPoint = await myRegionPoint()
    if (!((rPoint / 100 | 0) >= milestoneId)) throw Error('RegionMilestoneDontEnoughPoint')
  } else {
    if (state[type] & mask) throw Error('PersonMilestoneClaimed')

    if (!(state.point >= milestone.requiredPoints)) throw Error('PersonMilestoneDontEnoughPoint')
  }

  const item = await Item.query().findById(milestone.itemId).throwIfNotFound()

  item.partitionId = partitionId

  return State.transaction(async trx => {
    const af = await state.$query(trx)
      .whereNot(type, '&', mask)
      .update({
        [type]: raw('??|?', type, mask),
      })

    if (!af) throw Error('MilestoneClaimed')

    await History.query(trx).insert({
      userId: user.id,
      type: type === 'region' ? 1 : 2,
      content: milestoneId,
    })

    await Transaction.query(trx).insert({
      userId: user.id,
      item,
    })

    state[type] |= mask

    return {
      item,
      state,
    }
  })
}

const updateUserInfo = async (parent, { data }, { session: { user } }, info) => {
  data.id = user.id
  await UserInfo.query().upsertGraph(data, { insertMissing: true })
  await State.query().findById(user.id).update({ status: raw('??|?', 'status', 2) })

  return data
}

export default {
  refer,
  predict,
  claimVod,
  claimMission,
  claimMilestone,
  updateUserInfo,
}
