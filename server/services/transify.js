import qs from 'querystring'
import fetch from '../helpers/fetch'

import logger from '../kit/logger'

const DOMAIN = 'transify.seagroup.com'
const BASE_URL = `https://${DOMAIN}`
const AUTHORIZATION = `Token 014dad385200b37a49582afe7a3c35ffe4a3c283`

export const RESOURCE_ID = 1213
export const LANG_ID = process.env.LANG_ID || 4

const request = async (endpoint, query, method = 'GET', body = null, logLevel = 'trace') => {
  let url, json = null
  try {
    url = BASE_URL + endpoint
    if (query) url += '?' + qs.stringify(query)

    const resp = await fetch(url, {
      method,
      headers: { 'Authorization': AUTHORIZATION },
      body: body && JSON.stringify(body),
    })

    json = await resp.json()
    if (resp.statusCode !== 200) throw Error(json.msg || resp.statusMessage)

    logger[logLevel](`[TRANSIFY] SUCCESS %s %s body: %o, status: %d, resp: %o`, method, url, body, resp.statusCode, json)
    return json
  } catch (err) {
    logger.error(`[TRANSIFY] ERROR %s %s body: %o, error: %s, resp: %o`, method, url, body, err.message, json)
    throw err
  }
}

export const getResources = (resourceId = RESOURCE_ID, languageId = LANG_ID, handler = 'json') => request(`/api/resources/${resourceId}/languagestoken/${languageId}/export/${handler}`)
