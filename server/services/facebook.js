import { URL } from 'url'
import Busboy from 'busboy'

import settings from '../settings'
import fetch from '../helpers/fetch'

import { Configuration } from '../db'
import redis from '../kit/redis-client'

const stream2buffer = async (stream) => {
  const arr = []
  for await (const buf of stream) {
    arr.push(buf)
  }
  return Buffer.concat(arr)
}

const NAME_REGEX = /^[0-9a-f]{32}$/
const keyimage = name => `im${name}`

export const upload = async (req, res) => {
  try {
    const name = req.params.name
    if (!NAME_REGEX.test(name)) throw Error('InvalidName')

    const busboy = new Busboy({
      headers: req.headers
    })

    await new Promise((resolve, reject) => {
      busboy.on('file', (fieldname, file, filename, encoding, mimetype) => {
        stream2buffer(file)
          .then(buf => redis.setBuffer(keyimage(name), buf, 'EX', 60))
          .catch(reject)
      })

      busboy.on('finish', () => {
        resolve()
      })

      req.pipe(busboy)
    })

    res.send('OK')
  } catch (err) {
    res.throw(err.message)
  }
}

const fallbackImage = async (req, res) => {
  const meta = await Configuration.getMeta()
  res.redirect(meta.ogImage)
}

export const image = async (req, res) => {
  try {
    const name = req.params.name

    if (!NAME_REGEX.test(name)) {
      return await fallbackImage(req, res)
    }

    const buff = await redis.getBuffer(keyimage(name))

    if (!buff) {
      return await fallbackImage(req, res)
    }

    res.send(buff, 200, {
      'Content-Type': 'image/jpeg',
      'Cache-Control': 'public, max-age=2419200, immutable',
    })
  } catch (err) {
    res.throw(err.message, 400)
  }
}

export const share = async (req, res) => {
  try {
    const name = req.params.name
    const meta = await Configuration.getMeta()
    const ogImage = NAME_REGEX.test(name) ? `/fb/im/${name}` : meta.ogImage


    res.send(`
<html>
<head>
  <meta property="og:title" content="${meta.ogTitle}" /> 
  <meta property="og:image" content="${ogImage}" /> 
  <meta property="og:description" content="${meta.ogDescription}" />
</head> 
`, 200, {
      'Content-Type': 'text/html',
    })
  } catch (err) {
    res.throw(err.message, 404)
  }
}

export const proxy = async (req, res) => {
  try {
    const url = new URL(req.query.p)
    if (url.host === settings.homepage.host) throw Error('DPSH')

    const resp = await fetch(url)

    res.writeHead(resp.statusCode, resp.statusMessage, resp.headers)
    resp.pipe(res)

  } catch (err) {
    res.throw(err.message, 400)
  }
}
