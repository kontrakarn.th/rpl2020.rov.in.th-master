import { GraphQLUpload } from 'graphql-upload'

import {
  GraphQLJSON,
  GraphQLDate, GraphQLDateTime,
  GraphQLInt8, GraphQLInt16, GraphQLInt24, GraphQLInt32, GraphQLInt64,
  GraphQLUInt8, GraphQLUInt16, GraphQLUInt24, GraphQLUInt32, GraphQLUInt64,
} from '../kit/graphql/scalars'

import Types, { typeDefs as types } from './typedefs'
import { typeDefs as directives } from './directives'
import Query, { typeDefs as queries } from './queries'
import Mutation, { typeDefs as mutations } from './mutations'
import Subscription, { typeDefs as subscriptions } from './subscriptions'

const scalars = `
scalar JSON
scalar Upload

scalar Date
scalar DateTime

scalar Int8
scalar UInt8

scalar Int16
scalar UInt16

scalar Int24
scalar UInt24

scalar Int32
scalar UInt32

scalar Int64
scalar UInt64
`

const schema = `
schema {
  ${queries ? 'query: Query' : ''}
  ${mutations ? 'mutation: Mutation' : ''}
  ${subscriptions ? 'subscription: Subscription' : ''}
}
`

export { default as schemaDirectives } from './directives'

export const resolvers = {
  JSON: GraphQLJSON,
  Date: GraphQLDate,
  Upload: GraphQLUpload,
  DateTime: GraphQLDateTime,
  Int8: GraphQLInt8,
  Int16: GraphQLInt16,
  Int24: GraphQLInt24,
  Int32: GraphQLInt32,
  Int64: GraphQLInt64,
  UInt8: GraphQLUInt8,
  UInt16: GraphQLUInt16,
  UInt24: GraphQLUInt24,
  UInt32: GraphQLUInt32,
  UInt64: GraphQLUInt64,
}

if (Types) Object.assign(resolvers, Types)
if (queries) Object.assign(resolvers, { Query })
if (mutations) Object.assign(resolvers, { Mutation })
if (subscriptions) Object.assign(resolvers, { Subscription })

export const typeDefs = [
  scalars,
  directives,
  schema,
  types,
  queries,
  mutations,
  subscriptions,
]
