import { URL } from 'url'
import moment from 'moment'
import { raw, ref } from 'objection'

import settings from '../settings'
import fetch from '../helpers/fetch'

import {
  State,
  Role, Player, UserInfo,
  Region, RegionMilestone, PersonMilestone,
  Week, Predict, Mission, History, Refer, Vod,
  Transaction,
  Lucky, Item,
  Configuration,
} from '../db'

import redis, { cache } from '../kit/redis-client'
import { chance, currentWeekId, myRegionPoint, cacheMission, checkMissionStartTime, checkMissionEndTime } from './common'

import { getTencentId, getCharacterNames, dailyGames, matchHistory } from './game-aov'
import { getResources } from './transify'

export const typeDefs = `
type Query {
  user: User @auth
  state: State @auth
  userInfo: UserInfo @auth
  predict(weekId: UInt32!): Predict @auth
  
  myRegionPoint: Float
  regions: [Region] @cache(ttl: 30)
  regionMilestones: [RegionMilestone] @cache(ttl: 30)
  personMilestones: [PersonMilestone] @cache(ttl: 30)

  lucky: Item @cache
  item(itemId: UInt32!): Item @cache

  roles: [Role] @cache(ttl: 30)
  players: [Player] @cache
  
  rule: String
  entities: JSON @cache(ttl: 36)

  lastweekVote: [WeekResult] @cache
  livetimeVote: [UInt32!] @cache(ttl: 30)
  week(weekId: UInt32!): Week @cache

  vods: [Vod] @cache
  missions: [Mission] @cache

  histories(offset: UInt16!, limit: UInt8!): [History] @auth
  references(offset: UInt16!, limit: UInt8!): [Reference] @auth
  dashboards(offset: UInt16!, limit: UInt8!): [Dashboard] @cache(ttl: 30)

  meta: JSON
  transactions: [Transaction] @cache(ttl: 36)

  beginTime: DateTime

  livestream: JSON
}
`

const userInfo = async (parent, args, { session: { user } }, info) => {
  const i = await UserInfo.query().findById(user.id)
  return i || { id: user.id }
}

const entities = (parent, args, ctx, info) => getResources()

const regions = async () => {
  const regions = await Region.query()

  return Promise.all(regions.map(async region => {
    try {
      const gqlEndpoint = new URL('/graphql', region.url)

      if (gqlEndpoint.host === settings.homepage.host) {

        region.isMyRegion = true
        region.regionPoint = await myRegionPoint()
      } else {

        region.regionPoint = await fetch(gqlEndpoint, {
          method: 'POST',
          body: `{"query": "{myRegionPoint}"}`,
        }).then(res => res.json()).then(json => json.data.myRegionPoint)
      }

    } catch {

      region.regionPoint = 0
    }

    return region
  }))
}

const missions = () => Mission.query()
const personMilestones = () => PersonMilestone.query().withGraphFetched('item')
const regionMilestones = async () => RegionMilestone.query().withGraphFetched('item')

const roles = () => Role.query()
const vods = () => Vod.query().orderBy('id', 'desc')
const players = () => Player.query().withGraphFetched('roles')

const week = (parent, { weekId }, ctx, info) => Week.query().findById(weekId)
const predict = async (parent, { weekId }, { session: { user } }, info) => {
  const predict = await Predict.query().findById([weekId, user.id]).select('weekId as id', 'playerIds')

  return predict ?? { id: weekId }
}

const lastweekVote = async (parent, args, ctx, info) => {
  return Week.query()
    .where(raw('JSON_LENGTH(??)', 'playerIds'), 5)
    .orderBy('id', 'desc')
    .select('id', 'name', 'playerIds', 'mvps')
}

const livetimeVote = async (parent, args, ctx, info) => {
  const week = await Week.query()
    .findById(await currentWeekId())
    .select('livetime')
    .first()

  return week?.livetime
}

const user = (parent, args, { session: { user } }, info) => user

const state = async (parent, args, { session: { user } }, info) => {
  const today = moment().format('YYYY-MM-DD')

  let state = await State.query().findById(user.id)

  if (!state) {
    state = await State.query().insertAndFetch({
      id: user.id,
      checkedDate: today,
      nonce: chance.natural({ min: 1e8, max: 4e9 }),
    })
  }

  if (!state.tencentId && 'OK' === await redis.set(`tc:${user.id}`, '1', 'NX', 'EX', 600)) {
    try {
      const tencentId = await getTencentId(user.openId)
      await state.$query().update({ tencentId })
    } catch { }
  }

  if (state.tencentId && !state.partitions?.length && 'OK' === await redis.set(`pt:${user.id}`, '1', 'NX', 'EX', 600)) {
    try {
      const chars = await getCharacterNames(user.openId)
      await state.$query().update({
        partitions: chars
          .map(row => ({
            name: row.Name,
            id: parseInt(row.Partition),
          }))
          .filter(row => row.id !== 1001)
      })
    } catch { }
  }

  await state.checkDay(today)

  const weekId = await currentWeekId()
  await state.checkWeek(weekId)

  if (state.tencentId && state.partitions?.length && await checkMissionEndTime(false)) {
    if (await checkMissionStartTime(false)) {
      const dMission = await cacheMission(1, 128)
      if (dMission) {
        const { id, point } = dMission
        const mask = 1 << (id & 0x0f)

        if (~state.daily & mask && 'OK' === await redis.set(`md:${id}:${user.id}`, '1', 'NX', 'EX', 600)) {
          let condition = false

          for (const { id: partitionId } of state.partitions) {
            const dg = await dailyGames(state.tencentId, partitionId)

            if (Object.values(dg).some(v => v > 0)) {
              condition = true
              break
            }
          }

          if (condition) {
            await State.transaction(async trx => {
              const af = await state.$query(trx)
                .whereNot('daily', '&', mask)
                .update({
                  daily: raw('??|?', 'daily', mask),
                  point: raw('??+?', 'point', point),
                })
              if (!af) throw Error('MissionClaimed')

              await History.query(trx).insert({
                userId: user.id,
                type: 0,
                content: id,
                point: point,
              })

            })

            state.point += point
            state.daily |= mask
          }
        }
      }
    }

    if (state.checkedWeek) {
      const wMission = await cacheMission(0, 131)

      if (wMission) {
        if (state.weekWin < 0) {
          await state.updateWeekWin()
        } else {
          const { id, point } = wMission
          const mask = 1 << (id & 0x0f)

          if (~state.weekly & mask && 'OK' === await redis.set(`mw:${id}:${user.id}`, '1', 'NX', 'EX', 600)) {

            const { '5V5': { WinCount: winCount } } = await matchHistory(state.tencentId, state.partitions[0].id, 1)
            const condition = winCount >= state.weekWin + 3

            if (condition) {
              await State.transaction(async trx => {
                const af = await state.$query(trx)
                  .whereNot('weekly', '&', mask)
                  .update({
                    weekly: raw('??|?', 'weekly', mask),
                    point: raw('??+?', 'point', point),
                  })
                if (!af) throw Error('MissionClaimed')

                await History.query(trx).insert({
                  userId: user.id,
                  type: 0,
                  content: id,
                  point: point,
                })
              })

              state.point += point
              state.weekly |= mask
            }
          }
        }
      }
    }
  }

  return state
}

const rule = () => Configuration.getDefault('rules', () => ({
  value: 'rules',
  type: 'Text',
  description: 'Rules',
}))

const histories = async (parent, { offset, limit }, { session: { user } }, info) => {
  return History.query().where('userId', user.id).limit(limit).offset(offset).orderBy('id', 'desc')
}

const references = async (parent, { offset, limit }, { session: { user } }, info) => {
  const arr = await Refer.query()
    .select('id', 'srcId', 'dstId', 'createdAt')
    .where('dstId', user.id)
    // .where(builder => builder.where('srcId', user.id).orWhere('dstId', user.id))
    .limit(limit).offset(offset)
    .orderBy('id', 'desc')
    .withGraphFetched('[src]')
  // .withGraphFetched('[src,dst]')

  return arr.map(row => {
    const ret = { id: row.id, createdAt: row.createdAt }

    if (user.id === row.dstId) {
      ret.type = 1
      ret.charName = row?.src?.charName
    } else {
      ret.type = 0
      ret.charName = row?.dst?.charName
    }

    return ret
  })
}

const dashboards = async (parent, { offset, limit }, { session: { user } }, info) => {
  if (offset >= 1000) return []

  return State.query()
    .limit(limit)
    .offset(offset)
    .select('id', raw('??->>?', 'partitions', '$[0].name').as('charName'), 'point', 'lastTime')
    .orderBy('point', 'desc')
    .orderBy('lastSeq', 'asc')
    .orderBy('id', 'asc')
}

const lucky = async (parent, args, ctx, info) => {
  const luc = await Lucky.query().where('active', true).first().select('itemId')

  if (!luc) return null

  return Item.query().findById(luc.itemId).select('id', 'name', 'icon')
}

const item = async (parent, { itemId }, ctx, info) => {
  return Item.query().findById(itemId).select('id', 'name', 'icon')
}

const transactions = async (parent, args, ctx, info) => {
  const txns = await Transaction.query()
    .withGraphFetched('state')
    .select(raw('??->>?', 'item', '$.name').as('itemName'), 'createdAt')
    .orderBy('id', 'desc')
    .limit(20)

  return txns.map(txn => {
    txn.charName = txn.state.charName
    return txn
  })
}

const livestream = async () => {
  const endTime = await Configuration.getLivestreamEndTime()
  if (Date.now() < endTime) return Configuration.getLivestreamMeta()
}

export default {
  user,
  state,
  predict,
  userInfo,
  histories,
  references,
  transactions,

  item,
  lucky,
  lastweekVote,
  livetimeVote,

  rule,
  week,
  vods,
  roles,
  regions,
  players,
  entities,
  missions,
  dashboards,
  myRegionPoint,
  regionMilestones,
  personMilestones,

  livestream,
  meta: Configuration.getMeta,
  beginTime: Configuration.getBeginTime,
}
