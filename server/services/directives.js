
import { defaultFieldResolver } from 'graphql'
import { SchemaDirectiveVisitor } from '@graphql-tools/utils'

import logger from '../kit/logger'
import redis, { cache } from '../kit/redis-client'

import { Configuration } from '../db'

export const typeDefs = `
directive @log on FIELD_DEFINITION
directive @auth on FIELD_DEFINITION
directive @ulock on FIELD_DEFINITION
directive @cache(key: String, ttl: UInt16 = 18) on FIELD_DEFINITION
`

class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field) {
    const { resolve = defaultFieldResolver } = field
    field.resolve = async function (parent, args, ctx, info) {
      const { user } = ctx.session
      if (!user) throw new Error(`Unauthorized`)
      return resolve.apply(this, arguments)
    }
  }
}

class UserLockDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field) {
    const { resolve = defaultFieldResolver } = field
    field.resolve = async function (parent, args, ctx, info) {
      const { user: { id: userId } } = ctx.session
      const redkey = `lk:${userId}`
      if (await redis.set(redkey, 'lock', 'NX', 'EX', 60) !== 'OK') throw new Error('Processing')

      try {
        return await resolve.apply(this, arguments)
      } catch (err) {
        throw err
      } finally {
        await redis.del(redkey)
      }
    }
  }
}

class CacheDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field) {
    const { key, ttl = 18 } = this.args
    const argKeys = field.args.map(row => row.name)
    const { resolve = defaultFieldResolver } = field

    if (argKeys.length) {
      const pre = `c:${key ?? field.name}:`

      field.resolve = async function (parent, args, ctx, info) {
        const suf = argKeys.map(k => args[k]).join(':')
        return cache(pre + suf, resolve.bind(this, ...arguments), ttl)
      }
    } else {
      const cacheKey = `c:${key ?? field.name}`

      field.resolve = async function (parent, args, ctx, info) {
        return cache(cacheKey, resolve.bind(this, ...arguments), ttl)
      }
    }
  }
}

class LogDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field) {
    const { resolve = defaultFieldResolver } = field
    field.resolve = async function (parent, args, ctx, info) {
      try {
        return await resolve.apply(this, arguments)
      } catch (err) {
        logger.error(err)
        throw err
      }
    }
  }
}

export default {
  log: LogDirective,
  auth: AuthDirective,
  cache: CacheDirective,
  ulock: UserLockDirective,
}
