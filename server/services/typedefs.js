
export const typeDefs = `

type User {
  id: Int
  name: String
  avatar: String
}

type Item {
  id: Int
  name: String
  icon: String
  gameType: Int
}

type Region {
  id: Int
  name: String
  icon: String
  regionPoint: Float
  isMyRegion: Boolean
}

type RegionMilestone {
  id: Int
  item: Item
  name: String
  minPoints: Int
  startAt: DateTime
}

type PersonMilestone {
  id: Int
  item: Item
  name: String
  startAt: DateTime
  requiredPoints: Int
}

type Partition {
  id: UInt16
  name: String
}

type State {
  id: Int
  point: UInt32
  once: UInt32
  daily: UInt32
  weekly: UInt32
  region: UInt32
  person: UInt32
  status: UInt32
  partitions: [Partition]
  ref: UInt8
  rev: UInt8
  referCode: String
  checkedWeek: UInt16
  rankNum: UInt32
}

type Role {
  id: Int
  code: Int
  name: String
}

type PlayerRole {
  mvp: Float
  roleId: Int
  hot: Int
}

type Player {
  id: Int
  icon: String
  name: String
  team: String
  regionId: Int
  weekMvp: Float
  isActive: Boolean
  roles: [PlayerRole]
}

type Week {
  id: Int
  name: String
  status: UInt32
  endTime: DateTime
}

type Predict {
  id: Int
  playerIds: [UInt32!]
}

type Mission {
  id: UInt8
  name: String
  point: UInt32
  type: UInt8
  action: UInt8
  meta: JSON
}

type History {
  id: Int
  type: Int
  point: Int
  content: Int
  createdAt: DateTime
}

type Reference {
  id: Int
  type: Int
  charName: String
  createdAt: DateTime
}

type Dashboard {
  id: Int
  charName: String
  point: Int
  lastTime: DateTime
}

type Vod {
  id: UInt8
  title: String
  url: String
  thumb: String
  priority: UInt32
  meta: JSON
}

type UserInfo {
  id: Int
  fullname: String
  phone: String
  address: String
}

type Transaction {
  charName: String
  itemName: String
  createdAt: DateTime
}

type WeekResult {
  id: Int
  name: String
  playerIds: [Int]
  mvps: [Float]
}

`

export default {
  // type resolvers
}
