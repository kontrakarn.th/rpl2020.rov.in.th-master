import { raw } from 'objection'

import { EVENTID } from '../services/loot'
import { Transaction, State } from '../db'
import { sendItem } from '../services/game-aov'

export default {
  name: 'sendItem',
  concurrency: 3,
  processer: async ({ data: { txnId } }) => {
    let payload
    let status = 'FAIL'
    const txn = await Transaction.query()
      .findById(txnId)
      .whereNot('status', 'SUCCESS')
      .select('id', 'userId', 'item', 'serial')
      .throwIfNotFound()

    try {
      const { tencentId } = await State.query()
        .findById(txn.userId)
        .select('tencentId')
        .throwIfNotFound()

      const { item: { packageId, activityId, moduleId, partitionId }, serial } = txn
      payload = await sendItem(tencentId, packageId, activityId, moduleId, EVENTID, serial, partitionId)

      if (payload.Ret !== 0 || payload.HasSendFailItem !== false) throw new Error('send item error')
      status = 'SUCCESS'

    } catch (err) {
      payload = payload || { error: err.message }
      throw err
    } finally {
      await txn.$query().update({ payload, status, attempts: raw('??+1', 'attempts') })
    }
  },
}
