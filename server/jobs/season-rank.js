import { raw, ref } from 'objection'

import { State } from '../db'

export default {
  name: 'seasonRank',
  repeat: {
    cron: '6 1/3 * * *',
  },
  options: {
    removeOnComplete: true,
  },
  processer: async () => {

    return State.transaction(async trx => {
      await trx.raw(`set @r:=0`)
      const numAffect = await State.query(trx).alias('S')
        .update({ rankNum: raw(`@r:=@r+1`) })
        .orderBy('point', 'desc')
        .orderBy('lastSeq', 'asc')
        .orderBy('id', 'asc')
      return numAffect
    })
  },
}
