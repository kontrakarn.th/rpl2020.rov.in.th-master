import fs from 'fs'
import path from 'path'
import Bull from 'bull'
import lodash from 'lodash'

import settings from '../settings'
import logger from '../kit/logger'

let qjob = {}
const jobNames = []

if (settings.qjob) {
  qjob = new Bull('qjob', {
    ...settings.qjob,
    redis: settings.redis,
    prefix: settings.redis.keyPrefix,
  })

  const cronJobs = new Map()
  for (const file of fs.readdirSync(path.join(__dirname))) {
    if (file.charAt(0) !== '.' && file !== 'index.js' && file.slice(-3) === '.js') {
      const job = require(`./${file}`).default
      jobNames.push(job.name)
      qjob.process(job.name, job.concurrency || 1, job.processer)
      if (job.repeat) {
        cronJobs.set(job.name, { repeat: job.repeat, ...job.options })
      }
    }
  }

  // Add cron job
  qjob.getRepeatableJobs().then(async jobs => {
    for (const { key } of jobs) {
      await qjob.removeRepeatableByKey(key)
      logger.trace(`remove old cron job ${key}`)
    }
    for (const name of cronJobs.keys()) {
      await qjob.add(name, undefined, cronJobs.get(name))
    }
  })

  if (process.env.NODE_ENV === 'development') {
    qjob.on('completed', job => logger.trace(`Job#${job.id} has been processed by ${process.pid}`))
  }
}

export const graphSchema = {
  typeDefs: `
  extend enum ModelEnum { Job }

  type JobCounts {
    waiting: Int
    active: Int
    completed: Int
    failed: Int
    delayed: Int
  }

  type Job {
    id: ID
    name: String
    data: JSON
    opts: JSON
    delay: Int
    returnvalue: JSON
    attemptsMade: Int
    failedReason: String
    stacktrace: JSON
    timestamp: DateTime
    finishedOn: DateTime
    processedOn: DateTime
  }

  input JobInput {
    #${JSON.stringify(jobNames)}
    name: String!
    data: JSON
    opts: JSON
  }

  type JobPage {
    results: [Job]
    total: Int
  }

  extend type Query {
    getJob(id: ID!): Job @auth(model: Job, operation: VIEW)
    listJob(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: JSON): JobPage @auth(model: Job, operation: VIEW)
  }

  extend type Mutation {
    createJob(data: JSON!): Job @auth(model: Job, operation: ADD)
    updateJob(ids: [ID!]!, data: JSON): [ID!]! @auth(model: Job, operation: EDIT)
    removeJob(ids: [ID!]!): [ID!]! @auth(model: Job, operation: DELETE)
  }
`,
  resolvers: {
    Query: {
      getJob: (parent, { id }) => qjob.getJob(id),
      listJob: async (parent, { page = 0, perPage = 10, sortField, sortOrder = 'DESC', filter: { status = 'completed' } }) => {
        const begin = page * perPage
        return {
          results: qjob.getJobs(status, begin, begin + perPage - 1, sortField === 'id' && sortOrder === 'ASC'),
          total: qjob[`get${lodash.capitalize(status)}Count`](),
        }
      },
    },
    Mutation: {
      createJob: async (parent, { data: value }, ctx, info) => {
        let ret
        if (Array.isArray(value)) {
          ret = await Promise.all(value.map(
            async (row) => {
              const { name, data, opts } = row
              const job = await qjob.add(name, data, opts)

              row.id = job.id
              return job
            }
          ))
        } else {
          const { name, data, opts } = value
          const job = await qjob.add(name, data, opts)

          value.id = job.id
          ret = [job]
        }

        logger.info('[ADMIN] %s create %s %o', ctx.admin.email, 'Job', value)
        return ret[0]
      },
      updateJob: (parent, { ids, data }, ctx, info) => Promise.all(
        ids.map(async id => {
          const job = await qjob.getJob(id)
          if (!job) throw new Error(`Job#${id} not found!`)

          switch (data._action) {
            case 'retry': {
              await job.retry()
              break
            }
            default: {
              throw new Error('invalid action')
            }
          }

          logger.info('[ADMIN] %s update %s #[%s] data %o', ctx.admin.email, 'Job', id, data)
          return id
        })
      ),
      removeJob: (parent, { ids }, ctx) => Promise.all(
        ids.map(async id => {
          const job = await qjob.getJob(id)
          if (!job) throw new Error(`Job#${id} not found!`)

          await job.remove()

          logger.info('[ADMIN] %s remove %s #[%s]', ctx.admin.email, 'Job', id)
          return id
        })
      ),
    },
  },
}

export default qjob
