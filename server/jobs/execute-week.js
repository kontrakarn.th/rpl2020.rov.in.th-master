import { raw, ref } from 'objection'

import { Week, Predict, State, History, Configuration } from '../db'

export default {
  name: 'executeWeek',
  processer: async ({ data: { weekId } }) => {
    const week = await Week.query()
      .findById(weekId)
      .whereNot('status', '&', 1)
      .where('endTime', '<', new Date())
      .throwIfNotFound()

    const { playerIds: results } = week
    if (results?.length !== 5) throw Error('InvalidPlayerIds')

    const correctMap = await Configuration.getCorrectMvpPointMap()

    const ret = await Week.transaction(async trx => {
      const numAffect = await week.$query(trx)
        .update({
          status: raw('??|?', 'status', 1)
        })
        .whereNot('status', '&', 1)

      if (!numAffect) return 0

      let count = 0
      for (const predict of await Predict.query(trx)
        .where('weekId', weekId)
        .whereNot('status', '&', 1)
        .orderBy('createdAt', 'asc')
      ) {
        count += 1
        const { userId, playerIds } = predict

        let num = 0
        let match = 0
        for (let i = 0; i < 5; i++) {
          if (playerIds[i] === results[i]) {
            num += 1
            match |= 1 << i
          }
        }
        const wPoint = correctMap[num]

        await History.query(trx)
          .insert({
            userId,
            type: 5,
            point: wPoint,
            content: weekId,
          })
        await predict.$query(trx)
          .update({ match, point: wPoint, status: raw('??|?', 'status', 1) })

        if (wPoint > 0) {
          await State.query(trx).findById(userId).increment('point', wPoint)
        }
      }

      return count
    })
    return ret
  },
}
