import { raw, ref } from 'objection'

import { State } from '../db'
import { cacheMission } from '../services/common'

export default {
  name: 'checkWeekwin',
  repeat: {
    cron: '16 0/2 * * *',
  },
  options: {
    removeOnComplete: true,
  },
  processer: async () => {
    const wMission = await cacheMission(0, 131)
    if (!wMission) return false

    const states = await State.query()
      .where('checkedWeek', '>', 0)
      .where('tencentId', '>', 0)
      .where('weekWin', '<', 0)
      .where(raw('JSON_LENGTH(??)', 'partitions'), '>', 0)
      .select('id', 'tencentId', 'partitions')

    for (const state of states) {
      try {
        await state.updateWeekWin()
      } catch { }
    }

    return states.length
  },
}
