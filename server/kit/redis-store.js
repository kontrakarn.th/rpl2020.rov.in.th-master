import session from 'express-session'

import redis from './redis-client'

class RedisStore extends session.Store {
  destroy (sid, callback) {
    redis.del(sid, callback)
  }

  async get (sid, callback) {
    try {
      const data = await redis.get(sid)
      callback(null, JSON.parse(data))
    } catch (error) {
      callback(error, null)
    }
  }

  set (sid, session, callback) {
    const { maxAge = 108e5 } = session.cookie
    redis.set(sid, JSON.stringify(session), 'PX', maxAge, callback)
  }
}

export default RedisStore
