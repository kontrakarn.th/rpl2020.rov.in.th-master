import redis from './redis-client'

const capitalize = (str) => str.charAt(0).toUpperCase() + str.slice(1)

class Cacher {
  constructor (model) {
    this.model = model
    this.ex = undefined
    this.prefix = 'cache'
    for (const method of ['findOne', 'findAll', 'findAndCount', 'findAndCountAll']) {
      this[method] = async (options, customKey) => {
        const redisKey = customKey ? `${this.prefix}:${this.model.name}:${customKey}` : `${this.prefix}:${this.model.name}:${method}${options ? JSON.stringify(options) : ''}`
        let res = await redis.get(redisKey)
        if (res === null) {
          res = await this.model[method](options)
          if (this.ex) redis.set(redisKey, JSON.stringify(res), 'EX', this.ex)
          else redis.set(redisKey, JSON.stringify(res))
        } else res = JSON.parse(res)
        return res
      }
      this[`force${capitalize(method)}`] = async (options, customKey) => {
        const redisKey = customKey ? `${this.prefix}:${this.model.name}:${customKey}` : `${this.prefix}:${this.model.name}:${method}${options ? JSON.stringify(options) : ''}`
        const res = await this.model[method](options)
        if (this.ex) redis.set(redisKey, JSON.stringify(res), 'EX', this.ex)
        else redis.set(redisKey, JSON.stringify(res))
        return res
      }
      this[`clear${capitalize(method)}`] = async (options, customKey) => {
        const redisKey = customKey ? `${this.prefix}:${this.model.name}:${customKey}` : `${this.prefix}:${this.model.name}:${method}${options ? JSON.stringify(options) : ''}`
        return redis.del(redisKey)
      }
    }
  }

  ttl (seconds) {
    this.ex = seconds
    return this
  }

  prefix (str) {
    this.prefix = str
    return this
  }
}

export default Cacher
