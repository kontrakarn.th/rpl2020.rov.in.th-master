import polka from 'polka'
import lodash from 'lodash'
import qs from 'querystring'
import config from 'grant/lib/config'

import settings from '../../settings'
import { authCallback } from './base'

import oauth1 from 'grant/lib/flow/oauth1'
import oauth2 from 'grant/lib/flow/oauth2'

const access1 = oauth1.access({})
const access2 = oauth2.access({})

const appConfig = config(settings?.grant)

const router = polka()
  .use('/', async (req, res, next) => {
    try {
      await next(!req.session && 'Grant: mount session middleware first')
      if (!res.finished && !res.headersSent) res.throw('Grant: missing session or misconfigured provider')
    } catch (err) {
      if (!res.finished) res.throw(err.message)
    }
  })
  .get('/:provider/callback', async (req, res) => {
    const session = req.session.grant
    const provider = config.provider(appConfig, { provider: req.params.provider, ...session })

    const { [provider.name]: { response_type: responseType, callbackHook } = {} } = settings.grant

    const response = data => {
      if (callbackHook) {
        return callbackHook instanceof Function ? callbackHook(req, res, data) : authCallback(callbackHook, req, res, data)
      } else if (!provider.callback) {
        res.send(data)
      } else if (!provider.transport || provider.transport === 'querystring') {
        res.redirect(`${provider.callback}?${qs.stringify(data)}`)
      } else if (provider.transport === 'session') {
        session.response = data
        res.redirect(provider.callback)
      }
    }

    if (responseType === 'token') return response(req.query)

    const { output } = await (provider.oauth === 2 ? access2 : access1)({ provider, input: req })

    return response(output)
  })
  .get('/:provider/logout', (req, res) => {
    const { [req.params.provider]: { logoutHook } = {} } = settings.grant
    return logoutHook instanceof Function && logoutHook(req, res)
  })
  .get('/:provider/:override?', async (req, res) => {
    req.session.grant = {
      provider: req.params.provider,
      override: req.params.override,
      dynamic: Object.keys(req.query).length ? req.query : undefined,
      referer: req.headers['referer'],
    }

    const session = req.session.grant
    const provider = config.provider(appConfig, session)

    const { [provider.name]: { response_type: responseType } = {} } = settings.grant
    if (responseType === 'token') {
      lodash.defaultsDeep(provider, { custom_params: { response_type: 'token' } })
    }

    if (provider.oauth === 1) {
      const { output: request } = await oauth1.request({ provider, input: req })
      session.request = request
      const { output } = await oauth1.authorize({ provider, input: req })
      return res.redirect(output)
    } else if (provider.oauth === 2) {
      session.state = provider.state
      session.nonce = provider.nonce
      session.code_verifier = provider.code_verifier
      const { output } = await oauth2.authorize({ provider, input: req })
      return res.redirect(output)
    }
  })

export default router
