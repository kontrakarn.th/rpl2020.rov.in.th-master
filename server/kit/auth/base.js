import { URL } from 'url'
import lodash from 'lodash'
import fetch from '../../helpers/fetch'

import settings from '../../settings'

export async function successHandler (req, res, { accessToken, ...data }) {
  let model

  const filter = lodash.pick(data, this.key)
  const affectedCount = await this.Model.query().where(filter).update(data)
  if (affectedCount <= 0) {
    if (this.mode === 'upsert') {
      model = await this.Model.query().insert(data)
    }
    else {
      return this.errorHandler('invalid account')
    }
  } else {
    model = await this.Model.query().where(filter).first()
  }

  req.session[this.model.toLowerCase()] = { accessToken, ...model }

  const redirectUrl = this.redirect instanceof Function ? this.redirect(req, res) : this.redirect
  res.redirect(redirectUrl)
}

export function errorHandler (req, res, error) {
  res.throw(error)
}

/**
 * Base callback
 * @param {Object} options - Callback options
 * @param {String} options.model - Sequelize model (default: User)
 * @param {String|Array} options.key - Model key to find (default: id)
 * @param {String} options.mode - upsert or update (default: 'update')
 * @param {String} options.infoUrl - Base URL to take user info
 * @param {Function} options.info2Model - Map info to model date (default: v => v)
 * @param {String|Function} options.redirect - Url to redirect (default: homepage)
 * @param {Function} options.successHandler - (default: defaultSuccessHandler)
 * @param {Function} options.errorHandler - (default: defaultErrorHandler | throw 400)
 * @param {Object} req - http request
 * @param {Object} res - http response
 * @param {Object} data
 * @returns {Promise<void>}
 */
export const authCallback = async (options, req, res, data) => {
  options = Object.assign({
    model: 'User',
    key: 'id',
    mode: 'update',
    infoUrl: null,
    info2Model: v => v,
    redirect: settings.homepage,
    successHandler: successHandler,
    errorHandler: errorHandler,
  }, options)

  options.Model = require('../../db')[options.model]
  options.key = [].concat(options.key)

  const error = options.errorHandler = options.errorHandler.bind(options, req, res)

  if (data?.error) return error(data.error)
  if (!data?.access_token) return error('missing access_token')

  const getUrl = new URL(options.infoUrl)
  getUrl.searchParams.set('access_token', data.access_token)
  const resp = await fetch(getUrl)

  if (resp.ok) {
    const json = await resp.json()
    const md = await options.info2Model(json, req)
    if (options.key.find(k => !md[k])) return error('missing key value')

    return options.successHandler.bind(options, req, res, Object.assign(md, { accessToken: data.access_token }))()
  } else return error('invalid access_token')
}
