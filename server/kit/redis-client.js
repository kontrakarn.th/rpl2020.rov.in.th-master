import Redis from 'ioredis'

import settings from '../settings'

const redis = new Redis(settings.redis)
console.log(settings.redis)
export default redis

export const getOrSet = async (key, asyncFunc, ttl) => {
  let value = await redis.get(key)
  if (!value) { // Value doesn't exist, set new one
    value = await asyncFunc()
    const status = (ttl !== undefined ? await redis.setex(key, ttl, value) : await redis.set(key, value))
    if (status === 'OK') {
      return value
    } else {
      throw Error('Unable to write to redis')
    }
  } else { // Value is existing, return its value
    return value
  }
}

export const cache = async (key, asyncFn, ttl) => {
  let data = await redis.get(key)
  if (!data) {
    const value = await asyncFn()
    data = JSON.stringify(value)
    const status = (ttl !== undefined ? await redis.setex(key, ttl, data) : await redis.set(key, data))
    if (status === 'OK') {
      return value
    } else {
      throw Error('Unable to write to redis')
    }
  } else {
    return JSON.parse(data)
  }
}

export const getCache = async (key) => {
  const data = await redis.get(key)
  return data ? JSON.parse(data) : null
}

export const setCache = (key, value, ttl) => {
  const data = JSON.stringify(value)
  return ttl !== undefined ? redis.setex(key, ttl, data) : redis.set(key, data)
}

export const deleteCache = (key) => redis.del(key)
