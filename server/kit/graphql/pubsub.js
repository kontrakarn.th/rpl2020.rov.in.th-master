import { Readable } from 'stream'
import { Observable, merge, using } from 'rxjs'
import { share, map, filter, pluck } from 'rxjs/operators'

import redis from '../redis-client'

class ObservaleReadable extends Readable {
  constructor (observable) {
    super({
      objectMode: true,
    })

    this.subscription = observable.subscribe(
      value => this.push(value),
      error => this.destroy(error),
      value => this.push(null),
    )
  }

  _read () { }

  _destroy (error) {
    super.destroy(error)
    if (this.subscription) {
      this.subscription.unsubscribe()
      delete this.subscription
    }
  }
}

Observable.prototype[Symbol.asyncIterator] = function () {
  const readable = new ObservaleReadable(this)
  return readable[Symbol.asyncIterator]()
}

class ObservablePubsub {
  constructor ({ publisher }) {
    this.rdPub = publisher // redis publisher
    this.channelMap = new Map()

    const { options: { keyPrefix } } = publisher
    this.rdChanNameMapper = keyPrefix ? name => keyPrefix + name : name => name

    this.source = new Observable(subscriber => {
      // create the redis subscriber
      this.rdSub = this.rdPub.duplicate()
      if (this.channelMap.size) this.rdSub.subscribe([...this.channelMap.keys()].map(this.rdChanNameMapper))

      this.rdSub.on('message', (channel, message) => {
        subscriber.next({ channel, message })
      })
      return () => {
        // destroy the redis subscriber
        this.rdSub.quit()
        delete this.rdSub
      }
    }).pipe(share())
  }

  getChannel (name) {
    if (this.channelMap.has(name)) return this.channelMap.get(name)
    const rdChan = this.rdChanNameMapper(name)

    const channel = using(() => {
      setImmediate(() => {
        // subscribe the channel
        if (this.rdSub) this.rdSub.subscribe(rdChan)
      })
      return {
        unsubscribe: () => {
          // unsubscribe the channel
          this.channelMap.delete(name)
          if (this.rdSub) this.rdSub.unsubscribe(rdChan)
        },
      }
    }, () => this.source).pipe(
      filter(value => value.channel === rdChan),
      pluck('message'),
      map(JSON.parse),
      share(),
    )

    this.channelMap.set(name, channel)
    return channel
  }

  asyncIterator (channels) {
    return Array.isArray(channels)
      ? merge(...channels.map(name => this.getChannel(name)))
      : this.getChannel(channels)
  }

  publish (channel, payload) {
    return this.rdPub.publish(this.rdChanNameMapper(channel), JSON.stringify(payload))
  }
}

export default new ObservablePubsub({ publisher: redis })
