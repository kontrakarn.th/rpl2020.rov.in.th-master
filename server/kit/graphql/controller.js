import typeis from 'type-is'
import coBody from 'co-body'
import { processRequest } from 'graphql-upload'
import { parse, validate, execute, specifiedRules, GraphQLError } from 'graphql'

import settings from '../../settings'

import logger from '../logger'
import defaultSchema from '../../graphql/schema'
import adminSchema from '../../graphql/admin-schema'

const runQuery = ({ schema, root }) => {
  if (schema instanceof Function) schema = schema()
  if (schema instanceof Promise) schema.then(res => { schema = res }).catch(err => logger.error(err))

  return async (req, res) => {
    try {
      const { query, variables, operationName } = req.body || await coBody.json(req)

      const documentAST = parse(query)

      const validationErrors = validate(schema, documentAST, specifiedRules)
      if (validationErrors.length > 0) throw validationErrors

      const exeResult = await execute(
        schema,
        documentAST,
        root, // rootValue
        req, // contextValue,
        variables, // variableValues
        operationName,
      )

      return res.send(exeResult)
    } catch (err) {
      // Return 400: Bad Request if query couldn't be executed.
      return res.throw({ errors: Array.isArray(err) ? err : err instanceof GraphQLError ? [err] : [{ message: err.message }] })
    }
  }
}

const playground = options => (req, res) => res.send(`
<!DOCTYPE html>
<html>
<head>
  <meta charset=utf-8/>
  <meta name="viewport" content="user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, minimal-ui">
  <title>GraphQL Playground</title>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/graphql-playground-react/build/static/css/index.css"/>
  <link rel="shortcut icon" href="//cdn.jsdelivr.net/npm/graphql-playground-react/build/favicon.png"/>
  <script src="//cdn.jsdelivr.net/npm/graphql-playground-react/build/static/js/middleware.js"></script>
</head>
<body>
  <div id="root">
    <style>
      body { background-color: rgb(23, 42, 58); font-family: Open Sans, sans-serif; height: 90vh; }
      #root { height: 100%; width: 100%; display: flex; align-items: center; justify-content: center; }
      .loading { font-size: 32px; font-weight: 200; color: rgba(255, 255, 255, .6); margin-left: 20px; }
      img { width: 78px; height: 78px; } .title { font-weight: 400; }
    </style>
    <img src='//cdn.jsdelivr.net/npm/graphql-playground-react/build/logo.png'>
    <div class="loading"> Loading <span class="title">GraphQL Playground</span></div>
  </div>
  <script>window.addEventListener('load', function (event) {
      GraphQLPlayground.init(document.getElementById('root'), ${JSON.stringify(options)})
    })</script>
</body>
</html>`, 200, { 'content-type': 'text/html' })

const { graphql: { upload, subscriptions } } = settings

const graphqUpload = options => (req, res, next) => {
  if (!typeis(req, ['multipart/form-data'])) return next()
  const finished = new Promise(resolve => req.once('end', resolve))
  const { send } = res

  res.send = (...args) => finished.then(() => send.apply(res, args))

  processRequest(req, res, options)
    .then(body => {
      req.body = body
      next()
    })
    .catch(error => {
      if (error.status && error.expose) res.writeHead(error.status)
      next(error)
    })
}

export const defaultGraphqlGet = playground({
  subscriptionEndpoint: subscriptions && typeof subscriptions === 'string' ? subscriptions : undefined,
  settings: {
    'request.credentials': 'include',
  }
})

export const defaultGraphqlPost = [runQuery({ schema: defaultSchema })]
if (upload) defaultGraphqlPost.splice(0, 0, graphqUpload(upload))

export const adminGraphqlGet = playground({
  settings: {
    'request.credentials': 'include',
  }
})

const adminAuth = async (req, res, next) => {
  if (!req?.session?.admin) return res.throw('Unauthorized', 401)
  next()
}

export const adminGraphqlPost = [adminAuth, graphqUpload(), runQuery({ schema: adminSchema })]
