import logger from '../../kit/logger'

export const log = (options = {}) =>
  async (next, root, args, ctx) => {
    logger.info('Graphql API request', ctx.request.body)
    const startTime = new Date()
    try {
      const resp = await next()
      return resp
    } catch (ex) {
      logger.warn(ex.stack || `${ex}`)
      throw ex
    } finally {
      logger.info(`Graphql API completed in ${new Date() - startTime} ms.`)
    }
  }

export default {
  log,
}
