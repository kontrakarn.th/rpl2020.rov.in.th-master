import { execute, subscribe } from 'graphql'
import { SubscriptionServer } from 'subscriptions-transport-ws'

import settings from '../../settings'
import defaultSchema from '../../graphql/schema'

const { graphql: { subscriptions, defaultEndpoint } } = settings

export const createSubscriptionServer = (httpServer) => {
  return subscriptions && new SubscriptionServer({ execute, subscribe, schema: defaultSchema }, {
    server: httpServer,
    path: typeof subscriptions === 'string' ? subscriptions : defaultEndpoint
  })
}
