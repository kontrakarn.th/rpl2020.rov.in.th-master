import { GraphQLError } from 'graphql/error'
import { GraphQLScalarType } from 'graphql/type/definition'

const oneBinary = (numbit) => parseInt(Array(numbit).fill(1).join(''), 2)
const intMin = (numbit, unsigned) => unsigned ? 0 : -oneBinary(numbit - 1) - 1
const intMax = (numbit, unsigned) => unsigned ? oneBinary(numbit) : oneBinary(numbit - 1)

const intName = (numbit, unsigned) => `${unsigned ? 'U' : ''}Int${numbit}`
const intSerialize = (numbit, unsigned) => {
  const MIN = intMin(numbit, unsigned)
  const MAX = intMax(numbit, unsigned)

  return (value) => {
    if (typeof value === 'boolean') {
      return value ? 1 : 0
    }

    let num = value
    if (typeof value === 'string' && value.length) {
      num = Number(value)
    }

    if (!Number.isInteger(num)) {
      throw new GraphQLError(`Int cannot represent non-integer`)
    }
    if (num > MAX || num < MIN) {
      throw new GraphQLError(`Int cannot represent non ${numbit}-bit ${unsigned ? 'unsigned' : 'signed'} integer`)
    }
    return num
  }
}

const GraphQLInt = (numbit, unsigned) => {
  const name = intName(numbit, unsigned)
  const serialize = intSerialize(numbit, unsigned)

  return new GraphQLScalarType({
    name,
    description: `The \`${name}\` scalar`,
    serialize,
    parseValue: serialize,
    parseLiteral: (ast) => serialize(ast.value),
  })
}

export const GraphQLInt8 = GraphQLInt(8, false)
export const GraphQLInt16 = GraphQLInt(16, false)
export const GraphQLInt24 = GraphQLInt(24, false)
export const GraphQLInt32 = GraphQLInt(32, false)
export const GraphQLInt64 = GraphQLInt(64, false)

export const GraphQLUInt8 = GraphQLInt(8, true)
export const GraphQLUInt16 = GraphQLInt(16, true)
export const GraphQLUInt24 = GraphQLInt(24, true)
export const GraphQLUInt32 = GraphQLInt(32, true)
export const GraphQLUInt64 = GraphQLInt(64, true)
