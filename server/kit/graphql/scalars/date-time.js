import { GraphQLScalarType } from 'graphql/type/definition'

export default new GraphQLScalarType({
  name: 'DateTime',
  description: 'DateTime scalar',
  parseValue: value => value, // value from the client
  serialize: value => value, // value sent to the client
  parseLiteral: value => value,
})
