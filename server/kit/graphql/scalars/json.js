import { Kind } from 'graphql/language'
import { GraphQLScalarType } from 'graphql/type/definition'

const parseLiteralJSON = (ast) => {
  switch (ast.kind) {
    case Kind.STRING:
    case Kind.BOOLEAN:
      return ast.value
    case Kind.INT:
      return parseInt(ast.value)
    case Kind.FLOAT:
      return parseFloat(ast.value)
    case Kind.OBJECT: {
      const value = Object.create(null)
      for (const field of ast.fields) {
        value[field.name.value] = parseLiteralJSON(field.value)
      }
      return value
    }
    case Kind.LIST:
      return ast.values.map(parseLiteralJSON)
    default:
      return null
  }
}

export default new GraphQLScalarType({
  name: 'JSON',
  description: `
The \`JSON\` scalar type represents JSON values as specified by
[ECMA-404](http://www.ecma-international.org/
publications/files/ECMA-ST/ECMA-404.pdf).`,
  serialize: value => value,
  parseValue: value => value,
  parseLiteral: parseLiteralJSON,
})
