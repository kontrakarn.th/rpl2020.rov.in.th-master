import pino from 'pino'

const options = {
  level: process.env.LOG_LEVEL || 'info',
  prettyPrint: {
    translateTime: `SYS:yyyy-mm-dd'T'HH:MM:ss.l o`,
  },
}

if (process.env.NODE_ENV === 'development') {
  options.level = 'trace'
} else {
  options.prettyPrint.colorize = false
}

export default pino(options)
