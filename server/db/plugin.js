import { Model } from 'objection'

import master from './master'
import model2gql from '../helpers/objection-graphql-helper'

Model.knex(master)

Object.defineProperties(Model, {
  createdAt: {
    value: 'createdAt',
    writable: true,
  },
  updatedAt: {
    value: 'updatedAt',
    writable: true,
  },
  graphql: {
    value: model2gql,
  },
  tableName: {
    get () {
      Object.defineProperty(this, 'tableName', { value: this.name })
      return this.name
    },
    set (value) {
      Object.defineProperty(this, 'tableName', { value })
    },
  },
  useLimitInFirst: {
    value: true,
    writable: true,
  },
})

Object.defineProperties(Model.prototype, {
  $createdAt: {
    value () {
      if (this.constructor.createdAt && !this[this.constructor.createdAt]) {
        this[this.constructor.createdAt] = new Date()
      }
    },
  },
  $updatedAt: {
    value () {
      if (this.constructor.updatedAt && Object.keys(this).length) {
        this[this.constructor.updatedAt] = new Date()
      }
    },
  },
  $beforeInsert: {
    value () {
      this.$createdAt()
      this.$updatedAt()
    },
  },
  $beforeUpdate: {
    value () {
      this.$updatedAt()
    },
  },
})
