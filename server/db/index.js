import fs from 'fs'
import path from 'path'

import './plugin'

const db = {}

for (const file of fs.readdirSync(path.join(__dirname, 'models'))) {
  if (file.charAt(0) !== '.' && file.slice(-3) === '.js') {
    const model = require(`./models/${file}`).default
    db[model.name] = model
  }
}

module.exports = db
