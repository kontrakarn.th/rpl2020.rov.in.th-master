import knex from 'knex'
import logger from '../kit/logger'

import settings from '../settings'

const master = knex(settings.database)

if (process.env.NODE_ENV === 'development') {
  const SqlString = require('sqlstring')
  master.on('query', data => {
    logger.trace(`[%s] %s`, data.__knexUid, SqlString.format(data.sql, data.bindings))
  })
}

export default master
