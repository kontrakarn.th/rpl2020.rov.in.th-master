
exports.up = function (knex) {
  return knex.schema.table('PlayerRole', function (table) {
      table.integer('hot').unsigned().notNullable().defaultTo(0).after('mvp')
    })
}

exports.down = function (knex) {
  return knex.schema.table('PlayerRole', function (table) {
      table.dropColumn('hot')
    })
}
