
exports.up = function (knex) {
  return knex.raw(`ALTER SCHEMA ${knex.client.database()} DEFAULT CHARACTER SET utf8mb4`)
}

exports.down = function (knex) {
  return knex.raw(`ALTER SCHEMA ${knex.client.database()} DEFAULT CHARACTER SET utf8`)
}
