
exports.up = function (knex) {
  return knex.schema.table('Player', function (table) {
      table.float('weekMvp').notNullable().defaultTo(0).after('team')
    })
}

exports.down = function (knex) {
  return knex.schema.table('Player', function (table) {
      table.dropColumn('weekMvp')
    })
}
