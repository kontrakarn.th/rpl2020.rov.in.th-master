
exports.up = function (knex) {
  return knex.schema.table('State', function (table) {
      table.integer('rankNum').unsigned().notNullable().defaultTo(999999).after('lastTime')
    })
}

exports.down = function (knex) {
  return knex.schema.table('State', function (table) {
      table.dropColumn('rankNum')
    })
}
