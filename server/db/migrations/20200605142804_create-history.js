
exports.up = function (knex) {
  return knex.schema.createTable('History', function (table) {
    table.increments()
    table.integer('userId').unsigned().notNullable().references('id').inTable('User')
    table.tinyint('type').unsigned().notNullable()
    table.integer('content').unsigned().notNullable()
    table.integer('point').unsigned().notNullable().defaultTo(0)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('History')
}
