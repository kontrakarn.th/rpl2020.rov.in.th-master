
exports.up = function (knex) {
  return knex.schema.createTable('Predict', function (table) {
    table.integer('weekId').unsigned().notNullable().references('id').inTable('Week')
    table.integer('userId').unsigned().notNullable().references('id').inTable('User')
    table.json('playerIds').notNullable()
    table.tinyint('match').unsigned().notNullable().defaultTo(0)
    table.integer('point').unsigned().notNullable().defaultTo(0)
    table.tinyint('status').unsigned().notNullable().defaultTo(0)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())

    table.primary(['weekId', 'userId'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Predict')
}
