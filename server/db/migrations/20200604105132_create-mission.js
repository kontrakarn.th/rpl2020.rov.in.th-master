
exports.up = async function (knex) {
  await knex.schema.createTable('Mission', function (table) {
    table.tinyint('id').unsigned().notNullable().primary()
    table.string('name', 128).notNullable()
    table.tinyint('action').unsigned().notNullable().defaultTo(0)
    table.integer('point').unsigned().notNullable()
    table.json('meta')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('Mission').insert([
    { id: 0x01, name: 'Share Microsite', point: 7, action: 0 },
    { id: 0x02, name: 'Share MVP team', point: 30, action: 1 },
    { id: 0x11, name: 'Play 1 game', point: 2, action: 128 },
    { id: 0x12, name: 'Click to link', point: 2, action: 129 },
    { id: 0x13, name: 'Get link clicked ', point: 2, action: 130 },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Mission')
}
