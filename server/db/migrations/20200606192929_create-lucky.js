
exports.up = function (knex) {
  return knex.schema.createTable('Lucky', function (table) {
    table.increments()
    table.string('name', 64).notNullable()
    table.integer('itemId').unsigned().notNullable()
    table.double('chance').notNullable()
    table.integer('current').unsigned().notNullable().defaultTo(0)
    table.integer('limit').unsigned().notNullable()
    table.boolean('active').notNullable()
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Lucky')
}
