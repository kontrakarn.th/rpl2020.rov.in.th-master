
exports.up = function (knex) {
  return knex.schema.table('State', function (table) {
    table.integer('weekWin').notNullable().defaultTo(-1).alter()
  })
}

exports.down = function (knex) {
  return knex.schema.table('State', function (table) {
    table.integer('weekWin').unsigned().notNullable().defaultTo(0).alter()
  })
}
