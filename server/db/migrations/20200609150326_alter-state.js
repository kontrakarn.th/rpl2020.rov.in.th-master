
exports.up = function (knex) {
  return knex.schema
    .table('State', function (table) {
      table.integer('weekWin').unsigned().notNullable().defaultTo(0).after('checkedWeek')
      table.integer('lastSeq').unsigned().notNullable().defaultTo(0).after('nonce')
      table.datetime('lastTime').notNullable().defaultTo(knex.fn.now()).after('lastSeq')

      table.dropIndex('point')
      table.index([knex.raw('?? DESC', ['point']), 'lastSeq'], 'point_seq_idx')
    })
}

exports.down = function (knex) {
  return knex.schema
    .table('State', function (table) {
      table.dropColumns('weekWin', 'lastSeq', 'lastTime')
      table.dropIndex(['point', 'lastSeq'], 'point_seq_idx')
      table.index('point')
    })
}
