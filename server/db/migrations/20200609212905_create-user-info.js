
exports.up = function (knex) {
  return knex.schema.createTable('UserInfo', function (table) {
    table.integer('id').unsigned().primary().references('id').inTable('User')
    table.string('fullname', 128).notNullable()
    table.string('phone', 32).notNullable()
    table.string('address').notNullable()
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('UserInfo')
}
