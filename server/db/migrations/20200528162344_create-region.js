
exports.up = async function (knex) {
  await knex.schema.createTable('Region', function (table) {
    table.tinyint('id').unsigned().primary()
    table.string('name', 64).notNullable()
    table.string('icon').notNullable()
    table.string('url').notNullable().defaultTo('https://event.apl.lienquan.garena.vn')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('Region').insert([
    { id: 1, name: 'AOG', icon: '/images/aog.png', },
    { id: 2, name: 'RPL', icon: '/images/rpl.png', },
    { id: 3, name: 'GCS', icon: '/images/gcs.png', },
    { id: 4, name: 'ASL', icon: '/images/asl.png', },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Region')
}
