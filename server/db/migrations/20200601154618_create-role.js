
exports.up = async function (knex) {
  await knex.schema.createTable('Role', function (table) {
    table.tinyint('id').unsigned().primary()
    table.string('name', 16).notNullable()
    table.smallint('code').unsigned().notNullable().defaultTo(800)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('Role').insert([
    { id: 0, name: 'DSL', code: 0xe800 },
    { id: 1, name: 'Jungle', code: 0xe801 },
    { id: 2, name: 'Middle', code: 0xe802 },
    { id: 3, name: 'ADL', code: 0xe803 },
    { id: 4, name: 'Support', code: 0xe804 },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Role')
}
