
exports.up = async function (knex) {
  await knex.schema.createTable('PlayerRole', function (table) {
    table.integer('playerId').unsigned().notNullable().references('id').inTable('Player').onDelete('CASCADE').onUpdate('CASCADE')
    table.tinyint('roleId').unsigned().notNullable().references('id').inTable('Role')
    table.float('mvp').notNullable().defaultTo(0)
    table.primary(['playerId', 'roleId'])
  })

  const roles = []
  const chance = require('chance')()

  const players = await knex('Player').select('id')

  for (const { id } of players) {
    for (let j = 0; j < 5; j++) {
      roles.push({
        playerId: id,
        roleId: j,
        mvp: chance.floating({ min: 0, max: 10, fixed: 1 }),
      })
    }
  }

  return knex('PlayerRole').insert(roles)
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('PlayerRole')
}
