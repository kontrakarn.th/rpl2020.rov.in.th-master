
exports.up = async function (knex) {
  await knex.schema.createTable('Vod', function (table) {
    table.tinyint('id').unsigned().notNullable().primary()
    table.string('title').notNullable()
    table.string('url').notNullable()
    table.string('thumb').notNullable()
    table.integer('priority').unsigned().notNullable().defaultTo(0)
    table.json('meta')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Vod')
}
