
exports.up = function (knex) {
  return knex.schema.table('RegionMilestone', function (table) {
    table.integer('minPoints').unsigned().notNullable().defaultTo(100).after('requiredPoints')
  })
};

exports.down = function (knex) {
  return knex.schema.table('RegionMilestone', function (table) {
    table.dropColumn('minPoints')
  })
};
