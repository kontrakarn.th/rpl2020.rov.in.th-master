
exports.up = function (knex) {
  return knex.schema.createTable('Refer', function (table) {
    table.increments()
    table.integer('srcId').unsigned().notNullable().references('id').inTable('User')
    table.integer('dstId').unsigned().notNullable().references('id').inTable('User')
    table.date('date').notNullable()
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())

    table.unique(['srcId', 'dstId', 'date'])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Refer')
}
