
exports.up = function (knex) {
  return knex.schema.createTable('Transaction', function (table) {
    table.increments()
    table.integer('userId').unsigned().notNullable().references('id').inTable('User')
    table.json('item')
    table.enum('status', ['PENDING', 'SUCCESS', 'FAIL']).notNullable().defaultTo('PENDING')
    table.integer('attempts').unsigned().notNullable().defaultTo(0)
    table.string('serial', 64)
    table.json('payload')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Transaction')
}
