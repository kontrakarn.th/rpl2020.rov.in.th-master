
exports.up = async function (knex) {
  await knex.schema.createTable('RegionMilestone', function (table) {
    table.tinyint('id').unsigned().primary()
    table.string('name', 64).notNullable()
    table.integer('requiredPoints').unsigned().notNullable()
    table.tinyint('itemId').unsigned().notNullable()
    table.datetime('startAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('RegionMilestone').insert([
    { id: 1, name: 'First', itemId: 1, requiredPoints: 9e7 },
    { id: 2, name: 'Second', itemId: 2, requiredPoints: 12e7 },
    { id: 3, name: 'Third', itemId: 3, requiredPoints: 16e7 },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('RegionMilestone')
}
