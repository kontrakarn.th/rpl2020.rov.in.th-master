
exports.up = function (knex) {
  return knex.schema.createTable('Week', function (table) {
    table.increments()
    table.string('name', 32).notNullable()
    table.datetime('startTime').notNullable().index()
    table.datetime('endTime').notNullable()
    table.tinyint('status').unsigned().notNullable().defaultTo(0)
    table.json('playerIds')
    table.json('livetime')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Week')
}
