
exports.up = function (knex) {
  return knex.schema.createTable('State', function (table) {
    table.integer('id').unsigned().primary().references('id').inTable('User')
    table.bigint('tencentId').unsigned().notNullable().defaultTo(0)
    table.json('partitions')
    table.integer('point').unsigned().notNullable().defaultTo(0).index()
    table.integer('once').unsigned().notNullable().defaultTo(0)
    table.integer('daily').unsigned().notNullable().defaultTo(0)
    table.integer('weekly').unsigned().notNullable().defaultTo(0)
    table.integer('region').unsigned().notNullable().defaultTo(0)
    table.integer('person').unsigned().notNullable().defaultTo(0)
    table.integer('status').unsigned().notNullable().defaultTo(0)
    table.date('checkedDate').notNullable().defaultTo('2000-01-01')
    table.tinyint('ref').unsigned().notNullable().defaultTo(0)
    table.tinyint('rev').unsigned().notNullable().defaultTo(0)
    table.integer('checkedWeek').unsigned().notNullable().defaultTo(0)
    table.integer('nonce').unsigned().notNullable()
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('State')
}
