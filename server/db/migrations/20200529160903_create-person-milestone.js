
exports.up = async function (knex) {
  await knex.schema.createTable('PersonMilestone', function (table) {
    table.tinyint('id').unsigned().primary()
    table.string('name', 64).notNullable()
    table.integer('requiredPoints').unsigned().notNullable()
    table.tinyint('itemId').unsigned().notNullable()
    table.datetime('startAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('PersonMilestone').insert([
    { id: 1, name: 'First', itemId: 4, requiredPoints: 30 },
    { id: 2, name: 'Second', itemId: 4, requiredPoints: 90 },
    { id: 3, name: 'Third', itemId: 4, requiredPoints: 250 },
    { id: 4, name: 'Fourth', itemId: 4, requiredPoints: 350 },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('PersonMilestone')
}
