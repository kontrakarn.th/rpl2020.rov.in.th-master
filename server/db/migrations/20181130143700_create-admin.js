
exports.up = async function (knex) {
  await knex.schema.createTable('Admin', function (table) {
    table.increments()
    table.string('email', 127).notNullable().unique('emailUniq')
    table.enum('role', ['GUEST', 'STAFF', 'ADMIN']).notNullable().defaultTo('GUEST')
    table.string('name', 127)
    table.string('avatar')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  await knex.schema.createTable('AdminPermission', function (table) {
    table.increments()
    table.integer('adminId').unsigned().notNullable().references('id').inTable('Admin').onDelete('CASCADE').withKeyName('adPermFk')
    table.string('model', 32).notNullable()
    table.tinyint('allow', 2).unsigned().notNullable().defaultTo(0)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
    table.unique(['adminId', 'model'], 'adModelUniq')
  })

  await knex('Admin').insert([
    { email: 'tienquang.nguyen@ved.com.vn', role: 'ADMIN' },
    { email: 'thequang.pham@ved.com.vn', role: 'ADMIN' },
    { email: 'caocuong.tran@ved.com.vn' },
  ])
}

exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('AdminPermission')
    .dropTableIfExists('Admin')
}
