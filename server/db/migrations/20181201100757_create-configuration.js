
exports.up = function(knex) {
  return knex.schema.createTable('Configuration', function (table) {
    table.increments()
    table.string('key', 64).notNullable().unique('keyUniq')
    table.json('value').notNullable()
    table.enum('type', ['Text', 'Number', 'Boolean', 'Date', 'DateTime', 'Duration', 'Json']).notNullable().defaultTo('Text')
    table.json('schema')
    table.string('description')
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('Configuration')
}
