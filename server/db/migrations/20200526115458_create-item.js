
exports.up = async function (knex) {
  await knex.schema.createTable('Item', function (table) {
    table.increments()
    table.string('name', 64).notNullable()
    table.string('icon').notNullable()
    table.tinyint('gameType').unsigned().notNullable().defaultTo(0)
    table.integer('activityId').unsigned().notNullable().defaultTo(0)
    table.integer('moduleId').unsigned().notNullable().defaultTo(0)
    table.integer('packageId').unsigned().notNullable().defaultTo(0)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  return knex('Item').insert([
    { id: 1, name: 'Gold chest', icon: '/images/chest1.png', },
    { id: 2, name: 'Platine chest', icon: '/images/chest2.png', },
    { id: 3, name: 'Diamond chest', icon: '/images/chest3.png', },
    { id: 4, name: 'Item', icon: '/images/item.png', },
  ])
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Item')
}
