
exports.up = function (knex) {
  return knex.schema
    .table('RegionMilestone', function (table) {
      table.integer('itemId').unsigned().notNullable().alter()
    })
    .table('PersonMilestone', function (table) {
      table.integer('itemId').unsigned().notNullable().alter()
    })
};

exports.down = function (knex) {
  return knex.schema
    .table('RegionMilestone', function (table) {
      table.tinyint('itemId').unsigned().notNullable().alter()
    })
    .table('PersonMilestone', function (table) {
      table.tinyint('itemId').unsigned().notNullable().alter()
    })
};
