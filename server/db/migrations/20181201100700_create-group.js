
exports.up = function (knex) {
  return knex.schema
    .createTable('Group', function (table) {
      table.increments()
      table.string('name', 32).notNullable()
      table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
      table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
    })
    .createTable('GroupPermission', function (table) {
      table.increments()
      table.integer('groupId').unsigned().notNullable().references('id').inTable('Group').onDelete('CASCADE').withKeyName('grPermFk')
      table.string('model', 32).notNullable()
      table.tinyint('allow', 2).unsigned().notNullable().defaultTo(0)
      table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
      table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
      table.unique(['groupId', 'model'], 'grModelUniq')
    })
    .createTable('AdminGroup', function (table) {
      table.increments()
      table.integer('adminId').unsigned().notNullable().references('id').inTable('Admin').onDelete('CASCADE').withKeyName('agAdminFk')
      table.integer('groupId').unsigned().notNullable().references('id').inTable('Group').onDelete('CASCADE').withKeyName('agGroupFk')
      table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
      table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
      table.unique(['adminId', 'groupId'], 'adGrUniq')
    })
}

exports.down = function (knex) {
  return knex.schema
    .dropTableIfExists('AdminGroup')
    .dropTableIfExists('GroupPermission')
    .dropTableIfExists('Group')
}
