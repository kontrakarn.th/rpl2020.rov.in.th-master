
exports.up = function (knex) {
  return knex.schema
    .table('Player', function (table) {
      table.dropColumn('weekMvp')
    })
    .table('Week', function (table) {
      table.json('mvps').after('playerIds')
    })
}

exports.down = function (knex) {
  return knex.schema
    .table('Player', function (table) {
      table.float('weekMvp').notNullable().defaultTo(0).after('team')
    })
    .table('Week', function (table) {
      table.dropColumn('mvps')
    })
}
