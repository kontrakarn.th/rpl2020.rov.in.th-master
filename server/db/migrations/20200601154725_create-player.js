
exports.up = async function (knex) {
  await knex.schema.createTable('Player', function (table) {
    table.increments()
    table.string('name', 16).notNullable()
    table.string('icon').notNullable()
    table.tinyint('regionId').unsigned().notNullable()
    table.string('team', 16).notNullable()
    table.boolean('isActive').notNullable().defaultTo(true)
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })

  const players = []
  const chance = require('chance')()

  for (let i = 1; i <= 120; i++) {
    players.push({
      id: i,
      regionId: chance.integer({ min: 1, max: 4 }),
      name: chance.last(),
      icon: '/images/player.png',
      team: chance.first(),
    })
  }

  return knex('Player').insert(players)
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('Player')
}
