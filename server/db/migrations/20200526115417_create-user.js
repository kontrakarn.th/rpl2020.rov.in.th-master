
exports.up = function (knex) {
  return knex.schema.createTable('User', function (table) {
    table.increments()
    table.bigint('uid').unsigned().notNullable()
    table.string('openId', 32).notNullable().unique()
    table.string('name', 127)
    table.tinyint('platform').unsigned().notNullable()
    table.string('avatar')
    table.integer('ip').unsigned()
    table.datetime('createdAt').notNullable().defaultTo(knex.fn.now())
    table.datetime('updatedAt').notNullable().defaultTo(knex.fn.now())
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('User')
}
