import { Model } from 'objection'

class Predict extends Model {
  static idColumn = ['weekId', 'userId']
  static jsonAttributes = ['playerIds']

  static jsonSchema = {
    type: 'object',
    properties: {
      playerIds: {
        type: 'array',
        minItems: 5,
        maxItems: 5,
        items: { type: 'integer', mininum: 1 },
      },
    }
  }
}

export default Predict
