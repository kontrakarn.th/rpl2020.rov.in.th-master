import { Model, raw } from 'objection'

import State from './state'

class Refer extends Model {
  
  static relationMappings = {
    src: {
      relation: Model.HasOneRelation,
      modelClass: State,
      join: {
        from: `${this.tableName}.srcId`,
        to: `${State.tableName}.id`,
      },
      modify: builder => {
        builder.select('id', raw('??->>?', 'partitions', '$[0].name').as('charName'))
      },
    },
    dst: {
      relation: Model.HasOneRelation,
      modelClass: State,
      join: {
        from: `${this.tableName}.dstId`,
        to: `${State.tableName}.id`,
      },
      modify: builder => {
        builder.select('id', raw('??->>?', 'partitions', '$[0].name').as('charName'))
      },
    },
  }

  static graphqlOptions = {
    fields: {
      src: false,
      dst: false,
    },
  }
}

export default Refer
