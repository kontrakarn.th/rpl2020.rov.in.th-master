import { Model } from 'objection'

class GroupPermission extends Model {

  static graphqlOptions = {
    fields: {
      groupId: false,
    },
  }
}

export default GroupPermission
