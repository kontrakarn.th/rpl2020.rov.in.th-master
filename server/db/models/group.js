import { Model } from 'objection'

import GroupPermission from './group-permission'

class Group extends Model {
  static relationMappings = {
    permissions: {
      relation: Model.HasManyRelation,
      modelClass: GroupPermission,
      join: {
        from: `${Group.tableName}.id`,
        to: `${GroupPermission.tableName}.groupId`,
      },
    },
  }
}

export default Group
