import { Model } from 'objection'

class Week extends Model {
  static jsonAttributes = ['playerIds', 'livetime', 'mvps']

  static jsonSchema = {
    type: 'object',
    properties: {
      playerIds: {
        type: 'array',
        minItems: 5,
        maxItems: 5,
        items: { type: 'integer', mininum: 1 },
      },
      livetime: {
        type: 'array',
        maxItems: 5,
        items: { type: 'integer', mininum: 1 },
      },
      mvps: {
        type: 'array',
        maxItems: 5,
        items: { type: 'number', mininum: 0 },
      },
    }
  }
}

export default Week
