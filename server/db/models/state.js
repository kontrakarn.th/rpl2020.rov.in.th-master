import { Model } from 'objection'

import logger from '../../kit/logger'

class State extends Model {
  static jsonAttributes = ['partitions']

  get referCode () {
    const { id, nonce } = this

    const buf = Buffer.alloc(8)
    buf.writeUInt32LE(nonce)
    buf.writeUInt32LE(id, 4)

    return buf.toString('hex')
  }

  static checkCode (code) {
    if (!/^[0-9a-f]{16}$/.test(code)) throw Error('InvalidReferCode')

    const buf = Buffer.from(code, 'hex')

    return [buf.readUInt32LE(4), buf.readUInt32LE(0)]
  }

  async checkDay (today, trx) {
    if (this.checkedDate < today) {
      const numAffect = await this.$query(trx)
        .where('checkedDate', '<', today)
        .update({
          daily: 0,
          ref: 0, rev: 0,
          checkedDate: today,
        })

      if (!numAffect) throw Error('InvalidAction')
    }
  }

  async checkWeek (weekId, trx) {
    if (this.checkedWeek < weekId) {
      const numAffect = await this.$query(trx)
        .where('checkedWeek', '<', weekId)
        .update({
          weekly: 0,
          weekWin: -1,
          checkedWeek: weekId,
        })

      if (!numAffect) throw Error('InvalidAction')
    }
  }

  async updateWeekWin (trx) {
    const { '5V5': { WinCount: winCount } } = await require('../../services/game-aov').matchHistory(this.tencentId, this.partitions[0].id, 1)
    await this.$query(trx)
      .where('weekWin', '<', 0)
      .update({
        weekWin: winCount,
      })

    logger.info('[MW] set user #%s %s weekWin %s', this.id, this.tencentId, winCount)
  }

}

export default State
