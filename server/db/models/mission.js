import { Model, raw, ref } from 'objection'

class Mission extends Model {
  #type = undefined

  static jsonAttributes = ['meta']
  static virtualAttributes = ['type']

  set type (value) {
    this.#type = value
  }

  get type () {
    return Number.isInteger(this.#type) ? this.#type : Number.isInteger(this.id) ? this.id >> 4 : undefined
  }

  static graphqlOptions = {
    fields: {
      type: 'UInt8',
    }
  }

  static async beforeUpdate ({ inputItems }) {
    const { id, type } = inputItems[0]

    if (Number.isInteger(type)) {
      inputItems[0].id = raw(`(?&?)|?`, Number.isInteger(id) ? id : ref('id'), 0x0f, type << 4)
    }
  }
}

export default Mission
