import { Model } from 'objection'

import PlayerRole from './player-role'

class Player extends Model {
  
  static relationMappings = {
    roles: {
      relation: Model.HasManyRelation,
      modelClass: PlayerRole,
      join: {
        from: `${this.tableName}.id`,
        to: `${PlayerRole.tableName}.playerId`,
      },
    },
  }
}

export default Player
