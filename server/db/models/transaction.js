import { Model, raw, ref } from 'objection'

import User from './user'
import State from './state'

import { amsSerial } from '../../services/game-aov'
import { EVENTID, MERCHANDISE } from '../../services/loot'

class Transaction extends Model {
  static jsonAttributes = ['item', 'payload']

  static relationMappings = {
    user: {
      relation: Model.HasOneRelation,
      modelClass: User,
      join: {
        from: `${this.tableName}.userId`,
        to: `${User.tableName}.id`,
      },
    },
    state: {
      relation: Model.HasOneRelation,
      modelClass: State,
      join: {
        from: `${this.tableName}.userId`,
        to: `${State.tableName}.id`,
      },
      modify: builder => {
        builder.select('id', raw('??->>?', 'partitions', '$[0].name').as('charName'))
      },
    },
  }

  static graphqlOptions = {
    fields: {
      user: false,
      state: false,
    },
  }

  async $beforeInsert (queryContext) {
    await super.$beforeInsert(queryContext)

    const { gameType, activityId, moduleId } = this.item
    if (gameType >= MERCHANDISE) {
      this.status = 'SUCCESS'
    } else if (!this.serial) {
      this.serial = amsSerial(EVENTID, activityId, moduleId)
    }
  }

  async $afterInsert (context) {
    await super.$afterInsert(context)
    if (this.status !== 'SUCCESS') {
      const qjob = require('../../jobs').default
      qjob.add('sendItem', { txnId: this.id }, {
        attempts: 2,
        backoff: 6e5,
        delay: 3000 + Math.floor(16000 * Math.random()),
        removeOnComplete: true,
      })
    }
  }
}

export default Transaction
