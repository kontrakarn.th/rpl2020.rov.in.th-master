import { Model } from 'objection'

class PlayerRole extends Model {
  static idColumn = ['playerId', 'roleId']

  static graphqlOptions = {
    fields: {
      id: false,
      playerId: false,
    },
  }

  static createdAt = false
  static updatedAt = false
}

export default PlayerRole
