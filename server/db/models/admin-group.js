import { Model } from 'objection'

class AdminGroup extends Model {
  static idColumn = ['adminId', 'groupId']

  static graphqlOptions = {
    fields: {
      adminId: false,
      createdAt: false,
      updatedAt: false,
    },
  }
}

export default AdminGroup
