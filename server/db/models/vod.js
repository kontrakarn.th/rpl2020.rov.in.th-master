import { Model } from 'objection'

class Vod extends Model {
  static jsonAttributes = ['meta']

  static jsonSchema = {
    type: 'object',
    properties: {
      id: {
        type: 'integer',
        minimum: 0,
        maximum: 31,
      },
    },
  }
}

export default Vod
