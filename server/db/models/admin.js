import { Model } from 'objection'

import Group from './group'
import AdminGroup from './admin-group'
import AdminPermission from './admin-permission'
import GroupPermission from './group-permission'

const OPERATION_MASK = {
  VIEW: 1 << 0,
  ADD: 1 << 1,
  EDIT: 1 << 2,
  DELETE: 1 << 3,
}

class Admin extends Model {
  static jsonSchema = {
    type: 'object',
    required: ['email'],
    properties: {
      email: {
        type: 'string',
        format: 'email',
      }
    }
  }

  static relationMappings = {
    groups: {
      relation: Model.HasManyRelation,
      modelClass: AdminGroup,
      join: {
        from: `${this.tableName}.id`,
        to: `${AdminGroup.tableName}.adminId`,
      },
    },
    permissions: {
      relation: Model.HasManyRelation,
      modelClass: AdminPermission,
      join: {
        from: `${Admin.tableName}.id`,
        to: `${AdminPermission.tableName}.adminId`
      },
    },
  }

  async hasPermission (model, operation) {
    switch (this.role) {
      case 'ADMIN':
        break
      case 'STAFF':
        if (operation === 'VIEW') break
      case 'GUEST':
        const perm = await AdminPermission.query()
          .select('allow').first()
          .where({ model, adminId: this.id })
          .where('allow', '&', OPERATION_MASK[operation])
        if (perm) break

        const groupPerm = await GroupPermission.query().alias('P')
          .select('allow').first()
          .where({ model, adminId: this.id })
          .where('allow', '&', OPERATION_MASK[operation])
          .innerJoin(`${AdminGroup.tableName} as G`, `P.groupId`, `G.groupId`)
        if (groupPerm) break

        throw Error(`don't have permission to ${operation} ${model}`)
      default:
        throw Error('invalid role')
    }

    return true
  }

}

export default Admin
