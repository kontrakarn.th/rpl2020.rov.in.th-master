import { Model } from 'objection'

import Item from './item'

class RegionMilestone extends Model {
  static relationMappings = {
    item: {
      relation: Model.BelongsToOneRelation,
      modelClass: Item,
      join: {
        from: `${this.tableName}.itemId`,
        to: `${Item.tableName}.id`,
      },
    }
  }

  static graphqlOptions = {
    fields: {
      item: false,
    },
  }

}

export default RegionMilestone
