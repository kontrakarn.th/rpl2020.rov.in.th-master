import { Model } from 'objection'

class AdminPermission extends Model {

  static graphqlOptions = {
    fields: {
      adminId: false,
    },
  }
}

export default AdminPermission
