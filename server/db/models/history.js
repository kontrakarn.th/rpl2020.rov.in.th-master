import { Model, raw } from 'objection'

import State from './state'

class History extends Model {

  async $afterInsert (queryContext) {
    await super.$afterInsert(queryContext)

    if (this.point > 0) {
      await State.query(queryContext.transaction).findById(this.userId)
        .update({
          lastSeq: raw('GREATEST(??,?)', 'lastSeq', this.id),
          lastTime: raw('GREATEST(??,?)', 'lastTime', this.createdAt),
        })
    }
  }
}

export default History
