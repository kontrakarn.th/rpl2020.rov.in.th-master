import { Model } from 'objection'

import redis, { cache } from '../../kit/redis-client'

const CACHETIME = 3600
const cacheKey = key => `cfg:${key}`

class Configuration extends Model {
  static jsonAttributes = ['value', 'schema']

  static jsonSchema = {
    type: 'object',
    properties: {
      schema: {
        $ref: 'http://json-schema.org/draft-07/schema#',
      }
    }
  }

  $toDatabaseJson () {
    const dJson = super.$toDatabaseJson()
    switch (typeof this.value) {
      case 'object':
        break
      default:
        dJson.value = JSON.stringify(dJson.value)
    }
    return dJson
  }

  $beforeValidate (jsonSchema, json, opt) {
    if (json.schema) {
      jsonSchema.properties.value = json.schema

      if (json.value instanceof Date) {
        json.value = json.value.toISOString()
      }
    }
    else {
      delete jsonSchema.properties.schema
    }
    return jsonSchema
  }

  static async beforeUpdate ({ asFindQuery, inputItems: { 0: { value, schema } }, transaction }) {
    if (value !== undefined) {
      // generate the related query
      const query = asFindQuery(transaction).select('key')
      if (schema === undefined) query.select('schema')
      // fetch the related rows
      const rows = await query
      // if schema updated
      if (schema !== undefined) {
        this.fromJson({ value, schema })
      } else {
        if (value instanceof Date) value = value.toISOString()

        for (const { schema } of rows) {
          this.fromJson({ value, schema })
        }
      }
      // clear cache the related keys
      if (rows.length) await redis.del(...rows.map(row => cacheKey(row.key)))
    }
  }

  static async beforeDelete ({ asFindQuery, transaction }) {
    // fetch the related keys
    const rows = await asFindQuery(transaction).select('key')
    // clear cache the related keys
    if (rows.length) await redis.del(...rows.map(row => cacheKey(row.key)))
  }

  static getDefault (key, defs) {
    return cache(cacheKey(key), async () => {
      let data = await Configuration.query().select('value').where('key', key).first()
      if (!data) {
        if (defs instanceof Function) defs = await defs()
        data = await Configuration.query().insert({ key, ...defs })
      }
      return data.value
    }, CACHETIME)
  }

  static async getEndTime () {
    return new Date(await Configuration.getDefault('endTime', () => ({
      value: new Date().toISOString(),
      type: 'DateTime',
      schema: { type: 'string', format: 'date-time' },
      description: `The end time, the users can't do the missions`,
    })))
  }

  static async getClaimEndTime () {
    return new Date(await Configuration.getDefault('claimEndTime', () => ({
      value: new Date().toISOString(),
      type: 'DateTime',
      schema: { type: 'string', format: 'date-time' },
      description: `The end time, the users can't redeem rewards`,
    })))
  }

  static async getBeginTime () {
    return new Date(await Configuration.getDefault('beginTime', () => ({
      value: new Date().toISOString(),
      type: 'DateTime',
      schema: { type: 'string', format: 'date-time' },
      description: `The begin time, the users can do the daily missions`,
    })))
  }

  static getMeta () {
    return Configuration.getDefault('meta', () => ({
      value: {
        title: 'กิจกรรม RoV Pro League 2020 Winter',
        description: 'สร้าง Dream Team ของคุณด้วยผู้เล่นระดับมืออาชีพ แล้วพิชิตภารกิจทุกวัน สะสมคะแนนรับของรางวัลฟรี!',
        keywords: 'apl,aov,...',
        ogTitle: 'กิจกรรม RoV Pro League 2020 Winter',
        ogDescription: 'สร้าง Dream Team ของคุณด้วยผู้เล่นระดับมืออาชีพ แล้วพิชิตภารกิจทุกวัน สะสมคะแนนรับของรางวัลฟรี!',
        ogImage: '//cdngarenanow-a.akamaihd.net/webth/cdn/rov/rpl2020/ogimage.png',
        fbAppId: '297788304783940',
        fbQuote: 'fb quote',
        fbHashtag: '#apl',
        fbSdk: 'https://connect.facebook.net/vi_VN/sdk.js',
        gtagId: 'GTM-PMVSP85',
      },
      type: 'Json',
      description: 'Meta data',
    }))
  }

  static getCorrectMvpPointMap () {
    return Configuration.getDefault('correctMvpMap', () => ({
      value: [0, 2, 4, 6, 8, 10],
      type: 'Json',
      description: 'correctMvpMap',
    }))
  }

  static async getLivestreamEndTime () {
    return new Date(await Configuration.getDefault('livestreamEndTime', () => ({
      value: new Date().toISOString(),
      type: 'DateTime',
      schema: { type: 'string', format: 'date-time' },
      description: `The end time, the livestream popup wouldn't been shown`,
    })))
  }

  static getLivestreamMeta () {
    return Configuration.getDefault('livestreamMeta', () => ({
      value: {
        youtubeId: 'aH7S1HGj9P0',
        title: 'Live stream title',
        giftImage: '/images/livestream-gift.png',
        quote: 'livestream fb quote',
        hashtag: '#APL2020',
        shareBtn: 'share now',
      },
      type: 'Json',
      description: 'Livestream Meta',
    }))
  }
}

export default Configuration
