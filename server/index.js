const shell = () => {
  const repl = require('repl')

  function initializeContext (context) {
    context.db = require('./db')
    context.services = require('./services')
    context.settings = require('./settings').default
    context.redis = require('./kit/redis-client').default
  }

  const r = repl.start()
  initializeContext(r.context)

  r.on('reset', initializeContext)
  r.once('exit', () => {
    const { closeServer } = require('./server')
    console.log(`Received "exit" event from repl!`)
    closeServer()
  })
}

if (require.main === module) {
  const fs = require('fs')
  const os = require('os')
  const path = require('path')
  const { program } = require('commander')

  program
    .command('cluster')
    .description('Start the server in cluster mode')
    .option('-i, --instances <num>', 'Launch [num] instances', parseInt, os.cpus().length)
    .action((argv) => {
      const cluster = require('cluster')
      const { startServer } = require('./server')
      if (cluster.isMaster) {
        // Start workers
        const numInstances = argv.instances
        for (let i = 0; i < numInstances; i++) cluster.fork({ NODE_APP_INSTANCE: i })
      } else {
        startServer()
      }
    })

  program
    .command('shell')
    .description('Run npm commands parallel')
    .action(shell)

  for (const file of fs.readdirSync(path.join(__dirname, 'commands'))) {
    if (file.charAt(0) !== '.' && file.slice(-3) === '.js') {
      const { name, builder, action } = require(`./commands/${file}`)
      const command = program.command(name || file.slice(0, -3))
      if (builder instanceof Function) builder(command)
      command.action(action)
    }
  }

  program
    .action(() => {
      const { startServer } = require('./server')
      startServer()
    })

  program.parse(process.argv)

}
