export const startServer = () => {
  const http = require('http')

  const logger = require('./kit/logger').default

  const app = require('./app').default

  const server = http.createServer(app.handler)

  const network = process.env.UDS ? { path: process.env.UDS } : { port: process.env.PORT || 8000, host: process.env.HOST }

  require('./kit/graphql/subscription').createSubscriptionServer(server)

  server.listen(network, () => {
    logger.info({ network: server.address(), instance: process.env.NODE_APP_INSTANCE }, 'Server started')
  })

  process.once('SIGINT', () => server.close(closeServer))
  process.once('exit', (code) => logger.info(`Server exit with code: ${code}`))

  return server
}

export const closeServer = () => {
  const closePromies = []
  if (require.cache[require.resolve('./db/master')]) {
    closePromies.push(require('./db/master').default.destroy())
  }
  if (require.cache[require.resolve('./kit/redis-client')]) {
    closePromies.push(require('./kit/redis-client').default.quit())
  }
  if (require('./settings').default.qjob.enable && require.cache[require.resolve('./jobs')]) {
    closePromies.push(require('./jobs').default.close())
  }
  return Promise.all(closePromies)
    .then(() => process.exit(0))
    .catch(err => {
      console.error(err)
      process.exit(1)
    })
}
