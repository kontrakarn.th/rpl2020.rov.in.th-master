import polka from 'polka'
import helmet from 'helmet'
import session from 'express-session'
import sendType from '@polka/send-type'
import { ServerResponse, IncomingMessage } from 'http'

import settings from './settings'
import logger from './kit/logger'
import grant from './kit/auth/consumer'
import RedisStore from './kit/redis-store'
import { adminGraphqlGet, adminGraphqlPost, defaultGraphqlGet, defaultGraphqlPost } from './kit/graphql/controller'

import { upload, image, share, proxy } from './services/facebook'

Object.assign(ServerResponse.prototype, {
  send (data, code, headers) {
    sendType(this, code, data, headers)
  },
  throw (data, code = 400, headers) {
    sendType(this, code, data, headers)
  },
  redirect (url, code = 302, headers) {
    sendType(this, code, '', { ...headers, location: url })
  },
})

Object.defineProperties(IncomingMessage.prototype, {
  ip: {
    get () {
      if (!this.$ip) {
        const ipv4 = this.headers['x-forwarded-for'] || this.connection.localAddress
        if (ipv4) {
          const bytes = ipv4.split('.')
          if (bytes.length === 4)
            this.$ip = Buffer.from(bytes.map(v => parseInt(v))).readUInt32BE()
        }
      }
      return this.$ip || null
    }
  },
})

const app = polka()

app.use(helmet(settings.helmet))

if (process.env.NODE_ENV === 'development') {
  app.use((req, res, next) => {
    res.once('finish', () =>
      logger.trace(`${req.socket.remoteAddress} - "${req.method} ${req.originalUrl}" ${res.statusCode} ${res.getHeader('content-length') || '-'}`)
    )
    next()
  })
}

// Session middleware
app.use(session(Object.assign({ store: new RedisStore() }, settings.session)))

// Default GraphQL
app.post(settings.graphql.defaultEndpoint, ...defaultGraphqlPost)
if (process.env.NODE_ENV === 'development') app.get(settings.graphql.defaultEndpoint, defaultGraphqlGet)

// Admin GraphQL
app.post(settings.graphql.adminEndpoint, ...adminGraphqlPost)
if (process.env.NODE_ENV === 'development') app.get(settings.graphql.adminEndpoint, adminGraphqlGet)

app.use('/connect', grant)

app.get('/fb/px', proxy)
app.get('/fb/:name?', share)
app.get('/fb/im/:name', image)
app.post('/fb/up/:name', upload)

export default app
