import { merge } from 'lodash'
import { defaultFieldResolver } from 'graphql'
import { GraphQLUpload } from 'graphql-upload'
import { makeExecutableSchema } from '@graphql-tools/schema'
import { SchemaDirectiveVisitor } from '@graphql-tools/utils'

import db from '../db'
import {
  GraphQLJSON,
  GraphQLDate, GraphQLDateTime,
  GraphQLInt8, GraphQLInt16, GraphQLInt24, GraphQLInt32, GraphQLInt64,
  GraphQLUInt8, GraphQLUInt16, GraphQLUInt24, GraphQLUInt32, GraphQLUInt64,
} from '../kit/graphql/scalars'
import { graphSchema as jobGraphSchema } from '../jobs'
import { typedefs as adminTypedefs, resolvers as adminResolvers } from '../services/admin'

const typeDefs = [`
scalar JSON
scalar Upload

scalar Date
scalar DateTime

scalar Int8
scalar UInt8

scalar Int16
scalar UInt16

scalar Int24
scalar UInt24

scalar Int32
scalar UInt32

scalar Int64
scalar UInt64

schema {
  query: Query
  mutation: Mutation
}

type Query {
  # Current admin account info
  currentAdmin: Admin
}

type Mutation

enum ModelEnum

enum OperationEnum { VIEW ADD EDIT DELETE }

directive @auth(model: ModelEnum, operation: OperationEnum) on FIELD_DEFINITION

`]

const resolvers = {
  JSON: GraphQLJSON,
  Date: GraphQLDate,
  Upload: GraphQLUpload,
  DateTime: GraphQLDateTime,
  Int8: GraphQLInt8,
  Int16: GraphQLInt16,
  Int24: GraphQLInt24,
  Int32: GraphQLInt32,
  Int64: GraphQLInt64,
  UInt8: GraphQLUInt8,
  UInt16: GraphQLUInt16,
  UInt24: GraphQLUInt24,
  UInt32: GraphQLUInt32,
  UInt64: GraphQLUInt64,
  Query: {
    currentAdmin: (parent, args, ctx, info) => ctx.session.admin
  }
}

class AuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition (field) {
    const { resolve = defaultFieldResolver } = field
    const { model, operation } = this.args

    field.resolve = async function () {
      const { 2: { session: { admin: { id, email } } } } = arguments
      const admin = await db.Admin.query().where({ id, email }).select('id', 'email', 'role').first()
      await admin.hasPermission(model, operation)

      arguments[2].admin = admin
      return resolve.apply(this, arguments)
    }
  }
}

const schemaDirectives = {
  auth: AuthDirective,
}

// Merge
const mergeModelGraphql = async () => {
  for await (const graphql of Object.values(db).map(model => model.graphql())) {
    typeDefs.push(...graphql.typeDefs)
    merge(resolvers, graphql.resolvers)
  }

  typeDefs.push(jobGraphSchema.typeDefs)
  merge(resolvers, jobGraphSchema.resolvers)

  typeDefs.push(adminTypedefs)
  merge(resolvers, adminResolvers)

  return makeExecutableSchema({ typeDefs, resolvers, schemaDirectives })
}

export default mergeModelGraphql
