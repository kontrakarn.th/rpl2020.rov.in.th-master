import { makeExecutableSchema } from '@graphql-tools/schema'

import { typeDefs, resolvers, schemaDirectives } from '../services'

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
  schemaDirectives,
})

export default schema
