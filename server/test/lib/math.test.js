import expect from 'expect'
import { factorial, combination } from '../../libs/math'

describe('math', () => {
  describe('factorial', () => {
    it('returns the factorial of a non-negative integer', () => {
      expect(factorial(0)).toBe(0)
      expect(factorial(1)).toBe(1)
      expect(factorial(2)).toBe(2)
      expect(factorial(3)).toBe(6)
      expect(factorial(4)).toBe(24)
    })
  })

  describe('combination', () => {
    it('returns the combination C(n,k)', () => {
      expect(combination(6, 2)).toBe(15)
      expect(combination(1000, 1000)).toBe(1)
    })
  })
})
