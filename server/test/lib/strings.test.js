import expect from 'expect'
import { titleize } from '../../libs/strings'

describe('strings', () => {
  describe('titleize', () => {
    it('Upcase the first characters of words', () => {
      expect(titleize('test string')).toEqual('Test String')
      expect(titleize('teststring')).toEqual('Teststring')
      expect(titleize('')).toEqual('')
    })
  })
})
