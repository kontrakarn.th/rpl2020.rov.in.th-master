import expect from 'expect'
import redis, { getOrSet } from '../../kit/redis-client'

describe('redis-client', () => {
  describe('getOrSet', () => {
    beforeEach(async () => {
      await redis.flushdb()
    })

    it('creates new key-pair if does not exist', async () => {
      const res = await getOrSet('testkey', async () => ('testvalue'))
      expect(res).toEqual('testvalue')
      expect(await redis.get('testkey')).toEqual('testvalue')
    })

    it('returns the value if key is existing', async () => {
      const func = jest.fn()
      func.mockReturnValue('newvalue')

      await redis.set('testkey', 'testvalue')
      const res = await getOrSet('testkey', async () => { func() })
      expect(res).toEqual('testvalue')
      expect(func).not.toHaveBeenCalled()
    })

    it('works with ttl', async () => {
      await getOrSet('testkey', async () => ('testvalue'), 10)
    })

    afterAll(async () => {
      await redis.quit()
    })
  })
})
