/**
 * @deprecated. Please use lock-helper instead.
 */
export default (redis, { lockedHandler, timeout } = {}) => {
  const acquireLock = async (key) => {
    const currentTime = parseInt((await redis.time())[0])
    const lockValue = currentTime + (timeout || 0)

    if (await redis.setnx(key, lockValue)) { return true }
    // If cannot acquire lock & lock doesn't expire then return false
    if (!timeout || (parseInt(await redis.get(key)) > currentTime)) { return false }
    // If the lock has expired, try to acquire lock again
    const oldValue = parseInt(await redis.getset(key, lockValue))
    // Return if the lock value has not been changed by other
    return (oldValue < currentTime)
  }

  const releaseLock = async (key) => redis.del(key)

  const lock = (fkey, asyncFn) => async (...args) => {
    const key = (typeof fkey === 'function') ? fkey(...args) : fkey

    if (await acquireLock(key)) {
      try {
        return await asyncFn(...args)
      } finally {
        await releaseLock(key)
      }
    } else {
      if (lockedHandler) {
        return lockedHandler(...args)
      } else {
        throw new Error(`Unable to acquire lock "${key}"`)
      }
    }
  }

  return { lock }
}
