import lodash from 'lodash'
import pluralize from 'pluralize'
import { DataTypes, Op } from 'sequelize'

import logger from '../kit/logger'

const GET_ONE = 1
const GET_MANY = 1 << 1
const CREATE = 1 << 2
const UPDATE = 1 << 3
const DELETE = 1 << 4
const ALL = GET_ONE | GET_MANY | CREATE | UPDATE | DELETE
export const GQL_ROLE = { GET_ONE, GET_MANY, CREATE, UPDATE, DELETE, ALL }

const SUPPORT_OPERATOR = new Set(['lte', 'gte', 'lt', 'gt', 'like', 'in'])

export default (model) => {
  const {
    options: { name: { singular } },
    gqlOptions: { role = ALL, fields = {} } = {},
    attributes,
    sequelize,
    associations,
  } = model

  const plural = pluralize(singular)

  const typeDefs = []
  const queries = []
  const mutations = []
  const resolvers = { Query: {}, Mutation: {} }

  const gqlFields = {}

  for (const key of Object.keys(attributes)) {
    const { allowNull } = attributes[key]

    let { type } = attributes[key]
    let gqlType = 'String'

    if (type instanceof DataTypes.VIRTUAL) type = type.returnType

    if (type instanceof DataTypes.DATE) gqlType = 'DateTime'
    else if (type instanceof DataTypes.DATEONLY) gqlType = 'Date'
    else if (type instanceof DataTypes.BIGINT) gqlType = 'Int64'
    else if (type instanceof DataTypes.INTEGER) gqlType = 'Int'
    else if (type instanceof DataTypes.FLOAT) gqlType = 'Float'
    else if (type instanceof DataTypes.BOOLEAN) gqlType = 'Boolean'
    else if (type instanceof DataTypes.JSON) gqlType = 'JSON'
    else if (type instanceof DataTypes.ENUM) {
      gqlType = `${singular}${lodash.startCase(key)}Enum`
      typeDefs.push(`enum ${gqlType} {${type.values.join(',')}}`)
    }

    gqlFields[key] = {
      type: gqlType + (allowNull === false ? '!' : '')
    }
  }

  for (const association of Object.values(associations)) {
    let gqlType = association.target.name
    switch (association.associationType) {
      case 'HasMany':
      case 'BelongsToMany': {
        gqlType = `[${gqlType}]`
        break
      }
      case 'HasOne':
      case 'BelongsTo': {
        break
      }
    }
    gqlFields[association.as] = {
      type: gqlType,
      render: false,
    }
  }

  typeDefs.push(`type ${singular} {${Object.keys(gqlFields).reduce((acc, key) => {
    const { type, render, ...meta } = lodash.merge(gqlFields[key], fields[key])
    return render === false ? acc : acc + (Object.keys(meta).length ? `#${JSON.stringify(meta)}\n` : '') + `${key}:${type}\n`
  }, '')}}`)
  typeDefs.push(`type ${singular}Page {\nitems:[${singular}]\ntotalCount:Int\n}`)

  const dataToInclude = (data) => {
    const selections = new Set(Object.keys(data))

    const include = []
    for (const key of Object.keys(associations)) {
      if (selections.has(key)) include.push(key)
    }

    return include
  }

  const selectionSetToInclude = (selectionSet) => {
    const selections = new Set()
    for (const selection of selectionSet.selections) {
      selections.add(selection.name.value)
    }

    const include = []
    for (const key of Object.keys(associations)) {
      if (selections.has(key)) include.push(key)
    }

    return include
  }

  if (role & GET_MANY) {
    queries.push(`getPageOf${plural}(page: Int, perPage: Int, sortField: String, sortOrder: String, filter: JSON): ${singular}Page`)
    resolvers.Query[`getPageOf${plural}`] = async (root, { page = 0, perPage = 10, sortField, sortOrder = 'asc', filter: condition = {} }, context, info) => {
      const order = sortField && [[sortField, sortOrder]]
      if (condition.ids) {
        condition.id = condition.ids
        delete condition.ids
      } else {
        for (const key of Object.keys(condition)) {
          const postOpIndex = key.lastIndexOf('_')
          if (postOpIndex < 0) continue
          const op = key.slice(postOpIndex + 1)
          if (SUPPORT_OPERATOR.has(op)) {
            const realKey = key.replace(new RegExp(`_${op}$`), '')
            condition[realKey] = { ...condition[realKey], [Op[op]]: condition[key] }
            delete condition[key]
          }
        }
      }

      return {
        where: condition,
        order: order,
        limit: perPage,
        offset: page * perPage,
      }
    }

    resolvers[singular + 'Page'] = {
      items: ({ where, order, limit, offset }, args, context, info) => model.findAll({
        where,
        order,
        limit,
        offset,
        include: selectionSetToInclude(info.fieldNodes[0].selectionSet)
      }),
      totalCount: ({ where }, args, context, info) => model.count({ where }),
    }
  }

  if (role & GET_ONE) {
    queries.push(`get${singular}(id: ID!): ${singular}`)
    resolvers.Query[`get${singular}`] = (root, { id }, context, info) => model.findOne({
      where: { id },
      include: selectionSetToInclude(info.fieldNodes[0].selectionSet)
    })
  }

  if (role & CREATE) {
    mutations.push(`create${singular}(data: JSON!): ${singular}`)
    resolvers.Mutation[`create${singular}`] = async (root, { data: params }, context, info) => {
      const include = dataToInclude(params)
      const resource = await sequelize.transaction(transaction => model.create(params, { include, transaction }))
      if (context.admin) logger.admin(`${context.admin.email} - create ${singular} - ${JSON.stringify(params)}`)
      return resource
    }
  }

  if (role & UPDATE) {
    mutations.push(`update${singular}(id: ID!, data: JSON!): ${singular}`)
    resolvers.Mutation[`update${singular}`] = async (root, { id, data: params }, context, info) => {
      const resource = await sequelize.transaction(async transaction => {
        const include = new Set(selectionSetToInclude(info.fieldNodes[0].selectionSet))
        const instance = await model.findOne({ where: { id }, include: Array.from(include), transaction })
        if (!instance) throw Error(`${singular} not found!`)

        for (const key of dataToInclude(params)) {
          const nestedParams = params[key]
          delete params[key]

          const association = associations[key]
          const oldRef = include.has(key) ? instance.get(key) : await association.get(instance, { transaction })

          switch (association.associationType) {
            // TODO support more associationType
            // case 'BelongsToMany': TODO
            case 'HasMany': {
              const oldMap = new Map(oldRef.map(ref => [ref.id, ref]))
              const newRefs = []
              for (const v of nestedParams || []) {
                if (v.id && oldMap.has(v.id)) {
                  newRefs.push(await oldMap.get(v.id).update(v, { transaction }))
                  oldMap.delete(v.id)
                } else {
                  newRefs.push(await association.create(instance, v, { transaction }))
                }
              }
              for (const ref of oldMap.values()) await ref.destroy({ transaction })

              instance[key] = newRefs
              break
            }
            // case 'BelongTo': TODO
            case 'HasOne': {
              if (oldRef) {
                if (nestedParams) await oldRef.update(nestedParams, { transaction })
                else {
                  await oldRef.destroy({ transaction })
                  instance[key] = null
                }
              } else if (nestedParams) {
                instance[key] = await association.create(instance, nestedParams, { transaction })
              }
              break
            }
          }
        }
        return instance.update(params, { transaction })
      })
      if (context.admin) logger.admin(`${context.admin.email} - update ${singular}#${id} - ${JSON.stringify(params)}`)
      return resource
    }
  }

  if (role & DELETE) {
    mutations.push(`remove${singular}(id: ID!): ${singular}`)
    resolvers.Mutation[`remove${singular}`] = async (root, { id }, context) => {
      const resource = await model.findOne({ where: { id } })
      if (!resource) throw Error(`${singular} not found!`)
      await resource.destroy()
      if (context.admin) logger.admin(`${context.admin.email} - delete ${singular}#${id}`)
      return resource
    }
  }

  if (queries.length > 0) typeDefs.push(`extend type Query {\n${queries.join('\n')}\n}`)
  if (mutations.length > 0) typeDefs.push(`extend type Mutation {\n${mutations.join('\n')}\n}`)

  return { typeDefs, resolvers }
}
