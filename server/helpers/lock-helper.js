import redis from '../kit/redis-client'

const acquireLock = async (key, { timeout }) => {
  const currentTime = parseInt((await redis.time())[0])
  const lockValue = currentTime + (timeout || 0)

  if (await redis.setnx(key, lockValue)) { return true }
  // If cannot acquire lock & lock doesn't expire then return false
  if (!timeout || (parseInt(await redis.get(key)) > currentTime)) { return false }
  // If the lock has expired, try to acquire lock again
  const oldValue = parseInt(await redis.getset(key, lockValue))
  // Return if the lock value has not been changed by other
  return (oldValue < currentTime)
}

const releaseLock = async (key) => redis.del(key)

/**
 * Assure that only one fn with the same key is being called at a time.
 *
 * @param {String} key: The key will be used to check lock
 * @param {Function} asyncFn: Function will be called if can acquire lock
 * @param {Object} options:
 *    {Function} lockedHandler: function being called if can't acquire lock. Default is throwing Error.
 *    {Number} timeout: The limit duration (in second) the function can hold the lock. If the execution time exceeds
 *      this number, the other function can acquire the lock. Default is no limit.
 * @returns {Any}: Result of calling asyncFn
 */
export default async (key, asyncFn, { lockedHandler, timeout } = {}) => {
  if (await acquireLock(key, { timeout })) {
    try {
      return await asyncFn()
    } finally {
      await releaseLock(key)
    }
  } else {
    if (lockedHandler) {
      return lockedHandler()
    } else {
      throw new Error(`Unable to acquire lock "${key}"`)
    }
  }
}
