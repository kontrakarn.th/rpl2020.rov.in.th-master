import moment from 'moment'
import Sequelize from 'sequelize'

export const around = ({ tz, time, left = -7, right = 0, unit = 'day' } = {}) => {
  let selectTime = moment(time || new Date())
  if (tz) { selectTime = selectTime.tz(tz) }
  const startTime = selectTime.clone().add(left, unit).startOf(unit)
  const endTime = selectTime.clone().add(right, unit).endOf(unit)
  return {
    [Sequelize.Op.gte]: startTime,
    [Sequelize.Op.lte]: endTime,
  }
}

export default {
  around,
}
