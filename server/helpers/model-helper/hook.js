const CHANGING_HOOKS = ['afterSave', 'afterDestroy', 'afterBulkCreate', 'afterBulkDestroy', 'afterBulkUpdate']

export const onChange = (models, func) => {
  for (const hook of CHANGING_HOOKS) {
    for (const model of models) {
      model.addHook(hook, func)
    }
  }
}
