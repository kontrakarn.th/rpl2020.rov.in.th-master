export const asyncValidate = (f) => function (next) {
  const $this = this
  const check = async () => {
    try {
      await f($this)
      next()
    } catch (ex) {
      next(ex)
    }
  }
  return check()
}
