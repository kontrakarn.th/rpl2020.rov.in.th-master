import moment from 'moment'
import { query } from './model-helper'

const FORMAT = {
  'second': 'YYYY-MM-DD HH:mm:ss',
  'minute': 'YYYY-MM-DD HH:mm',
  'hour': 'YYYY-MM-DD HH',
  'day': 'YYYY-MM-DD',
  'month': 'YYYY-MM',
}

export const timename = (time, { tz, unit = 'day' } = {}) => {
  let selectTime = moment(time)
  if (tz) { selectTime = selectTime.tz(tz) }
  return selectTime.format(FORMAT[unit])
}

export const fillTimeRange = (groups = [], { tz, format, time, unit = 'day', left = 0, right = 0, init = 0 } = {}) => {
  let selectTime = moment(time)
  if (tz) { selectTime = selectTime.tz(tz) }
  format = format || FORMAT[unit]
  const res = {}
  for (let x = left; x <= right; ++x) {
    const groupName = selectTime.clone().add(x, unit).startOf(unit).format(format)
    res[groupName] = groups[groupName] === undefined ? init : groups[groupName]
  }
  return res
}

const countFn = (v, r) => ((v || 0) + 1)

/**
 * Return basic chart data
 * Examples:
 * const usersChart = () => recent(User)
 * const rpChart = () => recent(Transaction, {
 *   backgroundColor: '#FF6600',
 *   label: 'RP',
 *   left: -15,
 *   unit: 'day',
 *   fn: (v, r) => (v || 0) + r.rp,
 * })
 */
export const recent = async (model, options) => {
  const { attr = 'createdAt', label = 'Total', backgroundColor = '#0000FF', tz, unit, fn } = options
  const records = await model.findAll({ where: { [attr]: query.around(options) } })
  let groups = records.reduce((g, r) => {
    const name = timename(r[attr], { tz, unit })
    g[name] = (fn || countFn)(g[name], r)
    return g
  }, {})
  groups = fillTimeRange(groups, options)
  return {
    labels: Object.keys(groups),
    datasets: [{
      label,
      backgroundColor,
      data: Object.values(groups),
    }]
  }
}
