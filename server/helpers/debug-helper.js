import logger from '../kit/logger'

export const sleep = (duration) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => resolve(), duration * 1000)
  })
}

export const benchmark = async (promise, options = {}) => {
  if (typeof options === 'string') {
    options = { name: options }
  }
  const { name = promise } = options

  const start = new Date()
  logger.debug(`Start [${name}] at ${start}`)
  const result = await promise
  const end = new Date()
  logger.debug(`End [${name}] at ${end}`)
  const duration = end - start
  logger.debug(`Duration of [${name}]: ${duration}`)
  return result
}
