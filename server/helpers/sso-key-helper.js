const crypto = require('crypto')

const checksum = (buff) => {
  let checksum = 0
  for (let i = 0; i < buff.length; i += 4) checksum ^= buff.readUInt32LE(i)
  return checksum
}

const encryptSessionKey = (platform, uid, username, timestamp, key) => {
  if (platform !== 1) throw Error('Not support nongarena platform')

  if (!(key instanceof Buffer)) key = Buffer.from(key, 'hex')

  // init the plain buffer
  const plain = Buffer.alloc(32)

  let i = 0
  // random prefix
  crypto.randomFillSync(plain, i, 3)
  i += 3

  // write platform
  plain.writeUInt8(platform, i)
  i += 1

  // write timestamp
  plain.writeUInt32LE(timestamp, i)
  i += 4

  // write uid
  plain.writeUInt32LE(uid, i)
  i += 4

  // write username
  plain.write(username, i, 16, 'ascii')
  i += 16

  // calc checksum
  plain.writeUInt32LE(checksum(plain), i)

  // init the cipher
  const cipher = crypto.createCipheriv('AES-256-CBC', key, Buffer.alloc(16))
  return cipher.update(plain, 'hex', 'hex')
}

const decryptSessionKey = (session, key) => {
  if (session.length !== 64) throw Error('Invalid session key')
  if (!(key instanceof Buffer)) key = Buffer.from(key, 'hex')

  // init the decipher
  const decipher = crypto.createDecipheriv('AES-256-CBC', key, Buffer.alloc(16))
  decipher.setAutoPadding(false)

  // decrypt the plain buff
  const plain = decipher.update(session, 'hex')

  // validate checksum
  if (checksum(plain)) throw Error('Invalid session key')

  let i = 3
  // read platform
  const platform = plain.readUInt8(i)
  i += 1

  // validate platform
  if (platform !== 1) throw Error('Invalid session key')

  // read timestamp
  const timestamp = plain.readUInt32LE(i)
  i += 4

  // validate the timestamp
  const offset = (Date.now() / 1000 | 0) - timestamp
  if (offset < 0 || offset > 86400) throw Error('Invalid timestamp')

  // read uid
  const uid = plain.readUInt32LE(i)
  i += 4

  // read username
  const username = plain.toString('ascii', i, i + 16).replace(/\0+/, '')
  return { platform, timestamp, uid, username }
}

module.exports = { encryptSessionKey, decryptSessionKey }

const test = () => {
  const key = crypto.randomBytes(32).toString('hex')
  console.log('key', key)

  const en = encryptSessionKey(1, 200145, 'quang', Date.now() / 1000 | 0, key)
  console.log('encrypted', en)

  const de = decryptSessionKey(en, key)
  console.log('decrypted', de)

}
//test()
