const { URL } = require('url')
const { Agent: HttpsAgent } = require('https')
const { Agent, ClientRequest } = require('http')

const AGENT_OPTIONS = {
  keepAlive: true,
  maxSockets: 128,
  maxFreeSockets: 16,
}

const HTTP_AGENT = new Agent(AGENT_OPTIONS)
const HTTPS_AGENT = new HttpsAgent(AGENT_OPTIONS)

async function raw () {
  const raw = []
  for await (const chunk of this) raw.push(chunk)
  return Buffer.concat(raw)
}

const ResponseProtype = {
  text: {
    value: function () {
      return raw.apply(this).then(res => res.toString())
    }
  },
  json: {
    value: function () {
      return raw.apply(this).then(JSON.parse)
    }
  },
  ok: { get () { return this.statusCode >= 200 && this.statusCode < 300 } },
  status: { get () { return this.statusCode } },
  statusText: { get () { return this.statusMessage } },
}

const fetch = (url, options = {}) => {
  const { hostname: host, port, pathname, search, protocol } = new URL(url)
  Object.assign(options, { host, port, path: pathname + search, protocol, agent: protocol === 'https:' ? HTTPS_AGENT : HTTP_AGENT })
  return new Promise((resolve, reject) => {
    const req = new ClientRequest(options, res => {
      Object.defineProperties(res, ResponseProtype)
      resolve(res)
    })
    req.once('error', reject)
    if (options.body) req.write(options.body)
    req.end()
  })
}

module.exports = fetch
