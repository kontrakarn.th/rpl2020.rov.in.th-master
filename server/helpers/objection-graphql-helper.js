import moment from 'moment'
import lodash from 'lodash'
import { Model } from 'objection'

import logger from '../kit/logger'

const SUPPORT_OPERATOR = new Map([
  ['lte', '<='],
  ['gte', '>='],
  ['lt', '<'],
  ['gt', '>'],
  ['like', 'like'],
  ['in', 'in'],
  ['notIn', 'notIn'],
  ['contain', 'contain'],
])

const info2SelectionSet = info => {
  const { fieldNodes: [{ selectionSet: { selections } }] } = info
  return new Set(selections.map(selection => selection.name.value))
}

const ISO_8601_FULL = 'YYYY-MM-DD[T]HH:mm:ss.SSSZ'
const tranformRow = (data, model) => {
  for (const [key, value] of Object.entries(data)) {
    switch (typeof value) {
      case 'string':
        const time = moment(value, ISO_8601_FULL, true)
        if (time.isValid()) {
          data[key] = time.toDate()
        }
        break
      case 'array':
        if (model.getRelations()[key]) {
          const { relatedModelClass } = model.getRelations()[key]

          for (const row of value) {
            tranformRow(row, relatedModelClass)
          }
        }
        break
      case 'object':
        if (model.getRelations()[key]) {
          const { relatedModelClass } = model.getRelations()[key]

          tranformRow(value, relatedModelClass)
        }
        break
    }
  }

  return data
}

const buildFilter = (builder, filter, model, prefix) => {
  const fieldBuilder = prefix
    ? name => `${prefix}.${model.propertyNameToColumnName(name)}`
    : name => model.propertyNameToColumnName(name)

  for (const [key, value] of Object.entries(filter)) {
    const postOpIndex = key.lastIndexOf('_')
    if (postOpIndex >= 0) {
      const op = SUPPORT_OPERATOR.get(key.slice(postOpIndex + 1))
      if (op) {
        const field = fieldBuilder(key.slice(0, postOpIndex))
        switch (op) {
          case 'in':
            builder.whereIn(field, value)
            break
          case 'notIn':
            builder.whereNotIn(field, value)
            break
          case 'contain':
            if (value) builder.where(field, 'like', `%${value}%`)
            break
          default:
            builder.where(field, op, value)
        }
        continue
      }
    }
    builder.where(fieldBuilder(key), value)
  }
}

const checkUpdateRelation = (row, model) => {
  let useRelation = false
  const upsertOptions = { relate: [], unrelate: [], insertMissing: [] }
  for (const key of Object.keys(model.getRelations())) {
    if (row.hasOwnProperty(key)) {
      useRelation = true
      const { [key]: relation } = model.getRelations()

      switch (relation.constructor) {
        case model.HasManyRelation: {
          upsertOptions.insertMissing.push(key)
          break
        }
        case model.ManyToManyRelation: {
          upsertOptions.relate.push(key)
          upsertOptions.unrelate.push(key)
          break
        }
      }
    }
  }

  return [useRelation, upsertOptions]
}

async function model2gql () {
  const { name, graphqlOptions = {} } = this
  const typeDefs = [...graphqlOptions.typeDefs || []]
  const { resolvers = {}, fields: fieldOptions = {} } = graphqlOptions

  const metaRows = await this.knex().withSchema('INFORMATION_SCHEMA').from('COLUMNS')
    .where('table_name', this.tableName)
    .where('table_schema', this.knex().client.database())
    .orderBy('ORDINAL_POSITION', 'ASC')

  if (!metaRows.length) return { typeDefs, resolvers }

  const fields = {}

  const meta2field = meta => {
    let type = 'String'
    switch (meta.DATA_TYPE) {
      case 'tinyint':
        type = meta.COLUMN_TYPE === 'tinyint(1)' ? 'Boolean'
          : meta.COLUMN_TYPE.endsWith('unsigned') ? 'UInt8' : 'Int8'
        break
      case 'smallint':
        type = meta.COLUMN_TYPE.endsWith('unsigned') ? 'UInt16' : 'Int16'
        break
      case 'mediumint':
        type = meta.COLUMN_TYPE.endsWith('unsigned') ? 'UInt24' : 'Int24'
        break
      case 'int':
        type = meta.COLUMN_TYPE.endsWith('unsigned') ? 'UInt32' : 'Int32'
        break
      case 'bigint':
        type = meta.COLUMN_TYPE.endsWith('unsigned') ? 'UInt64' : 'Int64'
        break
      case 'float':
      case 'decimal':
        type = 'Float'
        break
      case 'boolean':
        type = 'Boolean'
        break
      case 'datetime':
      case 'timestamp':
        type = 'DateTime'
        break
      case 'date':
        type = 'Date'
        break
      case 'json':
        type = 'JSON'
        break
      case 'enum':
        type = `${name}${lodash.capitalize(meta.COLUMN_NAME)}Enum`
        typeDefs.push(`enum ${type} {\n${meta.COLUMN_TYPE.slice(6, -2).replace(/','/g, '\n')}\n}`)
        break
    }
    return type
  }

  for (const meta of metaRows) {
    fields[this.columnNameToPropertyName(meta.COLUMN_NAME)] = meta2field(meta)
  }

  for (const att of this.jsonAttributes || []) {
    fields[att] = 'JSON'
  }

  // Check relation
  for (const relation of Object.values(this.getRelations())) {
    const { name, relatedModelClass } = relation
    switch (relation.constructor) {
      case Model.HasManyRelation:
      case Model.ManyToManyRelation:
        fields[name] = `[${relatedModelClass.name}]`
        break
      default:
        fields[name] = relatedModelClass.name
    }
  }

  for (const [key, opt] of Object.entries(fieldOptions)) {
    if (opt === false) delete fields[key]
    if (typeof opt === 'string') fields[key] = opt
  }

  if (this.idColumn !== 'id' && !fields.id && fieldOptions.id !== false) {
    fields.id = 'ID'

    lodash.defaultsDeep(resolvers, {
      [name]: {
        id: parent => {
          if (parent.$id instanceof Function) {
            const tmp = parent.$id()
            return Array.isArray(tmp) ? tmp.join(':') : tmp
          }
        }
      }
    })
  }

  const { id: idType = 'ID' } = fields

  return {
    typeDefs: [`
      extend enum ModelEnum {
        ${name}
      }

      type ${name} {
        ${Object.entries(fields).map(([key, value]) => `${key}: ${value}`).join('\n')}
      }

      type ${name}Page {
        results: [${name}]
        total: UInt64
      }

      extend type Query {
        get${name}(id: ${idType}!): ${name} @auth(model: ${name}, operation: VIEW)
        list${name}(
          page: UInt32 = 0
          perPage: UInt32 = 10
          sortField: String
          sortOrder: String
          filter: JSON
        ): ${name}Page @auth(model: ${name}, operation: VIEW)
      }

      extend type Mutation {
        create${name}(data: JSON!): ${name} @auth(model: ${name}, operation: ADD)
        update${name}(ids: [${idType}!]!, data: JSON!): [${idType}!]! @auth(model: ${name}, operation: EDIT)
        remove${name}(ids: [${idType}!]!): [${idType}!]! @auth(model: ${name}, operation: DELETE)
      }
      `, ...typeDefs,
    ],
    resolvers: lodash.defaultsDeep(resolvers, {
      [`${name}Page`]: {
        results: ({ query, page, perPage, sortField, sortOrder }, args, ctx, info) => {
          query = query.clone()

          const selectionSet = info2SelectionSet(info)
          for (const key of Object.keys(this.getRelations())) {
            if (selectionSet.has(key)) query.withGraphFetched(key)
          }

          query.limit(perPage).offset(page * perPage)
          if (sortField) query.orderBy(this.propertyNameToColumnName(sortField), sortOrder)

          return query
        },
        total: ({ query }) => query.clone().count({ t: '*' }).first().then(r => r.t)
      },
      Query: {
        [`list${name}`]: (parent, { page, perPage, sortField, sortOrder, filter }, ctx, info) => {
          filter = filter || {}
          page = Math.max(0, page)
          perPage = Math.max(0, Math.min(10000, perPage))

          const query = this.query().alias('Q')

          // Filter
          tranformRow(filter, this)
          for (const [key, value] of Object.entries(filter)) {
            if (value && typeof value === 'object' && this.getRelations()[key]) {
              const relation = this.getRelations()[key]
              if (Object.keys(value).length) {
                const relationQuery = this.relatedQuery(key).alias('R')
                buildFilter(relationQuery, value, relation.relatedModelClass, 'R')
                query.whereExists(relationQuery)
                delete filter[key]
              }
            }
          }
          buildFilter(query, filter, this)

          return {
            query,
            page, perPage,
            sortField, sortOrder,
          }
        },
        [`get${name}`]: (parent, { id }, ctx, info) => {
          if (Array.isArray(this.idColumn)) id = id.split(':')

          const query = this.query().findById(id).throwIfNotFound()

          const selectionSet = info2SelectionSet(info)
          for (const key of Object.keys(this.getRelations())) {
            if (selectionSet.has(key)) query.withGraphFetched(key)
          }

          return query
        },
      },
      Mutation: {
        [`create${name}`]: async (parent, { data }, ctx, info) => this.transaction(async trx => {
          let ret

          if (Array.isArray(data)) {
            ret = await Promise.all(data.map(
              row => this.query(trx).insertGraph(tranformRow(row, this))
            ))
          } else {
            ret = await this.query(trx).insertGraph(tranformRow(data, this))
          }

          logger.info('[ADMIN] %s create %s %o', ctx.admin.email, name, ret)
          return Array.isArray(ret) ? ret[0] : ret
        }),
        [`update${name}`]: async (parent, { ids, data }, ctx, info) => {
          if (!ids.length) throw Error(`The 'ids' length must be greater than 0`)

          let rows

          if (Array.isArray(data)) {
            if (data.length !== ids.length) throw Error(`The 'data' must has the same length with the 'ids'`)
            rows = data
          } else {
            rows = [data]
          }

          // tranform the key fields
          const keys = Array.isArray(this.idColumn) ? ids.map(id => id.split(':')) : ids

          // tranform the data
          for (const row of rows) tranformRow(row, this)

          return this.transaction(async trx => {
            if (rows.length === 1) {
              const row = rows[0]
              const [useRelation, upsertOptions] = checkUpdateRelation(row, this)

              if (useRelation && keys.length > 1) throw Error('Relation update is not working with batch update ')

              if (useRelation) {
                const obj = this.fromJson(row, { patch: true })
                obj.$id(keys?.[0])

                await this.query(trx).upsertGraph(obj, upsertOptions)
              } else {
                await this.query(trx).findByIds(keys).patch(row)
              }
            } else {
              await Promise.all(rows.map(async (row, idx) => {
                const key = keys[idx]

                const [useRelation, upsertOptions] = checkUpdateRelation(row, this)

                if (useRelation) {
                  const obj = this.fromJson(row, { patch: true })
                  obj.$id(key)

                  await this.query(trx).upsertGraph(obj, upsertOptions)
                } else {
                  await this.query(trx).findById(key).patch(row)
                }
              }))
            }

            logger.info('[ADMIN] %s update %s #[%s] data %o', ctx.admin.email, name, ids, data)
            return ids
          })
        },
        [`remove${name}`]: async (parent, { ids }, ctx, info) => {
          if (!ids.length) throw Error(`The 'ids' length must be greater than 0`)

          // tranform the key fields
          const keys = Array.isArray(this.idColumn) ? ids.map(id => id.split(':')) : ids

          return this.transaction(async trx => {
            await this.query(trx).findByIds(keys).delete()

            logger.info('[ADMIN] %s remove %s #[%s]', ctx.admin.email, name, ids)
            return ids
          })
        },
      },
    }),
  }
}

export default model2gql
