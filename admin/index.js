import 'unfetch/polyfill'
import 'regenerator-runtime/runtime' // Enable async/await and generators, cross-browser

import React from 'react'
import ReactDOM from 'react-dom'
import { ApolloProvider } from '@apollo/client'

import App from './app'
import client from './kit/graphql'

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
  </ApolloProvider>,
  document.getElementById('main'),
)
