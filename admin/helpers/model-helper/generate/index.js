import React from 'react'
import Icon from '@material-ui/core/Icon'
import { EditButton, ShowButton } from 'react-admin/esm'

import convention from './convention'

/**
 * Generate admin model
 *
 * Example:
 * import {generate} from 'admin/helpers/model-helper'
 *
 * export default 
 *   generate('Exchange', {
 *     only: ['list', 'show'],
 *     listFields: {
 *       description: null, // set to null to remove field
 *     },
 *     editFields: {
 *       status: <SelectInput source='status' choices={{id: 1, name: 'started'}, {id: 2, name: 'finished'}}/>,
 *     },
 *     icon: 'list',
 *   })
 *  
 * 
 */
export default (model, options = {}) => {
  const {
    only = ['show', 'create', 'edit', 'list'],
    generator = convention,
    showFields,
    listFields,
    editInputs,
    createInputs,
    icon,
    options: { list, show, create, edit, label } = {},
  } = options

  const { listFor, showFor, createFor, editFor } = generator
  const res = {
    name: model,
    icon: icon && function Ico () {
      return typeof icon !== 'string' ? icon :
        icon.startsWith('fa') ? <span className="fa-icon"><i className={icon} /></span> : <Icon>{icon}</Icon>
    },
    options: { label },
  }

  const actions = {}
  if (only.includes('show')) {
    res.show = showFor(model, show, showFields)
    actions._show = <ShowButton />
  }
  if (only.includes('create')) {
    res.create = createFor(model, create, createInputs)
  }
  if (only.includes('edit')) {
    res.edit = editFor(model, edit, editInputs)
    actions._edit = <EditButton />
  }
  if (only.includes('list')) {
    res.list = listFor(model, list, { ...listFields, ...actions })
  }

  return res
}
