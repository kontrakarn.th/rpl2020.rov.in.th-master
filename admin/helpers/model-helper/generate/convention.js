/* eslint react/display-name: 0 */

import lodash from 'lodash'
import React, { useMemo } from 'react'

import {
  required, Filter,
  AutocompleteInput, AutocompleteArrayInput,
  ArrayInput, SimpleFormIterator, ArrayField,
  List, Datagrid, Show, SimpleShowLayout, Edit, Create, SimpleForm,
  TextInput, NumberInput, BooleanInput, SelectInput, ReferenceInput, ReferenceArrayInput,
  TextField, NumberField, BooleanField, SelectField, ReferenceField, ReferenceArrayField, SingleFieldList, DateField, ChipField,
} from 'react-admin/esm'

import {
  getNamedType, getNullableType,
  GraphQLEnumType, GraphQLObjectType, GraphQLList, GraphQLNonNull,
} from 'graphql/type'

import { JsonField, JsonInput, DateTimeInput, DateInput, useSchema } from '../../../kit'

const FIELDS = {
  Text: (props) => <TextField {...props} />,
  Number: (props) => <NumberField {...props} />,
  Boolean: (props) => <BooleanField {...props} />,
  Date: (props) => <DateField {...props} />,
  DateTime: (props) => <DateField showTime {...props} />,
  JSON: (props) => <JsonField {...props} />,
  Select: (props) => <SelectField {...props} />,
  NestedList: ({ children, ...props }) => <ArrayField {...props}><Datagrid children={children} /></ArrayField>,
  Reference: ({ children, ...props }) =>
    <ReferenceField allowEmpty {...props} children={React.isValidElement(children) ? children : <TextField source={children} />} />,
  ReferenceArray: ({ children, ...props }) =>
    <ReferenceArrayField {...props} children={React.isValidElement(children) ? children : <SingleFieldList><ChipField source={children} /></SingleFieldList>} />,
}

const INPUTS = {
  Text: (props) => <TextInput {...props} />,
  Number: (props) => <NumberInput {...props} />,
  Boolean: (props) => <BooleanInput {...props} />,
  Date: (props) => <DateInput {...props} />,
  DateTime: (props) => <DateTimeInput {...props} />,
  JSON: (props) => <JsonInput {...props} />,
  Select: (props) => <SelectInput allowEmpty {...props} />,
  NestedList: ({ children, ...props }) => <ArrayInput fullWidth {...props}><SimpleFormIterator children={children} /></ArrayInput>,
  Reference: ({ children, ...props }) =>
    <ReferenceInput allowEmpty {...props} children={React.isValidElement(children) ? children : <AutocompleteInput optionText={children} />} />,
  ReferenceArray: ({ children, ...props }) =>
    <ReferenceArrayInput allowEmpty {...props} children={React.isValidElement(children) ? children : <AutocompleteArrayInput optionText={children} />} />,
}

const LIST_FIELDS = lodash.pick(FIELDS, 'Number', 'Text', 'Date', 'DateTime', 'Reference', 'ReferenceArray', 'Select', 'Boolean')
const FILTER_INPUTS = lodash.pick(INPUTS, 'Number', 'Text', 'Date', 'DateTime', 'Reference', 'Select', 'Boolean')

const DEFAULT_INPUTS = {
  id: false,
  createdAt: false,
  updatedAt: false,
}

const SUPPORT_OPERATOR = [
  'lte',
  'gte',
  'lt',
  'gt',
  'contain',
]

class FieldsGenerator {
  constructor (schema, repoType, defaultOptions) {
    this.schema = schema
    this.defaultOptions = defaultOptions

    switch (repoType) {
      case 'list':
        this.repo = LIST_FIELDS
        break
      case 'create':
      case 'edit':
        this.repo = INPUTS
        break
      case 'filter':
        this.repo = FILTER_INPUTS
        break
      default:
        this.repo = FIELDS
    }
  }

  displayField (model) {
    const fields = this.schema.getType(model).getFields()

    for (const name of ['name', 'label', 'title']) {
      if (fields[name]) return name
    }
    return 'id'
  }

  renderFields (model, options) {
    options = lodash.defaults({}, options, this.defaultOptions)

    const children = []
    const fields = this.schema.getType(model).getFields()

    for (const field of Object.values(fields)) {
      const { name } = field
      const { [name]: option } = options

      if (option !== undefined) delete options[name]
      if (option === false) continue

      if (React.isValidElement(option)) {
        children.push(React.cloneElement(option, { ...option.props, key: name }))
      } else {
        const child = this.renderField(field, option)

        if (child) children.push(child)
      }
    }

    for (const [name, option] of Object.entries(options)) {
      if (React.isValidElement(option)) {
        children.push(React.cloneElement(option, { ...option.props, key: name }))
      }
    }

    return children
  }

  renderFilters (model, options) {
    options = lodash.defaults({}, options, this.defaultOptions)

    const children = []
    const fields = this.schema.getType(model).getFields()

    for (const field of Object.values(fields)) {
      const render = (op) => {
        let source = field.name
        if (op) source += '_' + op

        const { [source]: option } = options
        if (option !== undefined) delete options[source]

        if (option === false) return
        if (op && !option) return

        if (React.isValidElement(option)) {
          children.push(React.cloneElement(option, { ...option.props, key: source }))
        } else {
          const child = this.renderField(field, { ...option, source })

          if (child) children.push(child)
        }
      }

      render()
      for (const op of SUPPORT_OPERATOR) render(op)
    }

    for (const [name, option] of Object.entries(options)) {
      if (React.isValidElement(option)) {
        children.push(React.cloneElement(option, { ...option.props, key: name }))
      }
    }

    if (children.length) {
      const Component = props => <Filter {...props}>{children}</Filter>
      return <Component />
    }

    return null
  }

  renderField (field, option) {
    option = { ...option }

    const props = Object.assign(this.calcProps(field, option), option)
    const component = this.repo[props.componentType]

    if (component) {
      delete props.componentType
      props.key = props.source

      return component(props)
    }
  }

  calcProps (field, option) {
    const { name: source } = field
    const props = { source }

    if (field.type instanceof GraphQLNonNull) {
      props.validate = [required()]
    }

    if (source.endsWith('Id')) {
      const reference = source[0].toUpperCase() + source.slice(1, -2)

      if (this.schema.getType(reference)) {
        return Object.assign(props, {
          reference,
          componentType: 'Reference',
          children: this.displayField(reference),
        })
      }
    } else if (source.endsWith('Ids')) {
      const reference = source[0].toUpperCase() + source.slice(1, -3)

      if (this.schema.getType(reference)) {
        return Object.assign(props, {
          reference,
          componentType: 'ReferenceArray',
          children: this.displayField(reference),
        })
      }
    }

    const type = getNamedType(field.type)

    if (type instanceof GraphQLEnumType) {
      Object.assign(props, {
        componentType: 'Select',
        choices: type.getValues().map(({ name }) => ({ id: name, name }))
      })

    } else if (type instanceof GraphQLObjectType && getNullableType(field.type) instanceof GraphQLList) {
      const { children } = option
      if (children !== undefined) delete option.children

      Object.assign(props, {
        componentType: 'NestedList',
        children: React.isValidElement(children) ? children : this.renderFields(type.name, children),
      })

    } else {
      switch (type.name) {
        case 'Int':
        case 'Int8':
        case 'Int16':
        case 'Int24':
        case 'Int32':
        case 'Int64':
        case 'UInt8':
        case 'UInt16':
        case 'UInt24':
        case 'UInt32':
        case 'UInt64':
        case 'Float':
          props.componentType = 'Number'
          break
        case 'ID':
        case 'String':
          props.componentType = 'Text'
          break
        default:
          props.componentType = type.name
      }
    }

    return props
  }
}

const generateChildren = (schema, repo, model, fieldOptions, defaultOptions) => {
  if (!schema || !schema.getType(model)) return null
  const generator = new FieldsGenerator(schema, repo, defaultOptions)
  return generator.renderFields(model, fieldOptions)
}

export const listFor = (model, options, fieldOptions) => (props) => {
  const schema = useSchema()
  const children = useMemo(() => generateChildren(schema, 'list', model, fieldOptions), [schema])
  const filters = useMemo(() => {
    if (children) {
      const { filters } = options || {}

      if (filters === false) return false
      if (React.isValidElement(filters)) return filters

      const generator = new FieldsGenerator(schema, 'filter')
      return generator.renderFilters(model, filters)
    }
  }, [schema, children])

  return children && <List {...props} {...options} filters={filters}><Datagrid children={children} /></List>
}

export const showFor = (model, options, fieldOptions) => (props) => {
  const schema = useSchema()
  const children = useMemo(() => generateChildren(schema, 'show', model, fieldOptions), [schema])

  return children && <Show {...props} {...options}><SimpleShowLayout children={children} /></Show>
}

export const createFor = (model, options, fieldOptions) => (props) => {
  const schema = useSchema()
  const children = useMemo(() => generateChildren(schema, 'create', model, fieldOptions, DEFAULT_INPUTS), [schema])

  return children && <Create {...props} {...options}><SimpleForm children={children} /></Create>
}

export const editFor = (model, options, fieldOptions) => (props) => {
  const schema = useSchema()
  const children = useMemo(() => generateChildren(schema, 'edit', model, fieldOptions, DEFAULT_INPUTS), [schema])

  return children && <Edit {...props} {...options}><SimpleForm children={children} /></Edit>
}

export default {
  listFor,
  showFor,
  editFor,
  createFor,
}
