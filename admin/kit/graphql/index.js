import { ApolloClient, InMemoryCache } from '@apollo/client'

import link from './http'

export default new ApolloClient({
  link,
  cache: new InMemoryCache(),
})
