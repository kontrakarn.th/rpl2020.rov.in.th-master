import { HttpLink } from '@apollo/client'

import settings from '../../settings'

export default new HttpLink({
  uri: settings?.graphql?.endpoint,
  credentials: 'include',
})
