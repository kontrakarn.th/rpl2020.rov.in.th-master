import React from 'react'
import { Link } from 'react-router-dom'

const ShowField = ({ source, record = {}, basePath, children, reference }) =>
  <Link to={`${reference ? '/' + reference : basePath}/${(reference && record[source]) || record.id}/show`}>
    {children && React.cloneElement(children, { source, record })}
  </Link>

ShowField.defaultProps = { addLabel: true }

export default ShowField
