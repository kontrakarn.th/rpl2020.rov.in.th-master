import { get } from 'lodash'
import { Labeled } from 'react-admin/esm'
import React, { useEffect, useRef } from 'react'

const JsonField = ({ className, wrapLabel, fullWidth, label, source, isRequired, record, options }) => {
  const el = useRef()
  const ed = useRef()
  const value = get(record, source)

  // init the json editor
  useEffect(() => {
    const editor = new JSONEditor(el.current, {
      name: source,
      modes: ['view', 'preview'],
      ...options,
    }, value)

    ed.current = editor
    return () => {
      editor.destroy()
    }
  }, [])

  // update the value
  useEffect(() => {
    ed.current?.set(value)
  }, [value])

  return wrapLabel ? (
    <Labeled {...{ label, source, isRequired, fullWidth }}>
      <div ref={el} className={className} />
    </Labeled>
  ) : <div ref={el} className={className} />
}

JsonField.defaultProps = {
  addLabel: false,
  wrapLabel: true,
  fullWidth: true,
}

export default JsonField
