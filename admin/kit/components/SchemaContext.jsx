import React, { useContext } from 'react'

export const SchemaContext = React.createContext()

export const withSchema = Component => props => {
  const schema = useContext(SchemaContext)
  return <Component {...props} schema={schema} />
}

export const withSchemaType = typeName => Component => props => {
  const schema = useContext(SchemaContext)
  const type = schema && schema.getType instanceof Function ? schema.getType(typeName) : undefined
  return <Component {...props} type={type} schema={schema} />
}

export const useSchema = () => {
  return useContext(SchemaContext)
}

export const useSchemaType = (typeName) => {
  const schema = useContext(SchemaContext)
  return schema && schema.getType instanceof Function ? schema.getType(typeName) : undefined
}
