import Ajv from 'ajv'
import React, { useEffect, useRef } from 'react'
import { Labeled, useInput } from 'react-admin/esm'

const ajv = new Ajv()

const JsonInput = (props) => {
  const el = useRef()
  const ed = useRef()
  const { source, label, fullWidth, options } = props
  const { input: { onChange }, isRequired, meta: { initial, dirty } } = useInput(props)

  const initialValue = initial ?? props.initialValue ?? null

  // init the json editor
  useEffect(() => {
    const editor = new JSONEditor(el.current, {
      name: source,
      modes: ['tree', 'form', 'code'],
      ...options,
      onChange: () => onChange(editor.get()),
    }, initialValue)

    ed.current = editor
    return () => {
      editor.destroy()
    }
  }, [])

  // update the initial value
  useEffect(() => {
    if (!dirty) ed.current?.set(initialValue)
  }, [initialValue])

  // update the schema
  const { schema, schemaRefs } = options || {}
  useEffect(() => {
    if (schema && ajv.validateSchema(schema)) {
      ed.current?.setSchema(schema, schemaRefs)
    }
  }, [schema, schemaRefs])

  return (
    <Labeled {...{ label, source, isRequired, fullWidth }}>
      <div ref={el} />
    </Labeled>
  )
}

JsonInput.defaultProps = {
  fullWidth: true,
}

export default JsonInput
