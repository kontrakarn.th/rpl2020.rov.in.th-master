import React, { useState } from 'react'

import { MobileDateTimePicker as DateTimePicker } from '@material-ui/pickers'
import { FieldTitle, useInput, ResettableTextField, InputHelperText } from 'react-admin/esm'

const noop = () => { }

const DateTimeInput = (props) => {
  const [pError, setError] = useState()
  const { id, input, isRequired, meta: { touched, error } } = useInput(props)
  const { label, source, resource, variant, margin, disabled, className, options } = props

  const errorText = error || pError
  const showError = !!(touched && errorText)

  return (
    <props.component
      id={id}
      clearable={!isRequired}
      label={<FieldTitle label={label} source={source} resource={resource} isRequired={isRequired} />}

      {...input}
      {...options}
      {...{ onError: setError, disabled }}

      renderInput={props => (
        <ResettableTextField
          {...props}
          error={showError}
          onBlur={input.onBlur}
          helperText={<InputHelperText touched={touched} error={errorText} helperText={props.helperText} />}
          {...{ variant, margin, className, onChange: props.onChange || noop }}
        />
      )}
    />
  )
}

DateTimeInput.defaultProps = {
  component: DateTimePicker,
  options: {
    ampm: false,
    showTodayButton: true,
    inputFormat: 'DD/MM/YYYY, HH:mm:ss',
  },
}

export default DateTimeInput
