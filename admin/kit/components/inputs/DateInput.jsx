import React from 'react'

import moment from 'moment'
import { MobileDatePicker as DatePicker } from '@material-ui/pickers'

import DateTimeInput from './DateTimeInput'

const DateInput = props => <DateTimeInput {...props} />

DateInput.defaultProps = {
  component: DatePicker,
  parse: date => date ? moment(date).format('YYYY-MM-DD') : null,
  options: {
    showTodayButton: true,
    inputFormat: 'DD/MM/YYYY',
  },
}

export default DateInput
