import React from 'react'

import gql from 'graphql-tag'
import { useQuery } from '@apollo/client'
import { getIntrospectionQuery, buildClientSchema } from 'graphql/utilities'

import { SchemaContext } from './SchemaContext'

const SchemaProvider = ({ children }) => {
  let schema

  const { data, loading, error } = useQuery(gql(getIntrospectionQuery()))
  if (!loading && !error && data.__schema) {
    try {
      schema = buildClientSchema(data)
    } catch { }
  }

  return (
    <SchemaContext.Provider value={schema}>
      {children}
    </SchemaContext.Provider>
  )
}

export default SchemaProvider
