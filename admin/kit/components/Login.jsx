import React from 'react'

import { useLogin } from 'react-admin/esm'

import {
  makeStyles,
  Card, CardActions,
  Avatar, Icon, Button,
} from '@material-ui/core'
import { pink } from '@material-ui/core/colors'

const useStyles = makeStyles({
  main: {
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    background: 'url(https://source.unsplash.com/random/1600x900) no-repeat',
    backgroundSize: 'cover',
  },
  card: {
    minWidth: 300,
    marginTop: '6em',
  },
  avatar: {
    width: '8rem',
    height: '8rem',
    margin: '1rem auto',
    backgroundColor: pink.A200,
  },
  icon: {
    fontSize: '5rem',
  },
  button: {
    width: '100%',
  },
})

const Login = () => {
  const login = useLogin()
  const classes = useStyles()

  return (
    <div className={classes.main}>
      <Card className={classes.card}>
        <Avatar className={classes.avatar}><Icon className={classes.icon}>lock_outlined</Icon></Avatar>
        <CardActions>
          <Button className={classes.button} variant="contained" color="primary" onClick={login}>
            Sign in
        </Button>
        </CardActions>
      </Card>
    </div>
  )
}

export default Login
