import { gql, ApolloClient, InMemoryCache } from '@apollo/client'
import { getIntrospectionQuery, buildClientSchema } from 'graphql/utilities'

import buildQueryFactory from './build-query'

export default (client) => {
  const myClient = new ApolloClient({
    link: client.link,
    cache: new InMemoryCache({
      addTypename: false,
    }),
    defaultOptions: {
      query: {
        fetchPolicy: 'network-only',
      },
    },
  })

  return new Proxy({}, {
    get: function (_, fetchType) {
      return async (resource, params, options) => {
        if (!this.builder) this.builder = await client
          .query({ query: gql(getIntrospectionQuery()) })
          .then(resp => buildClientSchema(resp.data))
          .then(buildQueryFactory)

        const { query, variables, parseResponse } = this.builder(fetchType, resource, params)

        const { definitions: [{ operation }] } = query

        return (
          operation === 'mutation'
            ? myClient.mutate({ mutation: query, variables, ...options })
            : myClient.query({ query, variables, ...options })
        ).then(parseResponse)
      }
    },
  })
}
