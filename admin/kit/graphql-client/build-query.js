import lodash from 'lodash'
import gql from 'graphql-tag'
import { GraphQLObjectType, getNamedType } from 'graphql/type'

import settings from 'admin/settings'

const GET_ONE = 'getOne'
const GET_MANY = 'getMany'
const GET_LIST = 'getList'
const GET_MANY_REFERENCE = 'getManyReference'

const CREATE = 'create'

const UPDATE = 'update'
const UPDATE_MANY = 'updateMany'

const DELETE = 'delete'
const DELETE_MANY = 'deleteMany'

const QUERY_TYPES_SET = new Set([GET_LIST, GET_MANY, GET_MANY_REFERENCE, GET_ONE])

const difference = (object, base) => {
  if (!base || !Object.keys(base).length) return object

  return lodash.transform(object, (result, value, key) => {
    if (!lodash.isEqual(value, base[key])) {
      result[key] = value
    }
  })
}

const buildVariables = (fetchType, params) => {
  switch (fetchType) {
    case GET_LIST:
      return {
        page: params.pagination.page - 1,
        perPage: params.pagination.perPage,
        sortField: params.sort.field,
        sortOrder: params.sort.order,
        filter: params.filter,
      }
    case GET_MANY:
      return {
        filter: { id_in: params.ids },
        perPage: 1000,
      }
    case GET_MANY_REFERENCE:
      return {
        page: params.pagination.page - 1,
        perPage: params.pagination.perPage || 1000,
        sortField: params.sort.field,
        sortOrder: params.sort.order,
        filter: { [params.target]: params.id },
      }
    case GET_ONE:
      return { id: params.id }
    case CREATE:
      return { data: params.data }
    case UPDATE:
      return { ids: [params.id], data: difference(params.data, params.previousData) }
    case UPDATE_MANY:
      return { ids: params.ids, data: params.data }
    case DELETE:
      return { ids: [params.id] }
    case DELETE_MANY:
      return { ids: params.ids }
  }
}

const buildResponse = (fetchType, resource) => {
  const name = operationNames[fetchType](resource)
  switch (fetchType) {
    case GET_LIST:
    case GET_MANY:
    case GET_MANY_REFERENCE:
      return ({ data: { [name]: dataForType } }) => ({
        data: dataForType.results,
        total: dataForType.total,
      })
    case UPDATE:
    case DELETE:
      return ({ data: { [name]: dataForType } }) => ({
        data: { id: dataForType?.[0] },
      })
    case GET_ONE:
    case CREATE:
    case UPDATE_MANY:
    case DELETE_MANY:
      return ({ data: { [name]: dataForType } }) => ({
        data: dataForType,
      })
  }
}

const MAX_DEPTH = 2

const buildSelectionSet = (type, options, depth = 1) => {
  type = getNamedType(type)
  if (!(type instanceof GraphQLObjectType)) return ''

  options = { ...options }

  return `{${
    Object.values(type.getFields())
      .map(field => {
        if (options[field.name] === false) return
        const childType = getNamedType(field.type)

        if (childType instanceof GraphQLObjectType) {
          if (depth < MAX_DEPTH || options[field.name]) return field.name + buildSelectionSet(childType, options[field.name], depth + 1)
        } else {
          return field.name
        }
      })
      .filter(value => value).join(' ')
    }}`
}

const buildSelectionOptions = (resource, fetchType) => {
  let opts = {}
  switch (fetchType) {
    case GET_MANY:
      opts = { total: false }
      break
  }
  return Object.assign(opts, lodash.get(settings.fetchSelectionOptions, `${resource}.${fetchType}`))
}

class OperationMap {
  static map = new Map()

  static buildArgs (args) {
    const vars = []
    const params = []
    for (const arg of args) {
      vars.push(`$${arg.name}:${arg.type.toString()}`)
      params.push(`${arg.name}:$${arg.name}`)
    }
    return [`(${vars.join(',')})`, `(${params.join(',')})`]
  }

  static get (fetchType, resource) {
    const name = operationNames[fetchType](resource)

    const cacheKey = fetchType + name
    if (this.map.has(cacheKey)) return this.map.get(cacheKey)

    const operation = (QUERY_TYPES_SET.has(fetchType) ? this.Query : this.Mutation)[name]
    if (!operation) {
      throw new Error(
        `No query or mutation matching aor fetch type ${fetchType} could be found for resource ${resource}`,
      )
    }

    const type = buildSelectionSet(operation.type, buildSelectionOptions(resource, fetchType))
    const [vars, params] = this.buildArgs(operation.args)
    const ret = gql`${QUERY_TYPES_SET.has(fetchType) ? 'query' : 'mutation'} ${name}${vars}{${name}${params} ${type}}`

    this.map.set(cacheKey, ret)
    return ret
  }
}

const operationNames = {
  [GET_LIST]: name => `list${name}`,
  [GET_ONE]: name => `get${name}`,
  [GET_MANY]: name => `list${name}`,
  [GET_MANY_REFERENCE]: name => `list${name}`,
  [CREATE]: name => `create${name}`,
  [UPDATE]: name => `update${name}`,
  [DELETE]: name => `remove${name}`,
  [UPDATE_MANY]: name => `update${name}`,
  [DELETE_MANY]: name => `remove${name}`,
}

export default (schema) => {
  OperationMap.Query = schema.getQueryType().getFields()
  OperationMap.Mutation = schema.getMutationType().getFields()
  return (fetchType, resource, params) => {
    return {
      query: OperationMap.get(fetchType, resource),
      variables: buildVariables(fetchType, params),
      parseResponse: buildResponse(fetchType, resource),
    }
  }
}
