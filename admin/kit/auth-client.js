import settings from '../settings'

class MyAuthProvider {
  constructor (client) {
    this.client = client
  }

  async login () {
    window.location.replace(settings.auth.login)
  }

  async logout () {
    await fetch(settings.auth.logout, { credentials: 'include' })
  }

  async checkAuth () {
    if (this.isLoggedIn) return

    try {
      const { data: { currentAdmin } } = await this.client.query({ query: require('../gql/queries/getCurrentAdmin.gql') })
      if (!currentAdmin) throw Error('Unauthorized')
      this.isLoggedIn = true
    } catch (err) {
      err.status = 401
      throw err
    }
  }

  async checkError (params) {
    const { status } = params
    if (status === 401 || status === 403) {
      this.isLoggedIn = false
      throw Error('auth error')
    }
  }

  async getPermissions (params) { }
}

export default (client) => new MyAuthProvider(client)
