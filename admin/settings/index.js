export default {
  graphql: {
    endpoint: '/admin/graphql',
  },
  auth: {
    login: '/connect/google',
    logout: '/connect/google/logout',
  },
  fetchSelectionOptions: {
    /* Refactor result selection set */
    Admin: {
      getList: {
        results: {
          groups: true,
        },
      }
    },
    Player: {
      getList: {
        results: {
          roles: {
            hot: false,
          },
        },
      }
    },
  },
}
