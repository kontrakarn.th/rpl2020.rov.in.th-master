import React from 'react'

import {
  List,
  AppBar,
  DateField,
  NumberField,
  SingleFieldList,
  SimpleForm, Filter,
  SelectInput, SelectField,
  TextField, AutocompleteInput,
  ReferenceField, ReferenceArrayField,
  ReferenceInput, ReferenceArrayInput,
} from 'react-admin/esm'

import Empty from './component/Empty'
import UserMenu from './layout/UserMenu'
import ListActions from './component/ListActions'

TextField.defaultProps = {
  ...TextField.defaultProps,
  emptyText: '[NULL]',
}

SelectField.defaultProps = {
  ...SelectField.defaultProps,
  emptyText: '[NULL]',
}

SelectInput.defaultProps = {
  ...SelectInput.defaultProps,
  emptyText: '[NULL]',
}

AutocompleteInput.defaultProps = {
  ...AutocompleteInput.defaultProps,
  emptyText: '[NULL]',
}

DateField.defaultProps = {
  ...DateField.defaultProps,
  locales: 'en-GB',
  options: { hour12: false },
}

List.defaultProps = {
  ...List.defaultProps,
  empty: <Empty />,
  actions: <ListActions />,
  sort: { field: 'id', order: 'DESC' },
}

AppBar.defaultProps = {
  ...AppBar.defaultProps,
  userMenu: <UserMenu />,
}

NumberField.defaultProps = {
  ...NumberField.defaultProps,
  options: { useGrouping: false },
}

SimpleForm.defaultProps = {
  ...SimpleForm.defaultProps,
  margin: 'normal',
  variant: 'standard',
}

Filter.defaultProps = {
  ...Filter.defaultProps,
  margin: 'normal',
  variant: 'standard',
}

ReferenceInput.defaultProps = {
  ...ReferenceInput.defaultProps,
  filterToQuery: text => ({ name_contain: text }),
}

ReferenceArrayInput.defaultProps = {
  ...ReferenceArrayInput.defaultProps,
  filterToQuery: text => ({ name_contain: text }),
}

ReferenceField.defaultProps = {
  ...ReferenceField.defaultProps,
  link: 'show',
}

ReferenceArrayField.defaultProps = {
  ...ReferenceArrayField.defaultProps,
  link: 'show',
}

SingleFieldList.defaultProps = {
  ...SingleFieldList.defaultProps,
  linkType: 'show',
}
