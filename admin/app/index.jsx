import './app.scss'
import './patch.jsx'

import React from 'react'
import { Admin } from 'react-admin/esm'
import MomentUtils from '@date-io/moment'
import { useApolloClient } from '@apollo/client'
import { LocalizationProvider } from '@material-ui/pickers'

import { SchemaProvider } from '../kit'
import buildAuthClient from '../kit/auth-client'
import buildDataProvider from '../kit/graphql-client'

import resources from './resource'

import Lazy from './component/Lazy'

const Dashboard = Lazy({ loader: () => import('./dashboard') })
const Login = Lazy({ loader: () => import('../kit/components/Login') })

const App = () => {
  const client = useApolloClient()
  return (
    <LocalizationProvider dateAdapter={MomentUtils}>
      <SchemaProvider>
        <Admin
          title="Admin Site"
          loginPage={Login}
          dashboard={Dashboard}
          authProvider={buildAuthClient(client)}
          dataProvider={buildDataProvider(client)}
        >
          {resources()}
        </Admin>
      </SchemaProvider>
    </LocalizationProvider>
  )
}

export default App
