import React, { Suspense } from 'react'

import DefaultLoading from './Loading'

const Lazy = ({ loader, loading: LoadingComponent = DefaultLoading }) => {
  const Component = React.lazy(loader)
  return () => <Suspense fallback={<LoadingComponent />}><Component /></Suspense>
}

export default Lazy
