import React from 'react'

import inflection from 'inflection'
import { makeStyles } from '@material-ui/styles'
import Icon from '@material-ui/icons/LocationCity'
import Typography from '@material-ui/core/Typography'

import { useTranslate, CreateButton } from 'react-admin/esm'

import ImportButton from './ImportButton'

const useStyles = makeStyles(
  {
    message: {
      textAlign: 'center',
      opacity: 0.5,
      margin: '0 1em',
    },
    icon: {
      width: '9em',
      height: '9em',
      marginTop: '2em',
    },
    toolbar: {
      textAlign: 'center',
      marginTop: '2em',
    },
  },
  { name: 'RaEmpty' }
)

const Empty = ({ resource, basePath }) => {
  const classes = useStyles()
  const translate = useTranslate()

  const resourceName = inflection.humanize(
    translate(`resources.${resource}.name`, {
      smart_count: 0,
      _: inflection.pluralize(resource),
    }),
    true
  )

  const inviteMessage = translate('ra.page.invite')
  const emptyMessage = translate('ra.page.empty', { name: resourceName })

  return (
    <>
      <div className={classes.message}>
        <Icon className={classes.icon} />
        <Typography variant="h4" paragraph>
          {translate(`resources.${resource}.empty`, { _: emptyMessage })}
        </Typography>
        <Typography variant="body1">
          {translate(`resources.${resource}.invite`, { _: inviteMessage })}
        </Typography>
      </div>
      <div className={classes.toolbar}>
        <CreateButton variant="contained" basePath={basePath} />
        &nbsp;&nbsp;&nbsp;
        <ImportButton variant="contained" resource={resource} />
      </div>
    </>
  )
}

export default Empty
