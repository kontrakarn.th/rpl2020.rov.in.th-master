import React from 'react'
import { CheckboxGroupInput } from 'react-admin/esm'

import { useSchemaType } from '../../kit'

export const operationFormat = value => [0, 1, 2, 3].filter(cur => value & 1 << cur)
export const operationParse = value => value.reduce((acc, cur) => acc | 1 << cur, 0)

const OperationInput = (props) => {
  const type = useSchemaType('OperationEnum')
  const choices = type && type.getValues instanceof Function ? type.getValues().map(({ name }, id) => ({ id, name })) : []
  return <CheckboxGroupInput {...props} choices={choices} />
}

OperationInput.defaultProps = {
  source: 'allow', label: 'Allow', fullWidth: true,
  parse: operationParse,
  format: operationFormat,
}

export default OperationInput
