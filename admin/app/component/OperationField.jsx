import React from 'react'
import Chip from '@material-ui/core/Chip'

import { useSchemaType } from '../../kit'
import { operationFormat } from './OperationInput'

const OperationField = ({ record, source }) => {
  const type = useSchemaType('OperationEnum')
  const choices = type && type.getValues instanceof Function ? type.getValues().map(item => item.name) : []
  return operationFormat(record[source]).map(value => <Chip key={value} label={choices[value]} style={{ margin: '0.25rem' }} />)
}

OperationField.defaultProps = { source: 'allow' }

export default OperationField
