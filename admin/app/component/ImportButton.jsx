import React, { useState, useRef, useCallback } from 'react'

import {
  getNullableType,
  GraphQLObjectType, GraphQLList, GraphQLScalarType,
} from 'graphql/type'

import csv from 'papaparse'
import { Button, useDataProvider, useNotify, useRefresh, useCreate } from 'react-admin/esm'

import { useSchemaType } from '../../kit'

const BULK_SIZE = 1000

export async function* csvChunks (file, size = BULK_SIZE, options) {
  const stream = new ReadableStream({
    start (controller) {
      csv.parse(file, {
        header: true,
        dynamicTyping: true,
        skipEmptyLines: true,
        ...options,
        step: async ({ data, errors }, parser) => {
          this.parser = parser

          if (errors.length) {
            parser.abort()
            controller.error(errors[0])
          }

          controller.enqueue(data)
        },
        complete: () => {
          controller.close()
        },
        error: (err) => {
          controller.error(err)
        }
      })
    },
    pull (controller) {
    },
    cancel (reason) {
      this.parser?.abort(reason)
    },
  })

  const reader = stream.getReader()

  let buf = []

  while (true) {
    const { done, value } = await reader.read()
    if (done) {
      break
    }
    else {
      if (buf.length < size) {
        buf.push(value)
      } else {
        yield buf
        buf = [value]
      }
    }
  }

  if (buf.length) yield buf
}

export const generateTransform = (resourceMeta) => {
  const jsonFields = []
  for (const { name, type } of Object.values(resourceMeta?.getFields())) {
    const nullableType = getNullableType(type)
    if (
      nullableType instanceof GraphQLList
      || nullableType instanceof GraphQLObjectType
      || (nullableType instanceof GraphQLScalarType && nullableType.name === 'JSON')
    ) jsonFields.push(name)
  }

  return (row) => {
    for (const field of jsonFields) {
      const { [field]: value } = row
      if (value) {
        if (value instanceof Date) {
          row[field] = value.toISOString()
        } else {
          try {
            row[field] = JSON.parse(row[field])
          } catch { }
        }
      }
    }
    return row
  }
}

const ImportButton = ({ resource, options, ...rest }) => {
  const [loading, setLoading] = useState(false)

  const file = useRef()
  const notify = useNotify()
  const refresh = useRefresh()
  const dataProvider = useDataProvider()
  const resourceMeta = useSchemaType(resource)

  const onFileChange = useCallback(async ({ target }) => {
    let error
    let numCreated = 0

    try {
      setLoading(true)

      const transformRow = generateTransform(resourceMeta)

      for await (const rows of csvChunks(target.files[0], BULK_SIZE, options)) {
        const data = rows.map(row => transformRow(row))
        await dataProvider.create(resource, { data })

        numCreated += rows.length
      }

    } catch (err) {
      error = err
      console.error(err)
    } finally {
      target.value = ''

      if (numCreated) {
        refresh()
        notify(`${numCreated} ${resource} rows had been imported`)
      }

      if (error) notify(error.message, 'warning')

      setLoading(false)
    }
  }, [dataProvider, resourceMeta, options])

  const onClick = useCallback(evt => file.current.click(), [file])

  return (
    <Button
      label="Import"
      color="primary"
      disabled={loading}
      {...rest}
      onClick={onClick}
    >
      <span style={{ height: '1em', lineHeight: '1em' }}>
        <input type="file" style={{ display: 'none' }} accept=".csv" ref={file} onChange={onFileChange} />
        <i className="fas fa-angle-up" />
      </span>
    </Button>
  )
}

export default ImportButton
