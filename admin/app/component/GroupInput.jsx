import React, { useState, useEffect } from 'react'
import {
  useInput,
  AutocompleteArrayInput,
  useReferenceArrayInputController,
} from 'react-admin/esm'

const GroupInput = ({
  basePath, resource, source, record,
  initialValue, parse, format, validate,
  filter, filterToQuery, perPage, sort, reference,
  ...props
}) => {
  const [value, setValue] = useState([])
  const { id, input: oldInput, meta, isRequired } = useInput({ source, initialValue, parse, format, validate })

  useEffect(() => {
    const { value: newValue } = oldInput
    if (Array.isArray(newValue) && value.length !== newValue.length || value.some((a, i) => a !== newValue[i])) {
      setValue(newValue)
    }
  }, [oldInput.value])

  const input = { ...oldInput, value }

  const { choices, setFilter, setSort, setPagination, error } = useReferenceArrayInputController({
    input,
    filter, filterToQuery, perPage, sort,
    basePath, reference, source, resource,
  })

  return React.createElement(AutocompleteArrayInput, {
    source, resource, basePath,
    ...props,
    translateChoice: false,
    limitChoicesToValue: true,
    id, meta, isRequired, input,
    choices, setFilter, setSort, setPagination, error,
  })
}

GroupInput.defaultProps = {
  source: 'groups',
  reference: 'Group',
  filterToQuery: text => ({ name_contain: text }),
  format: arr => Array.isArray(arr) && arr.length ? arr.map(row => row.groupId) : arr,
  parse: arr => Array.isArray(arr) && arr.length ? arr.map(groupId => ({ groupId })) : arr,
}

export default GroupInput
