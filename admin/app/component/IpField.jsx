import React from 'react'
import { get } from 'lodash'

export const intToIp = (int = 0) => `${int >> 24 & 255}.${int >> 16 & 255}.${int >> 8 & 255}.${int & 255}`

const IpField = ({ record, source }) => {
  const ip = get(record, source)
  if (!ip) return ''
  return <>{ip >> 24 & 255}.{ip >> 16 & 255}.{ip >> 8 & 255}.{ip & 255}</>
}

IpField.defaultProps = {
  addLabel: true,
}

export default IpField
