import React, { useState, useCallback } from 'react'

import csv from 'papaparse'
import streamSaver from 'streamsaver'
import { Button, useDataProvider, useNotify } from 'react-admin/esm'

const tranformRows = (rows) => {
  let idx = 0
  const arr = new Array(rows.length)

  for (const row of rows) {
    const tmp = {}

    for (const [key, value] of Object.entries(row)) {
      switch (typeof value) {
        case 'array':
        case 'object':
          if (value) {
            tmp[key] = JSON.stringify(value)
            break
          }
        default:
          tmp[key] = value
      }
    }

    arr[idx] = tmp
    idx += 1
  }

  return arr
}

const ExportButton = ({ resource, sort, filter, perPage, disabled, ...rest }) => {
  const [loading, setLoading] = useState(false)

  const notify = useNotify()
  const { getList } = useDataProvider()

  const onClick = useCallback(async ({ target }) => {
    let error
    let numRows = 0
    const encoder = new TextEncoder()

    try {
      setLoading(true)

      const readableStream = new ReadableStream({
        page: 1,
        numPage: 0,
        async start (ctrl) {
          const { data, total } = await getList(resource, { sort, filter, pagination: { page: 1, perPage } })

          if (!total || !data.length) return ctrl.close()

          ctrl.enqueue(encoder.encode(csv.unparse(tranformRows(data))))

          const numPage = Math.ceil(total / perPage)
          if (numPage > 1) {
            this.numPage = numPage
          } else {
            ctrl.close()
          }

          numRows = total
        },
        async pull (ctrl) {
          this.page += 1
          if (this.page <= this.numPage) {
            const { data, total } = await getList(resource, { sort, filter, pagination: { page: this.page, perPage } })

            if (!total || !data.length) return ctrl.close()

            ctrl.enqueue(encoder.encode('\r\n'))
            ctrl.enqueue(encoder.encode(csv.unparse(tranformRows(data), {
              header: false,
            })))

          } else {
            ctrl.close()
          }
        },
      })

      // const readableStream = new Blob(['StreamSaver is awesome']).stream()
      const fileStream = streamSaver.createWriteStream(`${resource}.csv`)

      // more optimized pipe version (Safari may have pipeTo but it's useless without the WritableStream)
      if (window.WritableStream && readableStream.pipeTo) {
        await readableStream.pipeTo(fileStream)
      } else {
        const writer = fileStream.getWriter()
        const reader = readableStream.getReader()
        const pump = () => reader.read().then(res => res.done ? writer.close() : writer.write(res.value).then(pump))

        await pump()
      }
    } catch (err) {
      error = err
      console.error(err)
    } finally {
      target.value = ''

      if (numRows) notify(`${numRows} ${resource} rows had been exported`)

      if (error) notify(error.message, 'warning')

      setLoading(false)
    }
  }, [resource, perPage, sort, filter])

  return (
    <Button
      label="Export"
      color="primary"
      disabled={disabled || loading}
      {...rest}
      onClick={onClick}
    >
      <span style={{ height: '1em', lineHeight: '1em' }}>
        <i className="fas fa-angle-down" />
      </span>
    </Button>
  )
}

ExportButton.defaultProps = {
  perPage: 4096,
}

export default ExportButton
