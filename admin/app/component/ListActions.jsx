import React from 'react'

import Toolbar from '@material-ui/core/Toolbar'
import { CreateButton } from 'react-admin/esm'

import ImportButton from './ImportButton'
import ExportButton from './ExportButton'
import BatchUpdateButton from './BatchUpdateButton'

const ListActions = ({
  total, className,
  resource, basePath, hasCreate,
  filters, filterValues, showFilter, displayedFilters, currentSort,
}) => {
  return (
    <Toolbar className={className}>
      {filters && React.cloneElement(filters, { resource, showFilter, displayedFilters, filterValues, context: 'button' })}
      {hasCreate && <CreateButton basePath={basePath} />}
      {hasCreate && <ImportButton resource={resource} />}
      <BatchUpdateButton resource={resource} />
      <ExportButton disabled={total === 0} resource={resource} sort={currentSort} filter={filterValues} />
    </Toolbar>
  )
}

export default ListActions
