import React, { useState, useRef, useCallback, useMemo } from 'react'

import {
  getNullableType,
  GraphQLObjectType, GraphQLList, GraphQLScalarType,
} from 'graphql/type'

import { Button, useDataProvider, useNotify, useRefresh } from 'react-admin/esm'

import { useSchemaType } from '../../kit'

import { csvChunks, generateTransform } from './ImportButton'

const BULK_SIZE = 1000

const ImportButton = ({ resource, options, ...rest }) => {
  const [loading, setLoading] = useState(false)

  const file = useRef()
  const notify = useNotify()
  const refresh = useRefresh()
  const dataProvider = useDataProvider()
  const resourceMeta = useSchemaType(resource)

  const onFileChange = useCallback(async ({ target }) => {
    let error
    let numUpdated = 0

    try {
      setLoading(true)

      const transformRow = generateTransform(resourceMeta)
      
      for await (const rows of csvChunks(target.files[0], BULK_SIZE, options)) {
        const ids = rows.map(row => row.id)
        const data = rows.map(row => transformRow(row))

        await dataProvider.updateMany(resource, { ids, data })

        numUpdated += rows.length
      }

    } catch (err) {
      error = err
      console.error(err)
    } finally {
      target.value = ''

      if (numUpdated) {
        refresh()
        notify(`${numUpdated} ${resource} rows had been updated`)
      }

      if (error) notify(error.message, 'warning')

      setLoading(false)
    }
  }, [dataProvider, resourceMeta, options])

  const onClick = useCallback(evt => file.current.click(), [file])

  return (
    <Button
      label="Update"
      color="primary"
      disabled={loading}
      {...rest}
      onClick={onClick}
    >
      <span style={{ height: '1em', lineHeight: '1em' }}>
        <input type="file" style={{ display: 'none' }} accept=".csv" ref={file} onChange={onFileChange} />
        <i className="fas fa-angle-up" />
      </span>
    </Button>
  )
}

export default ImportButton
