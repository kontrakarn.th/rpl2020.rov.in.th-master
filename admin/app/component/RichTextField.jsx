import React, { useRef, useEffect } from 'react'

import { get } from 'lodash'
import { Labeled } from 'react-admin/esm'

const RichTextField = ({ wrapLabel, fullWidth, label, source, isRequired, record, className }) => {
  const el = useRef()
  const ed = useRef()
  const value = get(record, source)

  useEffect(() => {
    tinymce.init({
      target: el.current,
      menubar: false,
      toolbar: false,
      statusbar: false,
      plugins: 'autoresize',
      min_height: 60,
      max_height: 800,

      setup: editor => {
        editor.mode.set('readonly')
      },
      init_instance_callback: (editor) => {
        ed.current = editor
        editor.resetContent(value)
      },
    })

    return () => {
      tinymce.remove(ed.current)
    }
  }, [])

  useEffect(() => {
    ed.current?.resetContent(value)
  }, [value])

  return !wrapLabel
    ? <textarea ref={el} className={className} />
    : <Labeled {...{ label, source, isRequired, fullWidth }}><textarea ref={el} className={className} /></Labeled>
}

RichTextField.defaultProps = {
  addLabel: false,
  wrapLabel: true,
  fullWidth: true,
}

export default RichTextField
