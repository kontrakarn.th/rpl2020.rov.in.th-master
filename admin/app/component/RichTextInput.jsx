import React, { useEffect, useRef } from 'react'

import { Labeled, useInput } from 'react-admin/esm'

let RichTextInput = (props) => {
  const { label, source, fullWidth } = props
  const { input, meta: { initial }, isRequired } = useInput(props)

  const el = useRef()
  const ed = useRef()

  useEffect(() => {
    tinymce.init({
      height: '60vh',
      target: el.current,
      menubar: 'edit insert format table tools',
      plugins: 'autoresize table link image media lists',
      toolbar: 'undo redo | formatselect | alignleft aligncenter alignright alignjustify | forecolor backcolor | bold italic underline blockquote | outdent indent | numlist bullist checklist | link image media | removeformat',
      max_height: 660,
      default_link_target: '_blank',

      setup: editor => { },
      init_instance_callback: (editor) => {
        ed.current = editor
        editor.resetContent(initial)

        editor.on('change keyup setcontent', (e) => {
          const newContent = editor.getContent({ format: 'raw' })

          if (newContent !== input.value) {
            input.onChange(newContent)
          }
        })
      },
    })

    return () => {
      tinymce.remove(ed.current)
    }
  }, [])

  useEffect(() => {
    ed.current?.resetContent(initial)
  }, [initial])

  return (
    <Labeled {...{ label, source, isRequired, fullWidth }}>
      <textarea ref={el} />
    </Labeled>
  )
}

RichTextInput.defaultProps = {
  fullWidth: true,
}

export default RichTextInput
