import React from 'react'
import lodash from 'lodash'

import { ReferenceArrayField, SingleFieldList, ChipField } from 'react-admin/esm'

const GroupField = ({ refSource, refField, record, source, ...props }) => {
  const value = lodash.get(record, source)
  const refRecord = lodash.set({}, source, Array.isArray(value) ? value.map(row => lodash.get(row, refSource)) : [])

  return (
    <ReferenceArrayField {...props} source={source} record={refRecord}>
      <SingleFieldList>
        <ChipField source={refField} />
      </SingleFieldList>
    </ReferenceArrayField>
  )
}

GroupField.defaultProps = {
  addLabel: true,
  source: 'groups',
  reference: 'Group',
  refSource: 'groupId',
  refField: 'name',
}

export default GroupField
