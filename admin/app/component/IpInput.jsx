import React from 'react'
import { TextInput, required } from 'react-admin/esm'

import { intToIp } from './IpField'

const checkIp = value => {
  if (Number.isInteger(value)) return undefined
  try {
    const arr = value.split('.').map(v => parseInt(v))
    if (arr.length !== 4) throw Error()
    for (const i of arr) {
      if (!(i >= 0 && i <= 255)) throw Error()
    }
  } catch {
    return 'Invalid IPv4'
  }
}

const ipToInt = (ip = '') => {
  const view = new DataView(Uint8Array.from(ip.split('.').map(v => parseInt(v))).buffer)
  return view.getUint32(0, false)
}

const format = ip => {
  if (Number.isSafeInteger(ip)) return intToIp(ip)
  return ip
}

const parse = ip => {
  if (!checkIp(ip)) return ipToInt(ip)
  return ip
}


const IpInput = (props) => (
  <TextInput {...props} />
)

IpInput.defaultProps = {
  format, parse,
  validate: [checkIp],
}

export default IpInput
