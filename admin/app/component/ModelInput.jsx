import React from 'react'
import { SelectInput } from 'react-admin/esm'

import { useSchemaType } from '../../kit'

export const ModelInput = props => {
  const type = useSchemaType('ModelEnum')
  const choices = type && type.getValues instanceof Function ? type.getValues() : []
  return <SelectInput {...props} choices={choices} optionValue="value" />
}

ModelInput.defaultProps = { source: 'model', label: 'Model' }

export default ModelInput
