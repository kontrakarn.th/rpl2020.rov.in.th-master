import React from 'react'
import { gql, useQuery } from '@apollo/client'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'

import GET_CURRENT_ADMIN from 'admin/gql/queries/getCurrentAdmin.gql'

/* // If you need chart for you project, you can use chartjs and react-chart-js2
 * // Example:
 *
 * import { Line } from 'react-chartjs-2'
 *
 * @graphql(require('admin/gql/queries/getCharts.gql'), {
 *   props: ({data: {rpChart, usersChart}}) => ({rpChart, usersChart}) // Return chart data as JSON
 * })
 * class ChartArea extends Component {
 *   render () {
 *     const {rpChart, usersChart} = this.props
 *     return (
 *       <CardMedia>
 *         <Tabs>
 *           <Tab label="RP">
 *             <div>
 *               {rpChart && <Line data={JSON.parse(JSON.stringify(rpChart))} options={{responsive: true}}/>}
 *             </div>
 *           </Tab>
 *           <Tab label="New users">
 *             <div>
 *               {usersChart && <Line data={JSON.parse(JSON.stringify(usersChart))} options={{responsive: true}}/>}
 *             </div>
 *           </Tab>
 *         </Tabs>
 *       </CardMedia>
 *     )
 *   }
 * }
 */

const REGION_POINTS = gql`{regionPoints}`

const Dashboard = () => {
  const { data: { currentAdmin } = {} } = useQuery(GET_CURRENT_ADMIN)
  const { data: { regionPoints } = {} } = useQuery(REGION_POINTS)
  return (
    <Card>
      <CardContent>
        Welcome, {currentAdmin && currentAdmin.name}
        <p>
          Region total points: <b>{regionPoints} points</b>
        </p>
      </CardContent>
    </Card>
  )
}

export default Dashboard
