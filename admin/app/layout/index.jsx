import React from 'react'
import { Layout } from 'react-admin/esm'

import AppBar from './AppBar'

export default props => <Layout {...props} appBar={AppBar} />
