import React from 'react'
import { useQuery } from '@apollo/client'

import Avatar from '@material-ui/core/Avatar'
import { makeStyles } from '@material-ui/core/styles'
import { UserMenu, MenuItemLink } from 'react-admin/esm'

import GET_CURRENT_ADMIN from 'admin/gql/queries/getCurrentAdmin.gql'

const useStyles = makeStyles({
  avatar: {
    width: '1.25em',
    height: '1.25em',
  },
})

export default (props) => {
  const classes = useStyles()
  const { data, loading, error } = useQuery(GET_CURRENT_ADMIN)

  if (loading || error) return null

  const { currentAdmin: { id, name, avatar } } = data
  const icon = <Avatar className={classes.avatar} src={avatar} />

  return (
    <UserMenu {...props} label={name} icon={icon}>
      <MenuItemLink to={`/Admin/${id}/show`} primaryText="Profile" leftIcon={icon} />
    </UserMenu>
  )
}
