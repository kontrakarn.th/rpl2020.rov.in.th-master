import React from 'react'
import { AppBar } from 'react-admin/esm'

import UserMenu from './UserMenu'

export default props => <AppBar {...props} userMenu={<UserMenu />} />
