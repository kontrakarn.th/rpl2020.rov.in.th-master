import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('Example', {
  listFields: {},
  showFields: {},
  createInputs: {},
  editInputs: {},
  icon: 'list',
})
