import React from 'react'

import get from 'lodash/get'
import gql from 'graphql-tag'
import { useQuery } from '@apollo/client'
import Icon from '@material-ui/core/Icon'
import Chip from '@material-ui/core/Chip'

import {
  CheckboxGroupInput,
  Button, BulkDeleteButton,
  ReferenceArrayInput, AutocompleteArrayInput,
  ReferenceArrayField, SingleFieldList, ChipField,
  useNotify, useRefresh, useUnselectAll, useCreate,
} from 'react-admin/esm'

import JsonField from '../../kit/components/fields/JsonField'

import { generate } from 'admin/helpers/model-helper'

const effectParse = value => value.reduce((acc, cur) => acc | 1 << cur, 0)
const effectFormat = value => [0, 1, 2, 3, 4, 5, 6, 7].filter(cur => value & 1 << cur)
const ROLE_QUERY = gql`{
  listRole {
    results {
      id
      name
    }
  }
}`

const EffectInput = (props) => {
  const { data } = useQuery(ROLE_QUERY)
  const choices = data?.listRole?.results ?? []
  return <CheckboxGroupInput {...props} choices={choices} />
}

EffectInput.defaultProps = {
  fullWidth: true,
  source: 'effect',
  parse: effectParse,
  format: effectFormat,
}

const EffectField = ({ record, source }) => {
  const { data } = useQuery(ROLE_QUERY)
  const choices = data?.listRole?.results

  if (!choices?.length) return null
  return effectFormat(get(record, source))?.map(idx => <Chip key={idx} label={choices?.[idx]?.name} style={{ margin: '0.25rem' }} />)
}

const ExecuteButton = ({ resource, selectedIds }) => {
  const notify = useNotify()
  const refresh = useRefresh()
  const unselectAll = useUnselectAll()

  const [updateMany, { loading }] = useCreate(
    'Job',
    {
      name: 'executeWeek',
      data: {
        weekId: selectedIds?.[0],
      }
    },
    {
      onSuccess: () => {
        refresh()
        unselectAll(resource)
        notify('The week execute task has been queued')
      },
      onFailure: error => notify(`Error: ${error.message}`, 'warning'),
    }
  )

  if (selectedIds?.length !== 1) return null

  return (
    <Button
      label="execute"
      disabled={loading}
      onClick={updateMany}
    >
      <Icon>directions_run</Icon>
    </Button>
  );
};

const BulkActionButtons = props => (
  <>
    <ExecuteButton {...props} />
    <BulkDeleteButton {...props} />
  </>
)

const PlayersField = (props) => {
  return (
    <ReferenceArrayField {...props} >
      <SingleFieldList>
        <ChipField source="name" />
      </SingleFieldList>
    </ReferenceArrayField>
  )
}

PlayersField.defaultProps = {
  addLabel: true,
  source: 'livetime',
  reference: 'Player',
}

const PlayersInput = (props) => {
  return (
    <ReferenceArrayInput {...props} >
      <AutocompleteArrayInput />
    </ReferenceArrayInput>
  )
}

PlayersInput.defaultProps = {
  source: 'livetime',
  reference: 'Player',
}

export default generate('Week', {
  listFields: {
    playerIds: { label: 'Week Results' },
    mvps: <JsonField source="mvps" {...{ wrapLabel: false, className: 'no-border', options: { mainMenuBar: false, navigationBar: false, maxVisibleChilds: 5 } }} />,
    livetime: <PlayersField source="livetime" />,
    createdAt: false,
  },
  showFields: {
    playerIds: { label: 'Week Results' },
    livetime: <PlayersField source="livetime" />,
  },
  createInputs: {
    status: false,
    playerIds: false,
    mvps: false,
    livetime: <PlayersInput label="Livetime" />,
  },
  editInputs: {
    status: { disabled: true },
    playerIds: { label: 'Week Results' },
    mvps: {
      initialValue: [],
      options: {
        schema: {
          type: 'array',
          items: {
            type: 'number',
            minimum: 0,
          },
          maxItems: 5,
        },
      },
    },
    livetime: <PlayersInput label="Livetime" />,
  },
  icon: 'fas fa-calendar-alt',
  options: {
    list: {
      bulkActionButtons: <BulkActionButtons />,
    },
  },
})
