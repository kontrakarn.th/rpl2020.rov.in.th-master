import React from 'react'

import { generate } from 'admin/helpers/model-helper'

const CHANCE = {
  label: 'Chance (per 1 million)',
}

export default generate('Lucky', {
  listFields: {
    chance: CHANCE,
  },
  showFields: {
    chance: CHANCE,
  },
  createInputs: {
    chance: CHANCE,
    current: false,
  },
  editInputs: {
    chance: CHANCE,
    current: { disabled: true },
  },
  icon: 'fas fa-medal',
})
