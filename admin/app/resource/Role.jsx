import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('Role', {
  listFields: {},
  showFields: {},
  createInputs: {},
  editInputs: {},
  icon: 'fas fa-road',
  options: {
    list: {
      sort: { field: 'id', order: 'ASC' },
    },
  },
})
