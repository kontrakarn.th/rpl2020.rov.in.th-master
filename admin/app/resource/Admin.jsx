import React from 'react'

import {
  email,
  required,
  ImageField,
  ReferenceInput,
  AutocompleteInput,
} from 'react-admin/esm'

import { generate } from 'admin/helpers/model-helper'

import ModelInput from '../component/ModelInput'
import GroupInput from '../component/GroupInput'
import GroupField from '../component/GroupField'
import OperationField from '../component/OperationField'
import OperationInput from '../component/OperationInput'

const INPUTS = {
  email: {
    validate: [required(), email()],
  },
  avatar: {
    fullWidth: true,
  },
  groups: <GroupInput />,
  permissions: {
    children: {
      model: <ModelInput />,
      allow: <OperationInput />,
    },
  },
}

export default generate('Admin', {
  listFields: {
    avatar: false,
    createdAt: <GroupField source="groups" sortable={false} />,
  },
  showFields: {
    avatar: <ImageField source="avatar" />,
    groups: <GroupField source="groups" />,
    permissions: {
      children: {
        allow: <OperationField />,
        createdAt: true,
        updatedAt: true,
      },
    },
  },
  editInputs: INPUTS,
  createInputs: INPUTS,
  icon: 'fas fa-user-tie',
  options: {
    list: {
      filters: {
        email_contain: true,
        name: false,
        name_contain: true,
        avatar: false,
        email: (
          <ReferenceInput source="groups.groupId" reference="Group" label="Group" allowEmpty >
            <AutocompleteInput optionText="name" />
          </ReferenceInput>
        ),
      },
    },
  },
})
