import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('Refer', {
  listFields: {},
  showFields: {},
  createInputs: {},
  editInputs: {},
  icon: 'fas fa-link',
})
