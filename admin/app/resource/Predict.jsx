import React from 'react'

import { generate } from 'admin/helpers/model-helper'

const INPUT = {
  playerIds: {
    label: 'Players',
    allowDuplicates: true,
  },
  tierIds: {
    label: 'Tiers',
    allowDuplicates: true,
  },
}

export default generate('Predict', {
  listFields: {
    id: false,
    tierIds: false,
  },
  showFields: {
    id: false,
  },
  createInputs: INPUT,
  editInputs: INPUT,
  icon: 'fas fa-magic',
  options: {
    list: {
      sort: {},
    },
  },
})
