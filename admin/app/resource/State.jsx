import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('State', {
  listFields: {
    createdAt: false,
    updatedAt: false,
  },
  showFields: {},
  createInputs: {
    id: true,
  },
  editInputs: {},
  icon: 'fas fa-chess-knight',
})
