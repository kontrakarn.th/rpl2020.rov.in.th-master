import React, { useMemo } from 'react'
import {
  FunctionField, Filter, Button, BulkDeleteButton, SelectInput,
  required, useInput, useUpdateMany, useNotify, useRefresh, useUnselectAll,
} from 'react-admin/esm'

import Icon from '@material-ui/core/Icon'
import TextButton from '@material-ui/core/Button'

import { useSchemaType } from '../../kit'
import { generate } from 'admin/helpers/model-helper'
import JsonField from '../../kit/components/fields/JsonField'

const JOB_STATUS = [
  'active',
  'completed',
  'delayed',
  'failed',
  'waiting',
]

const StatusSelect = ({ id, name, initialValue, source, validate }) => {
  const { input: { value, onChange } } = useInput({ id, name, initialValue, source, validate })

  return JOB_STATUS.map(state =>
    <TextButton key={state} onClick={() => onChange(state)} color={state === value ? 'primary' : 'default'}>{state}</TextButton>,
  )
}

const JobFilter = (props) => (
  <Filter {...props}>
    <StatusSelect label="State" source="status" alwaysOn />
  </Filter>
)

const RetryButton = ({ resource, selectedIds }) => {
  const notify = useNotify()
  const refresh = useRefresh()
  const unselectAll = useUnselectAll(resource)
  const [retry, { loading }] = useUpdateMany(resource, selectedIds, { _action: 'retry' }, {
    onSuccess: ({ data }) => {
      refresh()
      unselectAll()
      notify(`Jobs retried`)
    },
    onFailure: (error) => notify(error.message, 'warning'),
  })

  return (
    <Button label="Retry" onClick={retry} disabled={loading}>
      <Icon>replay</Icon>
    </Button>
  )
}

const JobsBulkActionButtons = props => (
  <>
    <RetryButton {...props} />
    <BulkDeleteButton {...props} />
  </>
)

const cutoffString = (str, len = 30) => str?.length > len ? `${str?.slice(0, len)}...` : str

const JobNameInput = props => {
  const type = useSchemaType('JobInput')
  const choices = useMemo(() => {
    try {
      return JSON.parse(type.getFields().name.description).map(id => ({ id }))
    } catch {
      return []
    }
  }, [type])

  return <SelectInput {...props} choices={choices} optionText="id" validate={required()} />
}

export default generate('Job', {
  only: ['list', 'show', 'create'],
  listFields: {
    name: { sortable: false },
    data: <JsonField source="data" {...{ wrapLabel: false, className: 'no-border', options: { mainMenuBar: false, navigationBar: false, maxVisibleChilds: 3 } }} />,
    delay: <FunctionField label="Delay" render={record => `${record.delay / 1000}s`} />,
    timestamp: { sortable: false },
    attemptsMade: false,
    failedReason: false,
    processedOn: false,
    finishedOn: false,
  },
  showFields: {
    delay: <FunctionField label="Delay" render={record => `${record.delay / 1000}s`} />,
  },
  createInputs: {
    name: <JobNameInput source="name" />,
    opts: {
      options: {
        schema: {
          type: 'object',
          properties: {
            priority: { type: 'integer', minimum: 1 },
            delay: { type: 'integer', minimum: 0 },
            attempts: { type: 'integer', minimum: 1 },
            repeat: {
              type: 'object',
              properties: {
                cron: { type: 'string' },
                tz: { type: 'string' },
                startDate: { type: ['integer', 'string'], format: 'date-time' },
                endDate: { type: ['integer', 'string'], format: 'date-time' },
                limit: { type: 'integer', minimum: 1 },
                every: { type: 'integer', minimum: 1 },
                count: { type: 'integer', minimum: 0 },
              },
              additionalProperties: false,
            },
            backoff: {
              type: ['number', 'object'],
              minimum: 0,
              properties: {
                type: { type: 'string', enum: ['fixed', 'exponential'] },
                delay: { type: 'integer', minimum: 0 },
              },
              additionalProperties: false,
            },
            lifo: { type: 'boolean' },
            timeout: { type: 'integer', minimum: 1000 },
            jobId: { type: ['integer', 'string'] },
            removeOnComplete: { type: 'boolean' },
            removeOnFail: { type: 'boolean' },
            stackTraceLimit: { type: 'integer', minimum: 1 },
          },
          additionalProperties: false,
        },
      },
    },
    delay: false,
    returnvalue: false,
    attemptsMade: false,
    failedReason: false,
    stacktrace: false,
    timestamp: false,
    finishedOn: false,
    processedOn: false,
  },
  options: {
    label: 'Queue Jobs',
    list: {
      filters: <JobFilter />,
      filterDefaultValues: { status: 'failed' },
      bulkActionButtons: <JobsBulkActionButtons />,
    },
  },
  icon: 'fas fa-tasks',
})
