import React from 'react'

import { TextField } from 'react-admin/esm'

import { generate } from 'admin/helpers/model-helper'

export default generate('Transaction', {
  listFields: {
    serial: false,
    item: <TextField source="item.name" label="Item" />
  },
  showFields: {},
  createInputs: {},
  editInputs: {},
  icon: 'fas fa-shopping-cart',
})
