import React from 'react'

import { required } from 'react-admin/esm'

import { JsonField } from 'admin/kit'
import { generate } from 'admin/helpers/model-helper'

const TYPE = {
  allowEmpty: false,
  validate: [required()],
  componentType: 'Select',
  choices: [
    { id: 0, name: 'WEEKLY' },
    { id: 1, name: 'DAILY' },
  ],
}

const ACTION = {
  allowEmpty: false,
  validate: [required()],
  componentType: 'Select',
  choices: [
    { id: 0, name: 'SHARE' },
    { id: 1, name: 'SHARE MVP' },
    { id: 127, name: 'WATCH VDO' },
    { id: 128, name: 'PLAY 1 GAME' },
    { id: 129, name: 'CLICK TO LINK' },
    { id: 130, name: 'GET LINK CLICKED' },
  /*  { id: 131, name: 'WIN 3 GAME' },
    { id: 132, name: 'SHARE VOD' },
    { id: 133, name: 'VOTING MVP' },*/
  ],
}

export default generate('Mission', {
  listFields: {
    action: ACTION,
    createdAt: {
      source: 'type',
      ...TYPE,
    },
    meta: <JsonField source="meta" {...{ wrapLabel: false, className: 'no-border', options: { mainMenuBar: false, navigationBar: false, maxVisibleChilds: 3 } }} />,
    type: false,
  },
  showFields: {
    type: TYPE,
    action: ACTION,
  },
  createInputs: {
    id: true,
    type: TYPE,
    action: ACTION,
  },
  editInputs: {
    id: true,
    type: TYPE,
    action: ACTION,
  },
  icon: 'fab fa-jedi-order',
})
