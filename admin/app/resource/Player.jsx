import React from 'react'

import { ImageField, required, SelectInput } from 'react-admin/esm'

import { generate } from 'admin/helpers/model-helper'

const UPFIELD = {
  componentType: 'Select',
  label: 'Up/Down',
  choices: [
    { id: 0, name: 'UNCHANGE' },
    { id: 1, name: 'UP' },
    { id: 2, name: 'DOWN' },
  ],
  allowEmpty: false,
}

const INPUT = {
  icon: { fullWidth: true },
  roles: {
    children: {
      playerId: false,
      roleId: {
        label: 'Role',
        validate: [required()],
        allowEmpty: false,
        children: <SelectInput optionText="name" />,
        sort: { field: 'id', order: 'ASC' },
      },
      mvp: {
        label: 'MVP',
        validate: [required()],
      },
      hot: {
        label: 'Hot',
      },
      // tierId: {
      //   label: 'Tier',
      //   validate: [required()],
      //   allowEmpty: false,
      //   children: <SelectInput optionText="name" />,
      //   sort: { field: 'id', order: 'ASC' },
      // },
      // up: UPFIELD,
    },
  },
}

export default generate('Player', {
  listFields: {
    icon: <ImageField source="icon" />,
  },
  showFields: {
    icon: <ImageField source="icon" />,
    roles: {
      children: {
        playerId: false,
        up: UPFIELD,
      },
    },
  },
  createInputs: INPUT,
  editInputs: INPUT,
  icon: 'fas fa-gamepad',
  options: {
    list: {
      filters: {
        name_contain: true,
      },
    },
  },
})
