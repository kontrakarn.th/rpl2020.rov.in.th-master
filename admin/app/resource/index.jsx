import React from 'react'
import { Resource } from 'react-admin/esm'

const resources = [
  require('./Admin').default,
  require('./Group').default,
  require('./User').default,
  require('./State').default,
  require('./UserInfo').default,
  require('./Item').default,
  require('./Lucky').default,
  require('./Region').default,
  require('./RegionMilestone').default,
  require('./PersonMilestone').default,
  require('./Role').default,
  require('./Player').default,
  require('./Mission').default,
  require('./Vod').default,
  require('./Week').default,
  require('./Predict').default,
  require('./Refer').default,
  require('./History').default,
  require('./Transaction').default,
  require('./QueueJob').default,
  require('./Configuration').default,
]

export default () => resources.map(model => <Resource key={model.name} {...model} />)
