import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('PersonMilestone', {
  listFields: {

  },
  showFields: {
  },
  createInputs: {},
  editInputs: {},
  icon: 'fas fa-address-card',
})
