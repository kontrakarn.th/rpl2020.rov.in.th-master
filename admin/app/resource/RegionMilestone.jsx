import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('RegionMilestone', {
  listFields: {

  },
  showFields: {
  },
  createInputs: {},
  editInputs: {},
  icon: 'fas fa-atlas',
})
