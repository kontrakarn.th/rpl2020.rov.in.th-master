import React from 'react'

import { ImageField } from 'react-admin/esm'

import { generate } from 'admin/helpers/model-helper'

const TYPE = {
  componentType: 'Select',
  choices: [
    { id: 0, name: 'INGAME' },
    { id: 128, name: 'MERCHANDISE' },
  ],
  allowEmpty: false,
}

export default generate('Item', {
  listFields: {
    icon: <ImageField source="icon" />,
    gameType: TYPE,
  },
  showFields: {
    icon: <ImageField source="icon" />,
    gameType: TYPE,
  },
  createInputs: {
    icon: { fullWidth: true },
    gameType: TYPE,
  },
  editInputs: {
    icon: { fullWidth: true },
    gameType: TYPE,
  },
  icon: 'fas fa-gift',
})
