import React from 'react'

import { generate } from 'admin/helpers/model-helper'

import IpField from '../component/IpField'
import IpInput from '../component/IpInput'

export default generate('User', {
  listFields: {
    avatar: false,
    ip: <IpField source="ip" />,
  },
  showFields: {
    ip: <IpField source="ip" />,
  },
  createInputs: {
    ip: <IpInput source="ip" />,
  },
  editInputs: {
    ip: <IpInput source="ip" />,
  },
  icon: 'fas fa-user-circle',
  options: {
    list: {
      filters: {
      },
    },
  },
})
