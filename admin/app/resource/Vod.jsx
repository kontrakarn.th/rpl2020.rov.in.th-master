import React from 'react'

import { JsonField } from 'admin/kit'
import { generate } from 'admin/helpers/model-helper'

export default generate('Vod', {
  listFields: {
    meta: <JsonField source="meta" {...{ wrapLabel: false, className: 'no-border', options: { mainMenuBar: false, navigationBar: false, maxVisibleChilds: 3 } }} />,
  },
  showFields: {},
  createInputs: {
    id: true,
  },
  editInputs: {
    id: true,
  },
  icon: 'fas fa-video',
})
