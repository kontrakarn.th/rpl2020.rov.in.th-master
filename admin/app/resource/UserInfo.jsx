import React from 'react'

import { generate } from 'admin/helpers/model-helper'

export default generate('UserInfo', {
  listFields: {},
  showFields: {},
  createInputs: {
    id: true,
  },
  editInputs: {
    id: true,
  },
  icon: 'fas fa-info-circle',
})
