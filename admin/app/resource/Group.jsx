import React from 'react'

import { generate } from 'admin/helpers/model-helper'

import ModelInput from '../component/ModelInput'
import OperationField from '../component/OperationField'
import OperationInput from '../component/OperationInput'

export const modelInput = <ModelInput />

const INPUTS = {
  permissions: {
    fullWidth: true,
    children: {
      model: <ModelInput />,
      allow: <OperationInput />,
    },
  },
}

export default generate('Group', {
  listFields: {},
  showFields: {
    permissions: {
      children: {
        allow: <OperationField />,
        createdAt: true,
        updatedAt: true,
      },
    },
  },
  editInputs: INPUTS,
  createInputs: INPUTS,
  icon: 'fas fa-users',
})
