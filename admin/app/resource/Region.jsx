import React from 'react'

import { ImageField } from 'react-admin/esm'

import { generate } from 'admin/helpers/model-helper'

export default generate('Region', {
  listFields: {
    icon: <ImageField source="icon" />,
  },
  showFields: {
    icon: <ImageField source="icon" />,
  },
  createInputs: {
    id: true,
    icon: { fullWidth: true },
  },
  editInputs: {
    id: true,
    icon: { fullWidth: true },
  },
  icon: 'fas fa-globe-asia',
})
