import React, { useMemo } from 'react'

import lodash from 'lodash'
import moment from 'moment'
import {
  required, useInput,
  TextField, DateField, BooleanField,
  TextInput, NumberInput, BooleanInput,
} from 'react-admin/esm'

import RichTextInput from '../component/RichTextInput'
import RichTextField from '../component/RichTextField'

import { generate } from 'admin/helpers/model-helper'
import { DateInput, DateTimeInput, JsonField, JsonInput } from 'admin/kit'

const DurationField = ({ record, source }) => moment.duration({ seconds: lodash.get(record, source) }).humanize()

const ShortJsonField = ({ record, source }) => {
  const data = JSON.stringify(lodash.get(record, source))
  return data.length > 25 ? data.slice(0, 25) + '...' : data
}

const removeTags = (input) => input?.replace(/<[^>]+>/gm, '') || ''

const ShortTextField = ({ record, source }) => {
  const str = lodash.get(record, source)
  const sstr = useMemo(() => removeTags(str), [str])

  return sstr?.length > 36 ? sstr.slice(0, 36) + '...' : sstr
}

const TypedField = ({ view, ...props }) => {
  const { record: { type }, label, source } = props

  const component = useMemo(() => {
    switch (type) {
      case 'Date':
      case 'DateTime':
        return DateField
      case 'Boolean':
        return BooleanField
      case 'Duration':
        return DurationField
      case 'Json':
        return JsonField
      case 'Text':
        return view === 'list' ? ShortTextField : RichTextField
      default:
        return TextField
    }
  }, [view, type])

  const defaultProps = useMemo(() => {
    switch (type) {
      case 'DateTime':
        return { showTime: true }
      case 'Json':
        if (view === 'list') return { wrapLabel: false, className: 'no-border', options: { mainMenuBar: false, navigationBar: false, maxVisibleChilds: 3 } }
        break
    }
  }, [view, type])

  return React.createElement(component, { ...defaultProps, ...props })
}

const TypedInput = (props) => {
  const { input: { value: type } } = useInput({ source: 'type' })
  const { input: { value: schema } } = useInput({ source: 'schema' })

  const component = useMemo(() => {
    switch (type) {
      case 'Date':
        return DateInput
      case 'DateTime':
        return DateTimeInput
      case 'Duration':
      case 'Number':
        return NumberInput
      case 'Json':
        return JsonInput
      case 'Boolean':
        return BooleanInput
      case 'Text':
        return RichTextInput
      default:
        return TextInput
    }
  }, [type])

  const defaultProps = useMemo(() => {
    switch (type) {
      case 'Json':
        return { options: { schema } }
      case 'Boolean':
        return { defaultValue: false }
    }
  }, [type, schema])

  return React.createElement(component, { ...defaultProps, ...props })
}

export default generate('Configuration', {
  listFields: {
    value: <TypedField source="value" view="list" />,
    type: false,
    createdAt: false,
  },
  showFields: {
    value: <TypedField source="value" view="show" />,
  },
  createInputs: {
    value: <TypedInput source="value" validate={required()} />,
    description: { fullWidth: true },
    type: { initialValue: 'Text' },
    schema: {
      options: {
        schema: {
          $ref: 'http://json-schema.org/draft-07/schema#',
        },
      },
    },
  },
  editInputs: {
    key: { disabled: true },
    value: <TypedInput source="value" validate={required()} />,
    type: false,
    schema: false,
    description: { fullWidth: true },
  },
  options: {
    list: {
      sort: { field: 'id', order: 'ASC' },
      filters: {
        key: false,
        key_contain: true,
        description: false,
        description_contain: true,
      },
    },
  },
  icon: 'fas fa-cog',
})
